<div class="container black">
	<div class="row">
		<div class="col-xs-12 col-md-9 ">
			<div class="content">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<?php endwhile; else : ?>
					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-xs-12 col-md-3 text-left sidebar">
			<h2><?php echo the_field(sidebar_title); ?></h2>

		</div>
	</div>
</div>