<?php 
$about_company = get_field('about_company');
$about_company_right = get_field('about_company_right');
$company_logo = get_field('company_logo');

$postID = get_the_ID(); 

 ?>

<section>
	<div class="container-fluid about-company g-0">
		<div class="row g-0">
			<div class="col-12 col-lg-6 image" style="background-image: url('<?php echo the_post_thumbnail_url('$postID'); ?>'); background-size: cover; background-position: center;">
				<?php if ($company_logo): ?>
					<img class="company_logo thumbnail" src="<?php echo $company_logo; ?>" alt="">
				<?php endif; ?>
			</div>
			<div class="col-12 col-lg-6 content">
				<?php echo $about_company; ?>
				<?php echo $about_company_right; ?>
			</div>
		</div>
	</div>
</section>


<!-- <section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="about-box">
					<div class="row">
						<div class="col-12 d-flex <?php if ($about_company_right): echo "col-lg-6"; else: echo ""; endif; ?>">
							<div class="content" >
							<?php if ($about_company_right):

								else: ?>
									<img src="<?php echo $company_logo; ?>" alt="">
								<?php endif; ?>
								<img src="<?php echo $company_logo; ?>" class="d-lg-none" alt="">
								<?php echo $about_company; ?>
							</div>
							
						</div>
						<div class="col-3 d-lg-none col-lg-3 col-xl-2 logo p-5">
							
						</div> -->
						<?php if ($about_company_right): ?>
							<!-- <div class="col-12 col-lg-6">
								<div class="content">
									<img src="<?php echo $company_logo; ?>" class="d-none d-lg-block" alt="" style="float: right;">
									<?php echo $about_company_right; ?>
								</div>
							</div> -->
						<?php endif; ?>
						<!-- <div class="d-none col-lg-3 col-xl-2 d-lg-block logo p-5">
							
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->