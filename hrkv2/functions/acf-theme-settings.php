<?php 
if( function_exists('acf_add_options_page') ) {
 
	$page = acf_add_options_page(array(
		'page_title' 	=> 'Theme opties',
		'menu_title' 	=> 'Theme opties',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'manage_options',
		'redirect' 	=> false
	)); 


	if( function_exists('acf_add_options_sub_page') ) {
		// acf_add_options_sub_page(array(
		// 	'title' => 'Homepage',
		// 	'parent' => 'theme-general-settings',
		// 	'capability' => 'manage_options',
		// ));
		acf_add_options_sub_page(array(
			'title' => 'Bedrijfsgegevens',
			'parent' => 'theme-general-settings',
			'capability' => 'manage_options'
		));
	}
}

function my_acf_admin_head() {
    ?>
    <style type="text/css">

    	/* Wordpress styling */
    	.postbox {
    		border-radius: 15px;
			overflow: hidden;
    	}
    	#wpfooter {
    		display: none;
    	}
		.wrap.acf-settings-wrap {
			height: 50px;
		}
		.wrap.acf-settings-wrap h1 {
			color: #f1f1f1;
		}
		.acf-postbox {
			background: transparent;
			border: none;
			border-radius: 15px;
		}
		.acf-postbox .postbox-header {
			margin-bottom: 20px;
		}
		.acf-postbox .postbox-header h2 {
			background: #f1f1f1;
		}
		.acf-postbox .acf-hndle-cog {
			display: inline-block;
		}
    	.acf-fields>.acf-tab-wrap {
    		background: none;
    		padding: 15px;
    		/*text-align: center;*/
    		border-bottom: 1px solid #ededed;
    	}
    	.acf-postbox>.inside {
    		background: #fff;
    		border-radius: 15px;
    		box-shadow: 0 6px 12px rgba(140,152,164,.075);
    		overflow: hidden;
    	}
    	.acf-field .acf-label label {
    		font-weight: 400;
    	}
    	
    	.acf-tab-group {
    		padding: 0;
    	}
    	.acf-tab-group li {
    		margin: 0;
    		padding: 5px;
    	}
    	.acf-tab-group li a{
    		border: none;
    		line-height: 19px;
    	}
    	.acf-fields>.acf-tab-wrap .acf-tab-group {
    		background: #e6e6e6;
    		border-radius: 5px;
    		border-bottom: none;
    		z-index: 1;
    		display: inline-flex;
    	}
    	.acf-fields>.acf-tab-wrap .acf-tab-group li a {
    		background: transparent;
    		color: #666;
    		padding: 7px 14px;
    		font-weight: 400;
    	}
    	.acf-fields>.acf-tab-wrap .acf-tab-group li a:hover {
    		background: transparent;
    		color: #393939;
    	}
    	.acf-fields>.acf-tab-wrap .acf-tab-group li.active a {
    		background: #fff;
    		box-shadow: 0 3px 6px 0 rgba(140,152,164,.25);
    		border-radius: 5px;
    		color: #393939;
    	}
    	.acf-fields>.acf-tab-wrap .acf-tab-group li.active a {
    		
    	}
    	.acf-tab-wrap.-left .acf-tab-group li.active a {
			background-color: #008ec2;
			color: #fff;
			font-weight: 600;
			border-bottom: none;
		}

    	.acf-flexible-content .layout {
    		border-radius: 15px;
    		border: 1px solid #e9e9e9;
    		overflow: hidden;
    		box-shadow: 0 6px 12px rgba(140,152,164,.2);
    	}
    	.acf-flexible-content .layout .acf-fc-layout-handle {
    		padding: 10px;
    	}
		.acf-field.acf-accordion {
			border: none;
			/*border-bottom: 1px solid #d5d9dd;*/
			padding: 0 !important;
			border-radius: 15px;
		}
		.acf-field.acf-accordion.-open .acf-label.acf-accordion-title {
			background: #008ec2;
			border: 1px solid #008ec2;
			/*border-left: 5px solid #02739d;*/
			color: #fff;
			margin: 0;
		}
		.acf-field.acf-accordion.-open .acf-label.acf-accordion-title:hover {
			color: #fff;
		}
		.acf-field.acf-accordion .acf-label.acf-accordion-title {
			border-top: 1px solid #e4e4e4;
			/*border-bottom: 1px solid #e4e4e4;*/
			background: transparent;
			color: #666;
		}
		.acf-field.acf-accordion .acf-label.acf-accordion-title:hover {
			color: #393939;
		}
		.acf-field.acf-accordion.-open .acf-input.acf-accordion-content {
			/*background: #f4f4f4;*/
			border-bottom: 1px solid #ededed;
		}
		.acf-field.acf-accordion .acf-input.acf-accordion-content {
			border: none;
			
		}
		.acf-field[data-width]+.acf-field[data-width] {
			border-left: 0;
		}
			.acf-accordion { 
			border-bottom: none; 
		}
		.acf-actions {
			margin-top: 25px;
		}
		.acf-field input[type="text"], .acf-field input[type="password"], .acf-field input[type="date"], .acf-field input[type="datetime"], .acf-field input[type="datetime-local"], .acf-field input[type="email"], .acf-field input[type="month"], .acf-field input[type="number"], .acf-field input[type="search"], .acf-field input[type="tel"], .acf-field input[type="time"], .acf-field input[type="url"], .acf-field input[type="week"], .acf-field textarea, .acf-field select {
			border: 1px solid #c1c3c9;
			border-radius: 5px;
		}
		.acf-fields.-left>.acf-tab-wrap .acf-tab-group {
			padding-left: 0;
		}

		/* layout selector */
		.layout-selector {
			background: #f0f4f7;
		}

		.layout-selector ul.acf-hl:before {
			display: none;
		}

		.layout-selector ul.acf-radio-list {
			display: grid;
			grid-template-columns: 1fr 1fr 1fr;
			grid-gap: 30px;
		}

		.layout-selector ul.acf-radio-list li {
			margin-right: 0;
		}
		.layout-selector ul.acf-radio-list input[type="radio"] {
			position: absolute;
			opacity: 0;
			width: 0;
			height: 0;
		}
		.layout-selector ul.acf-radio-list label img {
			border: 4px solid transparent;
			box-shadow: 4px 4px 9px rgba(0, 0, 0, 0.22);
			max-width: 100%;
			transition: all 0.2s ease;
			box-sizing: border-box;
		}
		.layout-selector ul.acf-radio-list label.selected img {
			border: 4px solid rgba(37, 102, 144, 0.61);
		}
		.acf-field.acf-field-5fb3eecb4ac82 > .acf-label {
			display: none;
		}
		.acfe-modal.-open .acfe-modal-content .nav-tab-wrapper a.nav-tab-active, .acfe-modal.-open .acfe-modal-content .nav-tab-wrapper a.nav-tab-active:hover {
			background: #eeeeee;
			color: #0080a4;
		}
		.acfe-flexible-layout-thumbnail {
			background-color: #fff;
			background-size: contain;
			min-height: 180px;
		}
		
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a {
			position: relative;
			padding: 0 0 15px 0;
			overflow: hidden;
		}
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="cpt_content"]:after,
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="logo_row_slider_section"]:after,
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="testimonials_section"]:after,
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="content_image_fullwidth_section"]:after {
			background: rgba(126, 181, 0, 0.53);
			box-shadow: 0px 5px 17px rgba(0, 0, 0,0.2);
			color: #000;
			top: 0;
			left: -120px;
			position: absolute;
			content: " Meerdere layouts";
			padding: 7px 15px;
			transition: all 0.3s ease;
		}
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="cpt_content"]:hover:after,
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="logo_row_slider_section"]:hover:after,
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="testimonials_section"]:hover:after,
		.acfe-modal.-open .acfe-modal-content .acfe-flex-container ul li a[data-layout="content_image_fullwidth_section"]:hover:after {
			background: rgba(126, 181, 0, 0.9);
			left: 0;
			padding: 7px;
		}
		.acfe-seamless-style .acf-label {
			display: none;
		}
		.acfe-seamless-style .acf-input .acf-label {
			display: block;
		}

		.acf-admin-toolbar,
		.acf-admin-toolbar {
			display: none;
		}
		.acf-headerbar-field-editor {
			top: 32px;
		}
		.acf-field-settings.acf-fields>.acf-tab-wrap {
			padding: 0;
		}
		.acf-admin-field-groups .wp-list-table thead th {
			height: 28px;
			border-box: content-box;
		}

    </style>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');


?>
