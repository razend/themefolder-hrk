<?php
// 30-8-21: blade engine opmaak verwijderd.
// Selectie van activiteiten op homepage
$servicesOrder = get_field('servicesOrder','option');

$headerimage = get_field('headerimage');
$customheaderimage = get_field('custom_image_header');
$show_colored_space = get_field('show_colored_space');
$show_colored_space_footer = get_field('show_colored_space_footer');
$headerImageTitle = get_field('title_in_image_top');
$customheaderimage = get_field('custom_image_header');
$customfooterimage = get_field('custom_image_footer');
$footerImageTitle = get_field('title_in_image_bottom');
$footerimage = get_field('footerimage');
$overview_title = get_field('overview_title','option');
$overview_content = get_field('overview_content','option');
$service_content = get_field('service_content');
$service_content_part2 = get_field('service_content_part2');
?>
<!-- Enkele pagina -->
<?php if(is_singular('services')):
	if($headerimage === 'custom_image'): ?>
		<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space; ?>" style="background-image: url(<?php echo $customheaderimage; ?>);">
	<?php else : ?>
		<section id="fullwidth-image" class="<?php echo $headerimage; ?> d-flex justify-content-start align-items-center">
	<?php endif; ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<h1 class="ps-5"><?php echo $headerImageTitle; ?></h1>
				</div>
			</div>
		</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-12 text-left">
						<h1><?php echo the_title(); ?></h1>
						<div class="content">
							<?php echo $service_content; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="servicepoints">
			<div class="container">
				<div class="row">
					<?php if( have_rows('service_points_row_1') ): while( have_rows('service_points_row_1') ) : the_row(); 
						$service_icon = get_sub_field('service_icon');
						$service_point = get_sub_field( 'service_point');
						?>
						<div class="col servicepoint justify-content-center">
							<img src="<?php echo $service_icon; ?>" class="service_icon" alt="">
							<p><?php echo $service_point; ?></p>
						</div>
					<?php endwhile; endif; ?>
				</div>
				<div class="row">
					<?php if( have_rows('service_points_row_2') ): while( have_rows('service_points_row_2') ) : the_row(); 
						$service_icon = get_sub_field('service_icon');
						$service_point = get_sub_field( 'service_point');
						?>
						<div class="col servicepoint justify-content-center">
							<img src="<?php echo $service_icon; ?>" class="service_icon" alt="">
							<p><?php echo $service_point; ?></p>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-12 text-left">
						<div class="content">
							<?php echo $service_content_part2; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<?php get_template_part('resources/components/quote_slider'); ?>
	
		<?php if($footerimage === 'custom_image'): ?>
			<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space_footer; ?>" style="background-image: url(<?php echo $customfooterimage; ?>);">
		<?php else : ?>
			<section id="fullwidth-image" class="<?php echo $footerimage; ?> d-flex justify-content-start align-items-center">
		<?php endif; ?>
				<h1 class="ps-5"><?php echo $footerImageTitle; ?></h1>
			</section>
	<?php else : ?>
<!-- Overzicht -->
<section id="services">
	<div class="container">
		<div class="row">
			<div class="col-12  mb-5">
				<h1><?php echo $overview_title; ?></h1>
				<div class="content">
					<?php echo $overview_content; ?>
				</div>
			</div>
		</div>
		<div class="row row-eq-height">
			<?php foreach ($servicesOrder as $post): 
				$service_photo_text get_field('service_photo_text');
				$service_content_excerpt = get_field('service_content_excerpt');
				?>
			<div class="col service ">
				<div class="thumbnail d-flex align-items-end "style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);" onclick="window.location.href='<?php echo the_permalink(); ?>'">
					<a class="" href="<?php echo the_permalink(); ?>" ><?php echo $service_photo_text; ?></a>
				</div>
				<a class="title" href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
					<?php echo $service_content_excerpt; ?>
				<a class="readmore" href="<?php echo the_permalink(); ?>">lees meer <i class="fas fa-hand-pointer"></i></a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>




