<?php 
	// Background settings
	$background_settings = get_sub_field('section_background_settings');
	$section_bg = $background_settings['section_bg_img'];
	$section_bg_pos = $background_settings['background_position'];
	$bg_color = $background_settings['section_bg_color'];

	// Blok settings
	$block_settings = get_sub_field('section_settings');
	$section_less_margin = $block_settings['section_less_margin'];
	$sticky = get_sub_field('sticky-top');

	// Dev settings
	$developer_options = get_sub_field('developer_options');
	$section_class = $developer_options['section_class'];

	$layout = get_sub_field('layout_selection');
?>

<section  class="<?php echo get_row_layout(); echo " " . $section_class . " " . $layout; ?>" style="<?php if ($bg_color): ?>background-color: <?php echo $bg_color;?>; <?php endif; ?> ">