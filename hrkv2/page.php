<?php get_header();

	if(is_front_page()):
		get_template_part('resources/views/layouts/home');
	elseif(is_page('contact')):
		get_template_part('resources/views/layouts/contact');
	else:
		get_template_part('resources/views/layouts/page');
	endif;
	
get_footer(); ?>