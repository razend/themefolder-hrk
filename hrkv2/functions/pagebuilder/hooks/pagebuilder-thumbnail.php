<?php 

// Pagebuilder V2

add_filter('acfe/flexible/thumbnail/layout=hero_section', 'hero_layout_thumbnail', 10, 3);
function hero_layout_thumbnail($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/hero.png' );

}

add_filter('acfe/flexible/thumbnail/layout=textual_section', 'textual_layout_thumbnail', 10, 3);
function textual_layout_thumbnail($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/text_2col.png' );

}

add_filter('acfe/flexible/thumbnail/layout=cpt_section', 'cpt_section', 10, 3);
function cpt_section($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/dynamic_content_2.webp' );

}

add_filter('acfe/flexible/thumbnail/layout=quote_slider', 'quote_slider', 10, 3);
function quote_slider($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/quote_slider_2.png' );

}

add_filter('acfe/flexible/thumbnail/layout=usp_section', 'usp_section', 10, 3);
function usp_section($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/usps_1.webp' );

}

add_filter('acfe/flexible/thumbnail/layout=textual_video_img_section', 'textual_img_vid_layout_thumbnail', 10, 3);
function textual_img_vid_layout_thumbnail($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/text-image_4.webp' );

}

add_filter('acfe/flexible/thumbnail/layout=carousel_section', 'carousel_section', 10, 3);
function carousel_section($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/carousel_layout1.png' );

}

add_filter('acfe/flexible/thumbnail/layout=logo_section', 'logo_section', 10, 3);
function logo_section($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/logos_layout_1.png' );

}

add_filter('acfe/flexible/thumbnail/layout=contact_section', 'contact_section', 10, 3);
function contact_section($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/contact_1.webp' );

}
add_filter('acfe/flexible/thumbnail/layout=team_section', 'team_section', 10, 3);
function team_section($thumbnail, $field, $layout){

    // Must return an URL or Attachment ID
    return ( get_theme_file_uri() . '/images/pagebuilder/team_1.webp' );

}

?>