<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_63299f66ec074',
	'title' => 'Page builder v2',
	'fields' => array(
		array(
			'key' => 'field_63299f66f1633',
			'label' => 'Pagebuilder',
			'name' => 'pagebuilder',
			'aria-label' => '',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'hide_field' => '',
			'hide_label' => '',
			'hide_instructions' => '',
			'hide_required' => '',
			'acfe_flexible_advanced' => 1,
			'acfe_flexible_stylised_button' => 1,
			'acfe_flexible_layouts_templates' => 0,
			'acfe_flexible_layouts_placeholder' => 1,
			'acfe_flexible_layouts_thumbnails' => 1,
			'acfe_flexible_layouts_settings' => 1,
			'acfe_flexible_layouts_locations' => 0,
			'acfe_flexible_async' => array(
			),
			'acfe_flexible_add_actions' => array(
				0 => 'title',
				1 => 'toggle',
				2 => 'copy',
				3 => 'close',
			),
			'acfe_flexible_remove_button' => array(
			),
			'acfe_flexible_modal_edit' => array(
				'acfe_flexible_modal_edit_enabled' => '1',
				'acfe_flexible_modal_edit_size' => 'xlarge',
			),
			'acfe_flexible_modal' => array(
				'acfe_flexible_modal_enabled' => '1',
				'acfe_flexible_modal_title' => '',
				'acfe_flexible_modal_size' => 'full',
				'acfe_flexible_modal_col' => '5',
				'acfe_flexible_modal_categories' => '1',
			),
			'acfe_flexible_grid' => array(
				'acfe_flexible_grid_enabled' => '0',
				'acfe_flexible_grid_align' => 'center',
				'acfe_flexible_grid_valign' => 'stretch',
				'acfe_flexible_grid_wrap' => false,
			),
			'layouts' => array(
				'layout_6345745904049' => array(
					'key' => 'layout_6345745904049',
					'name' => 'carousel_section',
					'label' => 'Carousel',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_634574590404a',
							'label' => 'Carousel',
							'name' => 'carousel',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_636cb87ebc68c',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_63ad5ef5b3c91' => array(
					'key' => 'layout_63ad5ef5b3c91',
					'name' => 'contact_section',
					'label' => 'Contact',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_63ad5ef5b3c92',
							'label' => 'Contact',
							'name' => 'contact',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_63ac5df5d382e',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_633aaa0403c0a' => array(
					'key' => 'layout_633aaa0403c0a',
					'name' => 'cpt_section',
					'label' => 'Dynamische inhoud',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_633aaa0503c0b',
							'label' => 'Dynamisch inhoud',
							'name' => 'dynamic_content',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_633aa0dddf0af',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_5e1c6bf35bbd4' => array(
					'key' => 'layout_5e1c6bf35bbd4',
					'name' => 'hero_section',
					'label' => 'Heroblok',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_6329a12982d0f',
							'label' => 'Hero',
							'name' => 'hero',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_6329a07216004',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_634ff282c58a0' => array(
					'key' => 'layout_634ff282c58a0',
					'name' => 'logo_section',
					'label' => 'Logo\'s',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_634ff282c58a1',
							'label' => 'Logo\'s',
							'name' => 'logo_section',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_634ff0c2f30d5',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_6124de4cc7510' => array(
					'key' => 'layout_6124de4cc7510',
					'name' => 'quote_slider',
					'label' => 'Quote slider',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_633d7b534570a',
							'label' => 'Quote slider',
							'name' => 'quote_slider_block',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_633d7a2e982d1',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_63aacc7a5a85d' => array(
					'key' => 'layout_63aacc7a5a85d',
					'name' => 'usp_section',
					'label' => 'USP\'s',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_63aacc7a5a85e',
							'label' => 'USP\'s',
							'name' => 'usps_block',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_63aac7cb32978',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_63b2e5824e703' => array(
					'key' => 'layout_63b2e5824e703',
					'name' => 'team_section',
					'label' => 'Teamblok',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_63b2e5824e704',
							'label' => 'Team block',
							'name' => 'team_section',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'hide_field' => '',
							'hide_label' => '',
							'hide_instructions' => '',
							'hide_required' => '',
							'clone' => array(
								0 => 'group_63b2e39c307d4',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_settings' => '',
							'instruction_placement' => '',
							'acfe_permissions' => '',
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_5e14891e6e685' => array(
					'key' => 'layout_5e14891e6e685',
					'name' => 'textual_section',
					'label' => 'Tekstblok',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_6329cf82cd4ed',
							'label' => 'Vrij tekstveld',
							'name' => 'vrij_tekstveld',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_6329cf263e92e',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
				'layout_5e149f8f426db' => array(
					'key' => 'layout_5e149f8f426db',
					'name' => 'textual_video_img_section',
					'label' => 'Tekstblok met media',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_63440b5213384',
							'label' => 'Vrij tekstveld met afbeelding',
							'name' => 'tekst-image_section',
							'aria-label' => '',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_634406f3d78ea',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
							'acfe_field_group_condition' => 0,
							'acfe_seamless_style' => 0,
							'acfe_clone_modal' => 0,
							'acfe_clone_modal_close' => 0,
							'acfe_clone_modal_button' => '',
							'acfe_clone_modal_size' => 'large',
						),
					),
					'min' => '',
					'max' => '',
					'acfe_flexible_category' => '',
					'acfe_flexible_modal_edit_size' => '',
					'acfe_flexible_settings' => '',
					'acfe_flexible_settings_size' => 'medium',
					'acfe_flexible_thumbnail' => '',
					'acfe_flexible_render_template' => false,
					'acfe_flexible_render_style' => false,
					'acfe_flexible_render_script' => false,
					'acfe_layout_locations' => array(
					),
					'acfe_layout_col' => 'auto',
					'acfe_layout_allowed_col' => false,
				),
			),
			'acfe_settings' => '',
			'instruction_placement' => '',
			'acfe_permissions' => '',
			'min' => '',
			'max' => '',
			'button_label' => 'Extra blok',
			'acfe_field_group_condition' => 0,
			'acfe_flexible_hide_empty_message' => false,
			'acfe_flexible_empty_message' => '',
			'acfe_flexible_layouts_previews' => false,
			'acfe_flexible_layouts_state' => false,
			'acfe_flexible_grid_container' => false,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-pagebuilderv2.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
		0 => 'categories',
		1 => 'discussion',
		2 => 'format',
		3 => 'the_content',
		4 => 'comments',
		5 => 'revisions',
		6 => 'tags',
	),
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
	'acfe_autosync' => array(
		0 => 'php',
	),
	'acfe_form' => 1,
	'acfe_display_title' => 'Page builder',
	'acfe_permissions' => '',
	'acfe_meta' => array(
		'acfcloneindex' => array(
			'acfe_meta_key' => '',
			'acfe_meta_value' => '',
		),
	),
	'acfe_note' => '',
	'acfe_categories' => array(
		'pagebuilder' => 'Pagebuilder',
	),
	'modified' => 1712057820,
));

endif;