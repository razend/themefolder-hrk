<section id="fullwidth-image" class="elastiekmeisje">
	<div class="white-bar"></div>
</section>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>

<section id="vacancies" class="@if(is_front_page()) third-color @endif">
	<div class="container">
		@if(is_singular('vacancies'))
			<div class="row">
				<div class="col-12">
					<h1>{{the_title()}}</h1>
					<div class="content">
						{{the_field('vacancie_content')}}
					</div>
				</div>
			</div>
		@else
			@if(is_front_page())
				<div class="row">
					<div class="col-12 text-right">
						<h1 class="dark">nieuwste vacatures</h1>
					</div>
				</div>
			@endif
			<div class="row row-eq-height">
				<div class="col-12">
					<h2>Nieuwste vacatures</h2>
				</div>
				<?php $recent_posts = wp_get_recent_posts(array('post_type'=>'vacancies', 'numberposts' => '1' )); ?>
			    @foreach( $recent_posts as $recent )
					<div class="col-12">
						<h1 class="tone-color">{{the_title()}}</h1>
					</div>
					<div class="col-12 col-md-8 text-left">
						<div class="thumbnail">
							<a href='{{get_permalink($recent["ID"])}}' title='{{esc_attr($recent["post_title"])}}' style="background-image: url({{the_post_thumbnail_url()}});">
								
							</a>
						</div>

						

						{{the_field('vacancie_content')}}
					</div>
					<div class="col-12 col-md-4 text-left">
						@include('resources/components/twiglers')
					</div>
			       <!--  <li>
			        	<a href='{{get_permalink($recent["ID"])}}' title='{{esc_attr($recent["post_title"])}}' >{{$recent["post_title"]}}</a>
			   		</li> -->
			    @endforeach
			<!-- <ul>
			<?php
			    $recent_posts = wp_get_recent_posts(array('post_type'=>'vacancies', 'numberposts' => '6','offset' => 1));
			    foreach( $recent_posts as $recent ){
			        echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
			    }
			?>
			</ul> -->
			<?php query_posts('post_type=vacancies'); ?>
			@wpposts
				<div class="col-12 col-lg-4 vacancie">
					<div class="thumbnail">
						<a href="{{the_permalink()}}" style="background-image: url({{the_post_thumbnail_url()}});">
							{{the_field('vacancie_photo_text')}}
						</a>
					</div>
					<a class="title" href="{{the_permalink()}}">{{the_title()}}</a>
					<div class="content">
						{{the_field('vacancie_content_excerpt')}}
					</div>
					<a class="readmore" href="{{the_permalink()}}">(lees meer)</a>
				</div>
			@wpempty
				404
			@wpend
			<?php wp_reset_query(); ?>
			</div>
		@endif

		<div class="row">
			<div class="col-12 text-center">
				<h2 class="dark mt-5">voor een compleet overzicht: <a class="orange" href="https://recruitmentbanen.nl">recruitmentbanen.nl</a></h2>
			</div>
		</div>

	</div>
</section>