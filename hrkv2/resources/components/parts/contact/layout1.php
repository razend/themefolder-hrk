<?php 
	$title = get_sub_field('title');
	$content = get_sub_field('content');
	$content_right = get_sub_field('content_right');
	$form_id = get_sub_field('form_id');

	//recruiter
	$recruiter_id = get_sub_field('recruiter_id');
	$recruiter_name = get_the_title($recruiter_id);
	$recruiter_image = get_the_post_thumbnail_url( $recruiter_id, "medium");
	$recruiter_function = get_field('team_function', $recruiter_id);
	$recruiter_phone = get_field('team_phonenumber',$recruiter_id);
	$recruiter_email = get_field('team_emailaddress',$recruiter_id);
	$recruiter_linkedin = get_field('linkedin_profiel_url',$recruiter_id);

?>

<div class="container">
	<div class="row">
		<div class="col-12 text-center mb-5">
			<h1><?php echo $title ?> </h1>
		</div>
	</div>
	<div class="row colored-box">
		<div class="col-12 col-lg-6 d-flex align-items-center">
			<div class="content">
				<?php 
					echo $content;
					gravity_form( $form_id, false, false, false, '', false ); 
				?>
			</div>
		</div>
		<div class="col-12 col-lg-6">
			<div class="contact-box">
				<?php if ($content_right): ?>
						<p>
							<?php echo $content_right; ?>
						</p>
					<?php endif ?>
				<div class="d-flex align-items-end profile">
					<div class="recruiter-image">
						<img src="<?php echo $recruiter_image; ?>" alt="Foto van <?php echo ( $recruiter_name ); ?>">
					</div>
					<div class="quick-contact-box">
						<a class="no-underline" href="https://wa.me/<?php echo str_replace(' ', '', $recruiter_phone); ?>"><i class="fa-brands fa-whatsapp"></i></a><br>
						<p>whatsapp:<br><a class="smaller-numbers" href="https://wa.me/<?php echo str_replace(' ', '', $recruiter_phone); ?>" target="_blank"><?php echo $recruiter_phone; ?></a></p>
					</div>
				</div>
				<div class="recruiter-details">
					<p>
						<span class="large"><?php echo ( $recruiter_name ); ?></span> - <span class="large"><?php echo $recruiter_function; ?></span><br>
						<?php if($recruiter_phone): ?>
							<span class="semibold"><a class="smaller-numbers" href="tel:<?php echo str_replace(' ', '', $recruiter_phone); ?>" target="_blank" title="Bel <?php echo ( $recruiter_name ); ?>"><?php echo $recruiter_phone; ?></a></span><br>
						<?php endif; ?>
						<?php if($recruiter_email): ?>
							<span class="semibold"><a href="mailto:<?php echo $recruiter_email; ?>" target="_blank" title="Stuur <?php echo ( $recruiter_name ); ?> een e-mail"><?php echo $recruiter_email; ?></a></span>
						<?php endif; ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>