<?php
	$layout = get_sub_field('layout_selection');
	
	get_template_part( '/resources/components/parts/general/section_id');


if($layout == 'layout1'): 

	get_template_part( '/resources/components/parts/hero/layout1');

elseif($layout == 'layout2'):

	get_template_part( '/resources/components/parts/hero/layout2');


endif; 

wp_reset_query(); ?>