<?php 
$title = get_sub_field('title');
$text = get_sub_field('content');

$video_source = get_sub_field('video_source');
$vid = get_sub_field('video');
$video_script = get_sub_field('video_script');

?>

<div class="container">
	<div class="row">
		<div class="col-12 content col-lg-6 d-flex align-items-start flex-column">
			<div class="text">
				<?php echo $text;
				get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
			</div>			
		</div>
		<div class="col-12 col-lg-6 pt-lg-5">
			<div class="video">
				<?php if ($video_source == "youtube"): ?>
					<iframe src="https://www.youtube-nocookie.com/embed/<?php echo $vid; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<?php elseif($video_source == "vimeo"): ?>
					<iframe title="vimeo-player" src="https://player.vimeo.com/video/<?php echo $vid; ?>" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
				<?php elseif($video_source == "getcontrast"): ?>
					<?php echo $video_script; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>