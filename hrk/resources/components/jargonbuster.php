<?php // 30-8-21: blade engine opmaak verwijderd ?>
<section id="jargonbuster">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-expand-lg " role="navigation">

					<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button> -->
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'jargonbuster',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> '',
							'container_id'		=> 'navBar',
							'menu_class'		=> 'nav nav-tabs jargonbuster',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
				</nav>
			</div>
		</div>
		<div class="row">
			<div  class="col-12">
				<?php foreach(range('a','z') as $v):
					if (get_field('jargonbuster_'.$v)): ?>
					<h2><?php echo $v; ?></h2>
						<?php if( have_rows('jargonbuster_'.$v) ): while( have_rows('jargonbuster_'.$v) ) : the_row(); ?>
							<div class="jargon_word">
								<h3 id="<?php echo get_sub_field('jargonbuster_link'); ?>" ><?php echo get_sub_field('jargonbuster_word'); ?></h3>
								<p><?php echo get_sub_field('jargonbuster_description'); ?></p>
							</div>
						<?php endwhile; endif; ?>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>