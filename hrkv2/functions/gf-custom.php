<?php 
	add_filter( 'gform_address_display_format', 'address_format' );
	function address_format( $format ) {
		return 'zip_before_city';
	}

	add_filter( 'gform_confirmation_anchor', '__return_true' );

	add_filter( 'gform_countries', 'remove_country' );
	function remove_country( $countries ){
	    return array( 'Nederland', 'België' );
	}

	// add_filter( 'gform_address_country', 'change_address_country', 10, 2 );


	// Plaats valuta symvo
	add_filter( 'gform_currencies', 'gw_modify_currencies' );
	function gw_modify_currencies( $currencies ) {

		$currencies['EUR'] = array(
			'name'               => esc_html__( 'Euro', 'gravityforms' ),
			'symbol_left'        => '&#8364;',
			'symbol_right'       => '',
			'symbol_padding'     => ' ',
			'thousand_separator' => '.',
			'decimal_separator'  => ',',
			'decimals'           => 2
		);

		return $currencies;
	}

?>