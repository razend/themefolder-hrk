function mapsSelector() {
  if /* if we're on iOS, open in Apple Maps */
    ((navigator.platform.indexOf("iPhone") != -1) || 
     (navigator.platform.indexOf("iPad") != -1) || 
     (navigator.platform.indexOf("iPod") != -1))
    window.open("https://maps.apple.com/?daddr=(52.055620,%205.107970)&dirflg=d");
else /* else use Google */
    window.open("https://www.google.nl/maps/place/HetRecruitingKantoor/@52.0558947,5.1055992,17z/data=!3m1!4b1!4m5!3m4!1s0x47c6655c6c8d1f1d:0x4424f0df911546a2!8m2!3d52.0558947!4d5.1077932");
}

// var prepareForFixed = $('#forFixedNavAnimation').offset().top;
// var stickyTop = $('#content').offset().top;

// $(window).on( 'scroll', function(){
//     if ($(window).scrollTop() >= prepareForFixed) {
//         $('.navbar').addClass("prepareForFixed");
//     } else {
//     	$('.navbar').removeClass("prepareForFixed");
//     }
//     if ($(window).scrollTop() >= stickyTop) {
//         $('.navbar').addClass("fixed");
//         $('.navbar').removeClass("prepareForFixed");
//     } else {
//     	$('.navbar').removeClass("fixed");
//     }
// 

// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
} 

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
})