<?php
$vacancie_description = get_field('vacancie_description');
$vacancie_opertunities = get_field('vacancie_opertunities');
$vacancie_state = get_field('vacancie_state');
$vacancie_job_type = get_field('vacancie_job_type');
$vacancie_experience = get_field('vacancie_experience');
$vacancie_hours = get_field('vacancie_hours');
$vacancie_start = get_field('vacancie_start');
$taxonomy  = 'vacancie_categories';
$tax_terms = get_terms($taxonomy, array('hide_empty' => false));
$vacancie_companyname = get_field('vacancie_companyname');
$vacancie_contact_email = get_field('vacancie_contact_email');
$vacancie_company_website = get_field('vacancie_company_website');
$vacancie_recruiters = get_field('vacancie_recruiters');
$form_id = get_field('form_id');


?>
<!-- Enkele pagina -->
@if(is_singular('vacancies'))

<?php 
	$postID = get_the_ID();  
	$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all")); 
	?>
	<section id="fullwidth-image" class="no-image d-flex justify-content-start align-items-center">
		<h1>{{the_title()}}</h1>
		<div class="white-bar"></div>
	</section>

	<section id="vacancie">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8">
					
					<div class="content">
						<?php echo $vacancie_description; ?>
						
					</div>
					<div id="form" class="post-box blue mt-5">
						<?php gravity_form( $form_id, false, false, false, '', false ); ?>
					</div>
				</div>
				<div class="col-12 col-lg-4 sidebar">
					<div class="sticky-top">
						<a class="btn mt-5 mb-4" href="#form">Direct reageren</a>
						<div class="sidebar-box">
							<div class="box-title">Vacature details</div>
							<div class="box-content">
								<ul class="post-list">
									
									@if($vacancie_state)
										<li><span><strong>Regio: </strong><?php echo implode( ', ', $vacancie_state ) ?></span></li>
									@endif
									<li><span><strong>Functiecategorie: </strong>
									@foreach($term_list as $term_single)
									    <?php echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>'; ?>
									@endforeach
									</span></li>
									@if($vacancie_job_type)
									<li><span><strong>Dienstverband: </strong>{{$vacancie_job_type}}</span></li>
									@endif

									<?php if($vacancie_opertunities): ?>
										<li><span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span></li>
									<?php endif; ?>

									@if($vacancie_experience)
									<li><span><strong>Ervaring: </strong>
										@foreach( $vacancie_experience as $vacancie_experience_single )
									        {{$vacancie_experience_single}}, 
									    @endforeach
									</span></li>
									@endif
									@if($vacancie_hours)
									<li><span><strong>Uren: </strong>
										@foreach( $vacancie_hours as $vacancie_hour )
									        {{$vacancie_hour}}, 
									    @endforeach
									</span></li>
									@endif
									@if($vacancie_start)
									<li><span><strong>Startdatum: </strong>{{$vacancie_start}}</span></li>
									@endif
									<li><span><strong>Geplaatst op: </strong>{{get_the_date()}}</span></li>
								</ul>
							</div>
						</div>
						<div class="sidebar-box">
							<div class="box-title">Bedrijfsinformatie</div>
							<div class="box-content">
								<ul class="post-list">
									@if($vacancie_companyname)
									<li><span><strong>Bij: </strong>{{$vacancie_companyname}} </span></li>
									@endif
									@if($vacancie_contact_email)
									<li><span><strong>Contact: </strong><a href="mailt:{{$vacancie_contact_email}} ">{{$vacancie_contact_email}} </a></span></li>
									@endif
									@if($vacancie_companyname)
									<li><span><strong>Website: </strong><a href="{{$vacancie_company_website}}">{{$vacancie_companyname}}</a> </span></li>
									@endif
								</ul>
							</div>
						</div>
						<div class="sidebar-box">
							<div class="box-title">Recruiter(s)</div>
							<div class="box-content">
								
								@foreach ($vacancie_recruiters as $post)
									<div class="row teammember">
										<div class="col-4 col-lg-5 col-xl-4">
											<div class="thumbnail teamimage" style="background-image: url({{the_post_thumbnail_url()}});">
											</div>
										</div>
										<div class="col-8  col-lg-7 col-xl-8 pl-0">
											<p class="team_name pb-0"><strong>{{the_title()}}</strong><br> {{the_field('team_function')}}</p>
											<ul class="socialmedia icons-circle d-flex justify-content-start ">
												@if(get_field('team_phonenumber'))
												<li><a href="tel:{{the_field('team_phonenumber')}}" target="_blank"><i class="fas fa-phone"></i></i></a></li>
												@endif
												@if(get_field('team_emailaddress'))
												<li><a href="mailto:{{the_field('team_emailaddress')}}" target="_blank"><i class="far fa-envelope"></i></a></li>
												@endif
												@if(get_field('linkedin_profiel_url'))
												<li><a href="{{the_field('linkedin_profiel_url')}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
												@endif
											</ul>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


@else
<!-- Overzicht: Wordt gebruikt bij oa. alle vacatures van een bep. categorie -->

@wpposts
<?php 

$postID = get_the_ID();  
	$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));  
	$vacancie_companyname = get_field('vacancie_companyname',$postID);
	$vacancie_state = get_field('vacancie_state',$postID);
	$vacancie_opertunities = get_field('vacancie_opertunities',$postID);
	$vacancie_hours = get_field('vacancie_hours',$postID);
	$vacancie_job_type = get_field('vacancie_job_type',$postID);
	$vacancie_wages = get_field('vacancie_wages',$postID);
	$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));
?>
<div class="col-12 col-md-4">
		<div class="post">
			<a href="{{the_permalink()}}"><img class="card-img-top" src="{{the_post_thumbnail_url()}}" alt=""></a> 
			<div class="post-body">
				<h5 class="post-title  mb-3"><a href="{{the_permalink()}}">{{the_title()}}</a></h5>
					<p class="post-details compact pb-0">
						<?php if($vacancie_companyname): ?>
						<span><strong>Bij: </strong><?php echo $vacancie_companyname; ?> </span><br>
						<?php endif; ?>

						<?php if($vacancie_opertunities): ?>
							<span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span><br>
						<?php endif; ?>
						
						<strong>Regio:</strong> <?php echo implode( ', ', $vacancie_state ) ?><br>
						<strong>Functiecategorie:</strong>
						@foreach($term_list as $term_single)
						    <?php echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>'; ?>
						@endforeach
						<br>
						<strong>Uren:</strong> @foreach( $vacancie_hours as $vacancie_hour )
					        {{$vacancie_hour}}, 
					    @endforeach<br>
						<strong>Type contract:</strong> {{$vacancie_job_type}}<br>
						<?php if($vacancie_wages): ?>
							<strong>Salaris:</strong> € <?php echo $vacancie_wages; ?>
						<?php endif; ?>
					</p>
					<p class="post-text">{{get_the_excerpt()}}</p>
					<div class="d-flex justify-content-left">
						<a href="{{the_permalink()}}" class="btn small">Bekijk vacature</a>
					</div>
			</div>
			<div class="post-footer">
				{{the_tags()}}
			</div>
		</div>
	</div>
@wpempty
404
@wpend

@endif