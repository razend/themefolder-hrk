<?php

// Wordt gebruikt voor single weergave vacature

// Tab: Vacature informatie
$vacancie_description = get_field('vacancie_description');
$vacancie_description_clean = get_field('vacancie_description_clean');
$vacancie_opertunities = get_field('vacancie_opertunities');
$vacancie_state = get_field('vacancie_state');
$vacancie_job_type = get_field('vacancie_job_type');
$vacancie_experience = get_field('vacancie_experience');
$vacancie_hours = get_field('vacancie_hours');
$vacancie_wages = get_field('vacancie_wages');
$vacancie_start = get_field('vacancie_start');
$taxonomy  = 'vacancie_categories';
$tax_terms = get_terms($taxonomy, array('hide_empty' => false));

$vacancie_date_start = get_field('vacancie_start_exact_date');

// Tab: Bedrijfs informatie
$vacancie_companyname = get_field('vacancie_companyname');
$vacancie_contact_email = get_field('vacancie_contact_email');
$vacancie_company_website = get_field('vacancie_company_website');

// Tab: Locatie
$vacancie_postalcode = get_field('vacancie_postalcode');
$vacancie_state = get_field('vacancie_state');
$vacancie_country = get_field('vacancie_country');

// Recruiters
$vacancie_recruiters = get_field('vacancie_recruiters');

// Form instellingen
$form_id = get_field('form_id');
$form_url = get_field('form_url');

// Vacature instellingen
$vacancie_validThrough = get_field('vacancie_validThrough');
$vacancie_logo = get_the_post_thumbnail_url();

?>


<script type="application/ld+json"> 
    {
        "@context" : "http://schema.org/",
        "@type" : "JobPosting",
        "title" : "<?php echo get_the_title(); ?>",
        "description" : "<?php echo $vacancie_description_clean; ?>",
        "identifier": {
            "@type": "PropertyValue",
            "name": "<?php echo $vacancie_companyname; ?>",
            "value": "<?php echo get_the_ID(); ?>"
        },	
        "datePosted" : "<?php echo get_the_date('Y-m-d'); ?>",
        "validThrough" : "<?php echo $vacancie_validThrough; ?>",
        "employmentType" : "<?php echo $vacancie_job_type; ?>",
        "hiringOrganization" : {
            "@type" : "Organization",
            "name" : "<?php echo get_bloginfo( 'name' ); ?>",
            "sameAs" : "<?php echo site_url(); ?>",
            "logo" : "<?php echo $vacancie_logo; ?>"
        },
        "jobLocation" : {
            "@type" : "Place",
            "address" : {
                "@type" : "PostalAddress",
                "streetAddress" : "<?php ?>",
                "addressLocality" : "<?php ?>",
                "addressRegion" : "<?php echo implode( ', ', $vacancie_state ); ?>",
                "postalCode" : "<?php echo $vacancie_postalcode; ?>",
                "addressCountry": "NL"
            }
        },
        "baseSalary": {
            "@type": "MonetaryAmount",
            "currency": "<?php echo "EUR"; ?>",
            "value": {
                "@type": "QuantitativeValue",
                "value": "<?php echo $vacancie_wages; ?>",
                "unitText": "MONTH"
            }
        }
    }
</script>

<?php
// Enkele pagina
if(is_singular('vacancies')):
	$postID = get_the_ID();  
	$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all")); 
	?>
	<section id="fullwidth-image" class="blue small_padding no-image d-flex justify-content-start align-items-center">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1><?php echo the_title(); ?></h1>		
				</div>
			</div>
		</div>
	</section>

	<section id="vacancie">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8">
					<div class="content">
						<?php echo $vacancie_description; ?>
						
					</div>
					
						<?php if($form_id): ?>
							<div id="form" class="post-box blue mt-5">
								<?php gravity_form( $form_id, false, false, false, '', false ); ?>
							</div>
						<?php 	
						elseif($form_url): ?>
							<iframe id="form" width="100%" height="1000px;" src="<?php echo $form_url; ?>" height=""></iframe>
						<?php endif; ?>
				</div>
				<div class="col-12 col-lg-4 sidebar">
					<div class="sticky-top">
						<a class="btn mt-5 mb-4" href="#form">verstuur sollicitatie</a>
						<div class="sidebar-box">
							<div class="box-title">Vacature details</div>
							<div class="box-content">
								<ul class="post-list">
									
									<?php if($vacancie_state): ?>
										<li><span><strong>Regio: </strong><?php echo implode( ', ', $vacancie_state ) ?></span></li>
									<?php endif; ?>
									<li><span><strong>Functiecategorie:</strong>
										<span class="functie_cats">
											<?php foreach($term_list as $term_single):
											    echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>';
											endforeach; ?>
										</span>
									</span></li>
									<?php if($vacancie_job_type): ?>
									<li><span><strong>Dienstverband: </strong><?php echo $vacancie_job_type; ?></span></li>
									<?php endif; ?>

									<?php if($vacancie_opertunities): ?>
										<li><span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span></li>
									<?php endif; ?>

									<?php if($vacancie_experience): ?>
									<li><span><strong>Ervaring: </strong>
										<?php foreach( $vacancie_experience as $vacancie_experience_single ):
									        echo $vacancie_experience_single . ", "; 
									    endforeach; ?>
									</span></li>
									<?php endif; ?>
									<?php if($vacancie_hours): ?>
									<li><span><strong>Uren: </strong>
										<?php foreach( $vacancie_hours as $vacancie_hour ):
									        echo $vacancie_hour .", ";
									    endforeach; ?>
									</span></li>
									<?php endif; ?>
									<?php if($vacancie_start == "exacte datum"): // exacte datum?>
									<li><span><strong>Startdatum: </strong><?php echo $vacancie_date_start; ?></span></li>
									<?php else: ?>
									<li><span><strong>Startdatum: </strong><?php echo $vacancie_start; ?></span></li>
									<?php endif; ?>
									<?php if($vacancie_wages): ?>
									<li><span><strong>Salaris / Tarief: </strong>€ <?php echo $vacancie_wages; ?></span></li>
									<?php endif; ?>
									<li><span><strong>Geplaatst op: </strong><?php echo get_the_date(); ?></span></li>
								</ul>
							</div>
						</div>
						<div class="sidebar-box">
							<div class="box-title">Bedrijfsinformatie</div>
							<div class="box-content">
								<ul class="post-list">
									<?php if($vacancie_companyname): ?>
									<li><span><strong>Bij: </strong><?php echo $vacancie_companyname; ?></span></li>
									<?php endif; ?>
									<?php if($vacancie_contact_email): ?>
									<li><span><strong>Contact: </strong><a href="mailto:<?php echo $vacancie_contact_email; ?> "><?php echo $vacancie_contact_email; ?> </a></span></li>
									<?php endif; ?>
									<?php if($vacancie_companyname): ?>
									<li><span><strong>Website: </strong><a href="<?php echo $vacancie_company_website; ?>"><?php echo $vacancie_companyname; ?></a> </span></li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
						<div class="sidebar-box">
							<div class="box-title">Recruiter(s)</div>
							<div class="box-content">
								
								<?php foreach ($vacancie_recruiters as $post): ?>
									<div class="row teammember">
										<div class="col-4 col-lg-5 col-xl-4">
											<div class="thumbnail teamimage" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);">
											</div>
										</div>
										<div class="col-8  col-lg-7 col-xl-8 pl-0">
											<p class="team_name pb-0"><strong><?php echo the_title(); ?></strong><br> <?php echo the_field('team_function'); ?></p>
											<ul class="socialmedia icons-circle d-flex justify-content-start ">
												<?php if(get_field('team_phonenumber')): ?>
												<li><a href="tel:<?php echo the_field('team_phonenumber'); ?>" target="_blank"><i class="fas fa-phone"></i></i></a></li>
												<?php endif; ?>
												<?php if(get_field('team_emailaddress')): ?>
												<li><a href="mailto:<?php echo the_field('team_emailaddress'); ?>" target="_blank"><i class="far fa-envelope"></i></a></li>
												<?php endif; ?>
												<?php if(get_field('linkedin_profiel_url')): ?>
												<li><a href="<?php echo the_field('linkedin_profiel_url'); ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
												<?php endif; ?>
											</ul>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


<?php else :
// Overzicht: Wordt gebruikt bij oa. alle vacatures van een bep. categorie --> 
	if (have_posts()) : while ( have_posts() ) : the_post();
	$postID = get_the_ID();  
		$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));  
		$vacancie_companyname = get_field('vacancie_companyname',$postID);
		$vacancie_state = get_field('vacancie_state',$postID);
		$vacancie_opertunities = get_field('vacancie_opertunities',$postID);
		$vacancie_hours = get_field('vacancie_hours',$postID);
		$vacancie_job_type = get_field('vacancie_job_type',$postID);
		$vacancie_wages = get_field('vacancie_wages',$postID);
		$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));
	?>
	<div class="col-12 col-md-4">
			<div class="post">
				<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
				<div class="post-body">
					<h5 class="post-title mb-3"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
						<p class="post-details compact pb-0">
							<?php if($vacancie_companyname): ?>
							<span><strong>Bij: </strong><?php echo $vacancie_companyname; ?> </span><br>
							<?php endif; ?>

							<?php if($vacancie_opertunities): ?>
								<span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span><br>
							<?php endif; ?>
							
							<strong>Regio:</strong> <?php echo implode( ', ', $vacancie_state ) ?><br>

							<strong>Functiecategorie:</strong>
							<span class="functie_cats">
								<?php foreach($term_list as $term_single):
								    echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>';
								endforeach; ?>
							</span>
							<br>
							<strong>Uren:</strong> 
							<?php foreach( $vacancie_hours as $vacancie_hour ):
						        echo $vacancie_hour .", "; 
						    endforeach?><br>
							<strong>Type contract:</strong> <?php echo $vacancie_job_type; ?><br>
							<?php if($vacancie_wages): ?>
								<strong>Salaris:</strong> € <?php echo $vacancie_wages; ?>
							<?php endif; ?>
						</p>
						<div class="d-flex justify-content-left">
							<a href="<?php echo the_permalink(); ?>" class="btn small">Bekijk vacature</a>
						</div>
				</div>
			</div>
		</div>
	<?php endwhile; endif; ?>
<?php endif; ?>