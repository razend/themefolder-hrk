<?php $section_id = get_sub_field('section_id'); 

if ($section_id): ?>
	<div id="<?php echo $section_id; ?>"></div>
<?php endif ?>

<section id="servicepoints">
	<div class="container">
		<div class="row justify-content-around">
			<?php if( get_row_layout() == 'usps_section' ):
				if( have_rows('usps') ): while( have_rows('usps') ): the_row();
					$usp_title = get_sub_field('usp_title');
					$usp_image = get_sub_field('usp_image');?>
					<div class="col-6 col-md-4 servicepoint justify-content-center">
						<img src="<?php echo $usp_image; ?>" class="service_icon" alt="">
						<p><?php echo $usp_title; ?></p>
					</div>						
				<?php endwhile; endif; ?>
			<?php endif;	 ?>
		</div>
	</div>
</section>
