<?php 
	$company_name = get_field('company_name','option');

 ?>

<section id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php
						wp_nav_menu( array(
							'theme_location'	=> 'socials',
							'depth'				=> 1,
							'container'			=> 'div',
							'container_class'	=> 'footer-nav',
							'container_id'		=> 'footer-nav',
							'menu_class'		=> 'nav navbar-nav',
							'fallback_cb'			=> '__return_false',
							'walker'				=> new bootstrap_5_wp_nav_menu_walker(),
							'items_wrap'			=> '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
						) );
						?>
			</div>
			<div class="col-lg-9">
				<div class="d-lg-flex justify-content-end">
					© copyright <?php echo $company_name . " " . date('Y');?> 
					<span class="spacer"> | </span>
					<a href="http://frenkdesign.nl" target="_blank">ontwerp: frenkdesign</a><span class="spacer"> | </span><a href="https://razend.net">ontwikkeling: razend</a><span class="spacer"> | </span><a href="http://niqui.nu" target="_blank">teksten: niqui</a><span class="spacer"> | </span><a href="http://www.reedersphotography.myportfolio.com/" target="_blank">fotografie: reeders photography</a>
				</div>
			</div>
		</div>
	</div>
</section>