<?php 
 $address = get_field('address','option');
 $address_2 = get_field('address_2','option');
 $zipcode = get_field('zipcode','option');
 $city = get_field('city','option');
 $mobilenumber = get_field('mobilenumber','option');
 $phonenumber = get_field('phonenumber','option');
 $emailaddress = get_field('emailaddress','option');
 $company_name = get_field('company_name','option');
 ?>

		<?php if (!is_404()): ?>
		<footer>
			<div class="white-bar"></div>
			<div class="container">
				<div class="row">
					<div class="col-6 col-md-4 col-lg-2 col-xl-3">
						<h4>sitemap</h4>
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'footer_sitmap',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'footer-nav',
							'container_id'		=> 'footer-nav',
							'menu_class'		=> 'nav navbar-nav',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-3">
						<h4>algemeen</h4>
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'footer_general',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'footer-nav',
							'container_id'		=> 'footer-nav',
							'menu_class'		=> 'nav navbar-nav',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
					</div>
					<div class="col-12 col-md-4">
						<h4>contact</h4>
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<div class="contact-part">
								<span itemprop="streetAddress"><i class="fas fa-map-marker-alt float-left"></i></i><?= $address; ?><br>
								<?php if (get_field('address_2','option')):
									echo $address_2 . "<br>"; 
								endif; ?>
								</span>
								<span itemprop="postalCode" class="zipcode"><?= $zipcode ?></span>
								<span itemprop="addressLocality"><?= $city ?></span>
							</div>
							<div class="contact-part">
								<?php if(get_field('mobilenumber','option')): ?>
					 			<a href="tel:<?= $mobilenumber; ?>"><i class="fas fa-phone float-left"></i><span itemprop="telephone"><?= $mobilenumber; ?></span></a><br>
					 			<?php endif;
					 			if(get_field('phonenumber','option')): ?>
								<a href="tel:<?= $phonenumber; ?>"><i class="fas fa-phone float-left"></i><span itemprop="telephone"><?= $phonenumber; ?></span></a>
								<?php endif; ?>
							</div>
							<?php if(get_field('emailaddress','option')): ?>
							<div class="contact-part">
								<a href="mailto:<?= $emailaddress; ?>"><i class="far fa-envelope float-left"></i><span itemprop="email"><?= $emailaddress; ?></span></a>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="vertical-bar  d-none d-lg-block">
				
			</div>
			<div class="logo-area d-none d-lg-block">
				<?php the_custom_logo(); ?>
			</div>
		</footer>
		<section id="copyright">
			<div class="container">
				<div class="row">
					<div class="col-12">
						© copyright <?php echo date("Y") . $company_name; ?> <span class="spacer"> | </span><a href="http://frenkdesign.nl" target="_blank">ontwerp: frenkdesign</a> | <a href="https://razend.net" target="_blank">ontwikkeling: razend</a> | <a href="http://niqui.nu" target="_blank">teksten: niqui</a>
					</div>
				</div>
			</div>
		</section>
		<?php endif;
		if(get_field('google_script','option') == 'analytics_code'):
			get_template_part('resources/components/analytics');
		endif;
		
		if(get_field('chatprovider','option') == 'replain'):
			echo the_field('chat_replain_snippet','option');
		endif;
		wp_footer(); ?>
		<script src="<?php get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
		<script src="<?php get_template_directory_uri(); ?>/js/min/bootstrap-4-navbar.min.js"></script>
		<script type="text/javascript" src="<?php get_template_directory_uri(); ?>/js/global.js"></script>
		
	</body>
</html>