<?php 
	//Layout: multi team members
	$title = get_sub_field('title');	
	$content = get_sub_field('content');	

	$member_id = get_sub_field('selected_team_member');  
	$team_name = get_the_title($member_id);
	$team_bio = get_field('team_bio',$member_id);
	$team_function = get_field('team_function',$member_id);
	$thumb_url = get_the_post_thumbnail_url($member_id); 
	$thumb_title = get_the_post_thumbnail_url(['alt' => get_the_title()]); 
?>
<style>
	@media (min-width: 992px) {
		.team_section.layout3 .container {
			--bg-image: url(<?php echo $thumb_url; ?>);
			background-image: var(--bg-image);	
			background-repeat: no-repeat; 
			background-position: right bottom; 
			background-size: contain;
		}
	}
</style>
<div class="container" style="">
	<div class="row">
		<div class="col-12 col-lg-8">
			<div class="content">
				<h2><?php echo $title; ?></h2>
				<p>
					<strong>naam: </strong><?php echo $team_name; ?><br>
					<strong>functie: </strong><?php echo $team_function; ?>
				</p>
				<?php echo $team_bio; ?>
			</div>
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>