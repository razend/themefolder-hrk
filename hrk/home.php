<?php 
	get_header();
	$headerImage = get_field('ArchiveHeaderImage','option'); 
	$headerImageTitle = get_field('title_in_image_top','option');
	$page_for_posts = get_option( 'page_for_posts' );
	$kb_intro = get_field('knowledgebase_intro','option');
?>

<section id="fullwidth-image" class="<?php echo $headerImage['value']; ?> less-height d-flex justify-content-start align-items-center">
	<h1><?php echo $headerImageTitle; ?></h1>
	<div class="white-bar"></div>
</section>

<?php get_template_part('resources/components/breadcrumbs'); ?>

<section id="knowledgebase" class="pt-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
					<?php echo $kb_intro; ?>
			</div>

			<?php get_template_part('resources/components/category-selector');
			
			get_template_part('resources/components/knowledgebase');
				
			get_template_part('resources/components/pagination'); ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>