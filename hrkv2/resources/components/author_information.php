<?php
// Set the Current Author Variable $curauth
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
	$author_id = get_the_author_meta('ID');
	$team_id = get_field('user_teammember_id','user_'. $author_id);
	$team_function = get_field('team_function',$team_id);
	$team_bio = get_field('team_bio',$team_id);
	$icon_or_text = get_field('icon_or_text','option');
?>
<div class="row author_block">	
	<div class="col-4 col-md-3 col-xl-2">
		<div class="author-photo">
			<?php get_avatar( $curauth->user_email , '260 '); ?>
		</div>
	</div>
	<div class="col-8 col-md-9 col-xl-10">
		<div class="author_details">
			<div class="author_function"><?php echo $team_function; ?></div>
			<div class="author_name"><?php echo $curauth->nickname; ?></div> 
			<div class="author_bio"><?php echo $team_bio; ?></div>
		</div>
		<ul class="socialmedia ml-auto <?php if($icon_or_text === 'icon'){ echo  "icons-circle"; } ?> d-flex justify-content-end">
			<?php if( have_rows('user_social_media_networks',$team_id) ): while( have_rows('user_social_media_networks',$team_id) ) : the_row();
				$social_media_url = get_sub_field('social_media_url',$team_id);
				$icon = get_sub_field('icon',$team_id); ?>
				<li>
					<a href="<?php echo $social_media_url; ?>" target="_blank">
						<i class="<?php echo $icon; ?>"></i>
					</a>
				</li>
			<?php endwhile; endif; ?>
		</ul>
	</div>
</div>