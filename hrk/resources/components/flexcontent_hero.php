<?php
	$section_id = get_sub_field('section_id');
	$section_class = get_sub_field('section_class');
	$title = get_sub_field('title');
	$text = get_sub_field('content');
	$section_img = get_sub_field('section_img');
	$section_position = get_sub_field('background_position');
	$section_height_choise = get_sub_field('section_height_choise');
	$show_colored_space = get_sub_field('show_colored_space');
	if($section_height_choise == 'custom'):
	$section_height = get_sub_field('section_height_px');
	elseif($section_height_choise == 'auto'):
	$section_height = 'auto';
	endif;
	$cta_button_align = get_sub_field('cta_button_alignment');
?>
<?php if ($section_id): ?>
	<div id="<?php echo $section_id; ?>"></div>
<?php endif ?>

<?php if($show_colored_space == 'show_colored_space'): ?>
<section id="fullwidth-image" class="d-flex hero-section justify-content-center align-items-center <?php echo $section_class . " " . $show_colored_space; ?>" style="background-image: url(<?php echo $section_img; ?>); background-repeat: no-repeat; height: <?php echo $section_height; ?>px;">
<?php else : ?>
<section id="fullwidth-image" class="d-flex hero-section justify-content-start align-items-center <?php echo $section_class; ?>" style="background-image: url(<?php echo $section_img; ?>); background-position:<?php echo $section_position; ?>; background-repeat: no-repeat; background-size: cover; height: <?php echo $section_height; ?>px;">
<?php endif; ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="header-title"><?php echo $title; ?></div> 
					<?php echo $text; ?>
					<?php if( have_rows('cta_adding')): ?>
					<div class="text-<?php echo $cta_button_align; ?> cta-buttons">
						<?php while ( have_rows('cta_adding') ) : the_row();
							$cta_button_text = get_sub_field('cta_button_text');
							$cta_button_link = get_sub_field('cta_button_link');
							$cta_button_target = get_sub_field('cta_button_target');
						?>
						<a class="btn" href="<?php echo $cta_button_link; ?>" target="<?php echo $cta_button_target; ?>"><?php echo $cta_button_text; ?></a>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		
	<div class="white-bar"></div>
</section>	