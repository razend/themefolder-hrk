<?php 
$recruiter = get_field('vacancie_recruiters');
$recruiter_phone = get_field('team_phonenumber',$recruiter->ID);
$recruiter_email = get_field('team_emailaddress',$recruiter->ID);
$recruiter_linkedin = get_field('linkedin_profiel_url',$recruiter->ID);
 ?>

<?php if($recruiter_phone): ?>
	<a href="tel:<?php echo str_replace(' ', '', $recruiter_phone); ?>" target="_blank" title="Bel <?php echo ( $recruiter->post_title ); ?>"><i class="fas fa-phone"></i></a>
<?php endif;

if($recruiter_email): ?>
	<a href="mailto:<?php echo $recruiter_email; ?>" target="_blank" title="Stuur <?php echo ( $recruiter->post_title ); ?> een e-mail"><i class="fas fa-envelope"></i></a>
<?php endif;

if($recruiter_linkedin): ?>
	<a href="<?php echo $recruiter_linkedin; ?>" target="_blank" title="Bekijk <?php echo ( $recruiter->post_title ); ?> LinkedIn pagina"><i class="fa-brands fa-linkedin-in"></i></a>
<?php endif;

if($recruiter_phone): ?>
	<a href="https://wa.me/<?php echo str_replace(' ', '', $recruiter_phone); ?>" target="_blank" title="Stuur <?php echo ( $recruiter->post_title ); ?> een Whatsapp bericht"><i class="fa-brands fa-whatsapp"></i></a>
<?php endif; ?>