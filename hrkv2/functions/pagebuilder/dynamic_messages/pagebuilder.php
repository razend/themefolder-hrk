<?php 

// Section background image
add_action('acf/render_field/name=dm_section_bg_image', 'dm_section_bg_image');
function dm_section_bg_image(){
    echo 'Aanbovelen afmeting van je afbeelding is een breedte van 1366 pixels.<br><br> 
			Toegestaane formaten: jpg, jpeg en png<br>
			Aanbovelen breedte: 1366 pixels';
}

// Section background image position
add_action('acf/render_field/name=dm_section_bg_image_position', 'dm_section_bg_image_position');
function dm_section_bg_image_position(){
    echo 'De afbeelding staat zodaning ingesteld dat deze altijd een volledige breedte van de sectie vult. 
Met onderstaande instelling kun je aangeven of je de afbeelding aan de bovenkant, in het midden, of aan de onderkant wilt uitlijnen.';
}

// Section background image effect
add_action('acf/render_field/name=dm_section_bg_image_effect', 'dm_section_bg_image_effect');
function dm_section_bg_image_effect(){
    echo 'Geef je hier niets op, dan wordt de achtergrond wit.';
}

// Section background color
add_action('acf/render_field/name=dm_section_bg_color', 'dm_section_bg_color');
function dm_section_bg_color(){
    echo 'Selecteer een effect voor de achtergrond afbeelding';
}

// Hero: Section height
add_action('acf/render_field/name=dm_sectionheight', 'dm_sectionheight');
function dm_sectionheight(){
    echo 'Dit is het percentage van de hoogte van het browservenster. Bij automatisch wordt dit op basis van inhoud van dit blok berekend. ';
}

// Hero: Content alignment
add_action('acf/render_field/name=dr_hero_content_alignment', 'dr_hero_content_alignment');
function dr_hero_content_alignment(){
    echo 'Hiermee stel je de uitlijning van de tekst over de hele sectie in.';
}


// Gallery layout
add_action('acf/render_field/name=dm_layout_gallery', 'dm_layout_gallery');
function dm_layout_gallery(){
    echo 'Raster: allemaal gelijk vierkante foto\'s<br>
    	Metselwerk: de foto\'s behouden hun aspectratio waardoor je een speelser effect krijgt.';
}

// Photorow 
add_action('acf/render_field/name=dm_photo_selection', 'dm_photo_selection');
function dm_photo_selection(){
    
    echo 'Met dit blok kun je een serie (sfeer)foto\'s laten zien. Dit wordt vaker gebruikt om een 2 andere blokken met een witte achtergrond van elkaar te scheiden. Je kunt hier meerdere foto\'s selecteren, deze worden automatisch verdeeld onder de volledige breedte van het scherm met een maximale hoogte van 290pixels.<br><br>

Op kleine schermen zullen alleen de eerste 2 foto\'s zichtbaar zijn, op middelgrote schermen enkel de eerste 3, op grote schermen de eerste 4 en op extra grote schermen alle foto\'s';   
}

// Carousel
add_action('acf/render_field/name=dm_slider_effect', 'dm_slider_effect');
function dm_slider_effect(){
    
    echo 'Met deze instelling kun je kiezen of je de volgende slide naar links laat schuiven of dat ze door elkaar heen faden.';   
}
add_action('acf/render_field/name=dm_slider_interval', 'dm_slider_interval');
function dm_slider_interval(){
    
    echo 'Hiermee kun je instellen hoe snel de volgende slide verschijnt. Dit kun je opgeven in miliseconden. (1sec = 1000 millisecondes)';   
}
add_action('acf/render_field/name=dm_slider_indicators', 'dm_slider_indicators');
function dm_slider_indicators(){
    
    echo 'Indicators zijn de kleine knopjes onderaan de slide.';   
}
add_action('acf/render_field/name=dm_slider_controls', 'dm_slider_controls');
function dm_slider_controls(){
    
    echo 'Met deze optie kun je de pijltjes naar links en rechts verbergen.';   
}
add_action('acf/render_field/name=dm_carousel_overlay', 'dm_carousel_overlay');
function dm_carousel_overlay(){
    
    echo 'Met deze optie activeer je een (transparant)gekleurd vlak over de afbeelding. Je kunt dit ook gebruiken als normale achtergrond kleur van dit blok.';   
}

// Dynamische content +
add_action('acf/render_field/name=dm_linked_title', 'dm_linked_title');
function dm_linked_title(){
    
    echo 'Met deze optie wordt de titel klikbaar naar het het betreffende item.';   
}
add_action('acf/render_field/name=dm_linked_image', 'dm_linked_image');
function dm_linked_image(){
    
    echo 'Met deze optie wordt de afbeelding klikbaar gemaakt naar het betreffende item.';   
}
add_action('acf/render_field/name=dm_button_text', 'dm_button_text');
function dm_button_text(){
    
    echo 'Wil je een knop onder ieder item? Vul dan hier de tekst die je de knop wilt hebben of laat dit veld leeg om de knoppen te verbergen.';   
}


// Forms
add_action('acf/render_field/name=dm_form_title', 'dm_form_title');
function dm_form_title(){
    
    echo 'Zet een titel boven je formulier';   
}

add_action('acf/render_field/name=dm_form_id', 'dm_form_id');
function dm_form_id(){
    
    echo 'Geef hiet het ID op van het formulier wat je hier wilt laten zien. Het ID vind je in het menu formulieren.';   
}

add_action('acf/render_field/name=dm_form_bg_color', 'dm_form_bg_color');
function dm_form_bg_color(){
    
    echo 'Geef je formulier een achtergrond kleurtje zodat het meer opvalt.';   
}

// Blokken met icoontje
add_action('acf/render_field/name=dm_fa_icon', 'dm_fa_icon');
function dm_fa_icon(){
    
    echo 'Via <a href="https://fontawesome.com">fontawesome.com</a> kun je kiezen uit duizenden icoontjes. Wanneer je een icoontje hebt gevonden kopieer je de regel bv <code>&lt;i class="far fa-clock"&gt;&lt;/i&gt;</code> in onderstaand veld. Je kunt enkel de niet-pro icoontjes gebruiken. Werkt het icoontje niet goed, neem dan even contact met ons op, voor behoud van de snelheid van je website hebben we niet alle icoontjes geactiveerd.';   
}

// Accordion
add_action('acf/render_field/name=dm_accordion_behavior', 'dm_accordion_behavior');
function dm_accordion_behavior(){
    
    echo 'Mag er tegelijkertijd maar een toggle actief zijn en de anderen verborgen zijn of kunnen meerdere toggles open zijn op hetzelfde moment?';   
}

add_action('acf/render_field/name=dm_accordion_icons', 'dm_accordion_icons');
function dm_accordion_icons(){
    
    echo "Wil je <span class='dashicons dashicons-plus'></span> / <span class='dashicons dashicons-minus'></span> of  <span class='dashicons dashicons-arrow-up-alt2'></span> / <span class='dashicons dashicons-arrow-down-alt2'></span> bij een  gesloten / open toggle?";   
}

add_action('acf/render_field/name=dm_acordion_border_color', 'dm_acordion_border_color');
function dm_acordion_border_color(){
    
    echo "Hier kun je een andere kleur selecteren. Liever geen kleur, kies dan dezelfde kleur als de achtergrond van je sectie.";   
}

add_action('acf/render_field/name=dm_accordion_bg_color', 'dm_accordion_bg_color');
function dm_accordion_bg_color(){
    
    echo "Kies hier een kleur voor de achtergrond van de accordion";   
}

// Logo row / slider
add_action('acf/render_field/name=logo_tip', 'logo_tip');
function logo_tip(){
    
    echo "Voor het mooiste effect zorg je ervoor dat de afbeeldingen exact dezelfde pixelverhouding hebben en dat ze mooi zijn gecentreerd in de afbeelding. Advies afmeting: 680*435 pixels";   
}

add_action('acf/render_field/name=dr_number_of_logo_rows', 'dr_number_of_logo_rows');
function dr_number_of_logo_rows(){
    
    echo "Dit zijn het aantal rijen die altijd zichtbaar zijn boven de knop om de andere logo's uit te klappen. ";   
}

// vrij tekst veld
add_action('acf/render_field/name=dm_smaller_on_large_screens', 'dm_smaller_on_large_screens');
function dm_smaller_on_large_screens(){
    
    echo "Soms ziet het er beter uit wanneer je een grote lap tekst wat smaller maakt op grote schermen. ";   
}

// vrij tekst veld met afbeelding
add_action('acf/render_field/name=dm_text_image_choise', 'dm_text_image_choise');
function dm_text_image_choise(){
    
    echo "Wil je een afbeelding of video plaatsen?";   
}

add_action('acf/render_field/name=dm_text_media_image', 'dm_text_media_image');
function dm_text_media_image(){
    
    echo "Selecteer hier een afbeelding van ± 1000x1000 pixels in jpg, jpeg of png formaat.";   
}
add_action('acf/render_field/name=dm_text_media_video', 'dm_text_media_video');
function dm_text_media_video(){
    
    echo "Vul hier enkel de unieke video id in;<br>https://www.youtube-nocookie.com/embed/<strong>S2x6q_eNYyo</strong><br>https://vimeo.com/<strong>406474753</strong>";   
}

// Contact 


?>