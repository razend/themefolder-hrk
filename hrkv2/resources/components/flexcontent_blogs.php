<?php
	// 30-8-21: blade engine opmaak verwijderd. 
$block_settings = get_sub_field('block_settings');
$blog_selection = get_sub_field('blog_selection');
$select_cat = get_sub_field('select_cat');
$bg_color = get_sub_field('bg_color');
$section_id = get_sub_field('section_id');
$section_class = get_sub_field('section_class');

?>
<?php if($section_id): ?>
<section id="<?php echo $section_id; ?>" class="p-0" ></section>
<?php endif; ?>



<section id="knowledgebase" class="<?php echo $section_class; ?> ">
	<div class="container">
		<div class="row">
			<?php if($block_settings == "selection"): ?>
				<?php foreach( $blog_selection as $post ): 
				 setup_postdata($post); 
				 ?>
				 <div class="col-12 col-lg-4">
					<div class="card">
						<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
						<div class="card-body">
							<p class="card-details pb-0">
								
							</p>
							<h5 class="card-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
							<p class="card-text"><?php echo get_the_excerpt(); ?></p>
							<div class="d-flex justify-content-center">
								<a href="<?php echo the_permalink(); ?>" class="btn filled small rounded-corners">lees verder</a>
							</div>
						</div>
						<div class="card-footer <?php echo $bg_color; ?>">
							<?php if(has_category('',$post->ID)): ?>
								<div class="category_list">
									<strong>categorie:</strong>
									<?php echo the_category(', '); ?>
								</div>
							<?php endif; ?>
							<?php if(has_tag()): ?>
								<?php the_tags( '<strong>tags</strong>: ', ); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

			<?php elseif($block_settings == "all_from_posttype"): 


				$args;
				$the_query = new WP_Query(array('cat' => $select_cat, 'posts_per_page' => 3));
				if ( $the_query->have_posts() ) :
					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					

					<div class="col-12 col-lg-4">
					<div class="card">
						<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
						<div class="card-body">
							<p class="card-details pb-0">
								
							</p>
							<h5 class="card-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
							<p class="card-text"><?php echo get_the_excerpt(); ?></p>
							<div class="d-flex justify-content-center">
								<a href="<?php echo the_permalink(); ?>" class="btn filled small rounded-corners">lees verder</a>
							</div>
						</div>
						<div class="card-footer <?php echo $bg_color; ?>">
							<div class="category_list">
								<strong>categorie:</strong>
								<?php echo the_category(', '); ?>
							</div>
							<?php the_tags( '<strong>tags</strong>: ', ); ?>
						</div>
					</div>
				</div>	

				<?php endwhile; endif;
				wp_reset_postdata();
			endif; ?>
		</div>
		<?php if ($select_cat): ?>
		<div class="row">
			<div class="col-12 d-flex justify-content-center">
				<a class="btn base" href="<?php echo get_category_link( $select_cat ); ?>">alle artikelen in <?php echo get_cat_name($select_cat); ?></a>
			</div>
		</div>
		<?php endif ?>
	</div>
</section>

<?php wp_reset_query(); ?>