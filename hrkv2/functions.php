<?php 
// CSS laden in header

function mytheme_enqueue_style() {

    wp_enqueue_style( 'HetRecruitingKantoor', get_stylesheet_uri(), false, '2.0.25', false ); 
    wp_enqueue_script( 'Style' );
}
add_action( 'wp_enqueue_scripts', 'mytheme_enqueue_style' );

remove_action( 'wp_enqueue_scripts', 'wp_enqueue_classic_theme_styles' );
// include custom jQuery
// function include_custom_jquery() {

// 	wp_deregister_script('jquery');
// 	wp_enqueue_script('jquery', get_template_directory_uri() .'/js/jquery.min.js', array(), null, false);

// }
// add_action('wp_enqueue_scripts', 'include_custom_jquery');


// Menu's
require_once get_template_directory() . '/functions/bootstrap_5_wp_nav_menu_walker.php';

// Load menu's
function loadMenu() {
  register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'HRK' ),
	'footer_sitmap' => __( 'Footer Sitemap', 'HRK' ),
	'footer_general' => __( 'Footer Algemeen', 'HRK' ),
	'about' => __( 'Over HetRecruitingKantoor', 'HRK' ),
	'jargonbuster' => __( 'Jargonbuster', 'HRK' ),
	'kennisbank_archive' => __( 'Kennisbank archief pagina', 'HRK' ),
	'kennisbank' => __( 'Kennisbank', 'HRK' ),
	'socials' => __( 'Socials', 'HRK' ),
) );
}
add_action( 'init', 'loadMenu' );

add_filter('manage_posts_columns', 'posts_columns_id', 5);
add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);
add_filter('manage_pages_columns', 'posts_columns_id', 5);
add_action('manage_pages_custom_column', 'posts_custom_id_columns', 5, 2);

function posts_columns_id($defaults){
	$defaults['wps_post_id'] = __('ID');
	return $defaults;
}
function posts_custom_id_columns($column_name, $id){
	if($column_name === 'wps_post_id'){
		echo $id;
	}
}

// ACF stuff
include( get_template_directory() . '/functions/acf-theme-settings.php' ); // code voor ACF options pagina
include( get_template_directory() . '/functions/acf.php' );
include( get_template_directory() . '/functions/acf_business_settings.php' );
include( get_template_directory() . '/functions/acf-meetingplanner.php' );

// Thema opties
include( get_template_directory() . '/functions/pagebuilder/group_5c9b9045456ee.php' ); 
include( get_template_directory() . '/functions/pagebuilder/group_57b5849465c04.php' ); // Bedrijfsgegevens

// pagebuilder
include( get_template_directory() . '/functions/pagebuilder/group_5e6638e468e57.php' ); // pagebuilder
include( get_template_directory() . '/functions/pagebuilder/group_6124e537582c5.php' ); // Kleurcodes

// Pagebuilder V2
include( get_template_directory() . '/functions/pagebuilder/group_63299f66ec074.php' ); // pagebuilder V2
include( get_template_directory() . '/functions/pagebuilder/group_6329a1569743a.php' ); // Global clones
include( get_template_directory() . '/functions/pagebuilder/group_636cb87ebc68c.php' ); // Carousel
include( get_template_directory() . '/functions/pagebuilder/group_63ac5df5d382e.php' ); // Contact
include( get_template_directory() . '/functions/pagebuilder/group_633aa0dddf0af.php' ); // Dynamische inhoud
include( get_template_directory() . '/functions/pagebuilder/group_6329a07216004.php' ); // hero
include( get_template_directory() . '/functions/pagebuilder/group_634ff0c2f30d5.php' ); // Logo's
include( get_template_directory() . '/functions/pagebuilder/group_633d7a2e982d1.php' ); // Quoteslider
include( get_template_directory() . '/functions/pagebuilder/group_63aac7cb32978.php' ); // USP's
include( get_template_directory() . '/functions/pagebuilder/group_63b2e39c307d4.php' ); // Team blok
include( get_template_directory() . '/functions/pagebuilder/group_634406f3d78ea.php' ); // Vrij tekstveld met afbeelding / video
include( get_template_directory() . '/functions/pagebuilder/group_6329cf263e92e.php' ); // Vrij tekstveld

// Vacatures
include( get_template_directory() . '/functions/pagebuilder/group_5c9cbd705d382.php' ); // Vacatures

// Diensten
include( get_template_directory() . '/functions/pagebuilder/group_63b2d8e2cf217.php' ); // Diensten

// Team
include( get_template_directory() . '/functions/pagebuilder/group_5cc19d3fb6fdf.php' ); // Team 

include( get_template_directory() . '/functions/pagebuilder/group_5c9b9b541105c.php' ); // Diensten / Werk ACF
// include( get_template_directory() . '/functions/pagebuilder/group_5cb83defbabdb.php' ); // Grote foto's
include( get_template_directory() . '/functions/pagebuilder/group_5e7606c883f14.php' ); // Meetingbanner

// include( get_template_directory() . '/functions/pagebuilder/group_5c9b8bd5e8f06.php' ); // Grote koppen
include( get_template_directory() . '/functions/pagebuilder/group_60509ee1ea185.php' ); // Referenties

// Dynamic Messages pagebuilder
include( get_template_directory() . '/functions/pagebuilder/dynamic_messages/pagebuilder.php' );
include( get_template_directory() . '/functions/pagebuilder/dynamic_messages/theme_options.php' );
include( get_template_directory() . '/functions/pagebuilder/dynamic_messages/clonefields.php' );
include( get_template_directory() . '/functions/pagebuilder/hooks/pagebuilder-thumbnail.php' );


// Wordpress cleaning
include( get_template_directory() . '/functions/wp_cleaning.php' );

// Gravity Forms customization
include( get_template_directory() . '/functions/gf-custom.php' );

// Wordpress post duplicator
include( get_template_directory() . '/functions/post_duplicator.php' );


// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);
//kill Gutenberg stylesheet
function wp_dequeue_gutenberg_styles() {
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
}
add_action( 'wp_print_styles', 'wp_dequeue_gutenberg_styles', 100 );

// Custom theme stuff
add_theme_support( 'custom-logo' );

// Custom post types
add_action( 'init', 'create_post_type' );
function create_post_type() {
	// register_post_type( 'services',
	// 	array(
	// 		'labels' 				=> array(
	// 			'name' 				=> __( 'Diensten' ),
	// 			'singular_name' 	=> __( 'Dienst' ),
	// 			'add_new'		 	=> __( 'Nieuwe dienst'),
	// 			'view_items'		=> __( 'Alle diensten'),
	// 			'view_item'			=> __( 'Bekijk dienst'),
	// 			'all_items'			=> __( 'Alle diensten'),
	// 			'add_new_item'		=> __( 'Nieuwe dienst toevoegen'),
	// 		),
	// 		'public'				=> true,
	// 		'has_archive' 			=> true,
	// 		'supports' 				=> array('title','editor','publicize','thumbnail','post-formats'),
	// 		'rewrite' 				=> array('slug' => 'diensten'),
	// 		'taxonomies' 			=> array('topics'),
	// 		'menu_icon'				=> 'dashicons-megaphone',
	// 	)
	// );

	register_post_type( 'team',
		array(
			'labels' 				=> array(
				'name' 				=> __( 'HRK team' ),
				'singular_name' 	=> __( 'Team' ),
				'add_new'		 	=> __( 'Nieuw teamlid'),
				'view_items'		=> __( 'Alle teamleden'),
				'view_item'			=> __( 'Bekijk teamlid'),
				'all_items'			=> __( 'Alle teamleden'),
				'add_new_item'		=> __( 'Nieuwe teamlid toevoegen'),
			),
			'public'				=> true,
			'has_archive' 			=> false,
			'supports' 				=> array('title','editor','publicize','thumbnail','post-formats'),
			'rewrite' 				=> array('slug' => 'teamleden'),
			'taxonomies' 			=> array('topics', 'category' ),
			'menu_icon'				=> 'dashicons-admin-users',
		)
	);

	// register_post_type( 'cases',
	// 	array(
	// 		'labels' 				=> array(
	// 			'name' 				=> __( 'Cases' ),
	// 			'singular_name' 	=> __( 'Case' ),
	// 			'add_new'		 	=> __( 'Nieuwe case'),
	// 			'view_items'		=> __( 'Alle cases'),
	// 			'view_item'			=> __( 'Bekijk case'),
	// 			'all_items'			=> __( 'Alle cases'),
	// 			'add_new_item'		=> __( 'Nieuwe case toevoegen'),
	// 		),
	// 		'public'				=> true,
	// 		'has_archive' 			=> true,
	// 		'supports' 				=> array('title','editor','publicize','thumbnail','post-formats'),
	// 		'rewrite' 				=> array('slug' => 'cases'),
	// 		'taxonomies' 			=> array('topics', 'category' ),
	// 		'menu_icon'				=> 'dashicons-archive',
	// 	)
	// );
	// register_post_type( 'testimonials',
	// 	array(
	// 		'labels' 				=> array(
	// 			'name' 				=> __( 'Testimonials' ),
	// 			'singular_name' 	=> __( 'testimonial' ),
	// 			'add_new'		 	=> __( 'Nieuwe testimonial'),
	// 			'view_items'		=> __( 'Alle testimonials'),
	// 			'view_item'			=> __( 'Bekijk testimonial'),
	// 			'all_items'			=> __( 'Alle testimonials'),
	// 			'add_new_item'		=> __( 'Nieuwe testimonial toevoegen'),
	// 		),
	// 		'public'				=> true,
	// 		'has_archive' 			=> false,
	// 		'supports' 				=> array('title','editor','publicize','thumbnail','post-formats'),
	// 		'rewrite' 				=> array('slug' => 'testimonial'),
	// 		'taxonomies' 			=> array('topics' ),
	// 		'publicly_queryable'	=> false,
	// 		'menu_icon'				=> 'dashicons-testimonial',
	// 	)
	// );
	// register_post_type( 'references',
	// 	array(
	// 		'labels' 				=> array(
	// 			'name' 				=> __( 'Referenties' ),
	// 			'singular_name' 	=> __( 'Referentie' ),
	// 			'add_new'		 	=> __( 'Nieuwe referentie'),
	// 			'view_items'		=> __( 'Alle referenties'),
	// 			'view_item'			=> __( 'Bekijk referentie'),
	// 			'all_items'			=> __( 'Alle referenties'),
	// 			'add_new_item'		=> __( 'Nieuwe referentie toevoegen'),
	// 		),
	// 		'public'				=> true,
	// 		'has_archive' 			=> false,
	// 		'supports' 				=> array('title','editor','publicize','thumbnail','post-formats'),
	// 		'rewrite' 				=> array('slug' => 'references'),
	// 		'taxonomies' 			=> array('topics' ),	
	// 		'menu_icon'				=> 'dashicons-awards',
	// 	)
	// );

	// register_post_type( 'work',
	// 	array(
	// 		'labels' 				=> array(
	// 			'name' 				=> __( 'Werken' ),
	// 			'singular_name' 	=> __( 'Werk' ),
	// 			'add_new'		 	=> __( 'Nieuwe Werken pagina'),
	// 			'view_items'		=> __( 'Alle Werken paginas'),
	// 			'view_item'			=> __( 'Bekijk pagina'),
	// 			'all_items'			=> __( 'Alle Werken paginas'),
	// 			'add_new_item'		=> __( 'Nieuwe Werken pagina toevoegen'),
	// 		),
	// 		'public'				=> true,
	// 		'has_archive' 			=> true,
	// 		'supports' 				=> array('title','editor','publicize','thumbnail','post-formats'),
	// 		'rewrite' 				=> array('slug' => 'werken'),
	// 		'taxonomies' 			=> array('topics' ),
	// 		'menu_icon'				=> 'dashicons-businessman',
	// 	)
	// );
}

include( get_template_directory() . '/functions/plugins/vacancies.php' ); // Vacatures


// Shortcodes
function category_selector() {
	ob_start();
	include(get_stylesheet_directory() . '/resources/shortcodes/kennisbank_categoryselector.php');
	return ob_get_clean();
}
add_shortcode( 'categorie_menu', 'category_selector' );


function live_hero() {
	ob_start();
	include(get_stylesheet_directory() . '/resources/shortcodes/live-hero.php');
	return ob_get_clean();
}
add_shortcode( 'live-hero', 'live_hero' );



remove_filter( 'user_contactmethods', array( ! empty( $GLOBALS['wpseo_admin'] ) ? $GLOBALS['wpseo_admin'] : '', 'update_contactmethods' ), 10 );

// Featured image for custom post type
add_theme_support('post-thumbnails');

// Show which file is used
function meks_which_template_is_loaded() {
    if ( is_super_admin() ) {
        global $template;
        print_r( $template );
    }
}
 
/* Uitgelichte afbeeldingen toevoegen aan de RSS-feed */
function featuredtoRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '' . get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'float:left; margin:0 15px 15px 0;' ) ) . '' . $content;
}
return $content;
}

add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');


add_action( 'wp_footer', 'meks_which_template_is_loaded' );

/*
 * Set post views count using post meta
 */
function setPostViews($postID) {
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}


// // Exclude category from the_category
// function the_category_filter($thelist,$separator=' ') {
// 	if(!defined('WP_ADMIN')) {
// 		//Category IDs to exclude
// 		$exclude = array(35);
		
// 		$exclude2 = array();
// 		foreach($exclude as $c) {
// 			$exclude2[] = get_cat_name($c);
// 		}
		
// 		$cats = explode($separator,$thelist);
// 		$newlist = array();
// 		foreach($cats as $cat) {
// 			$catname = trim(strip_tags($cat));
// 			if(!in_array($catname,$exclude2))
// 				$newlist[] = $cat;
// 		}
// 		return implode($separator,$newlist);
// 	} else {
// 		return $thelist;
// 	}
// }
// add_filter('the_category','the_category_filter', 10, 2);

// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

// add_filter( 'gform_confirmation_anchor', '__return_true' );

/*
 * PHP Sync: Save Path
 */
add_filter('acfe/settings/php_save', 'my_acfe_php_save_point');
function my_acfe_php_save_point($path){
    
    return get_template_directory() . '/functions/pagebuilder';
    
}

/*
 * PHP Sync: Load Path
 */
add_filter('acfe/settings/php_load', 'my_acfe_php_load_point');
function my_acfe_php_load_point($paths){
    
    // Append path
    $paths[] = get_template_directory() . '/functions/pagebuilder';
    
    // Return
    return $paths;
    
}


// devbar
function which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		$template_name = basename( $template, '.php' );
		$template_dir  = basename ( dirname( $template ) );
		$theme = wp_get_theme();
		?>
		<div class="dev-bar">
			<?php echo "Template dir: themes/" . $template_dir . "/" . $template_name . ".php -" . " Thema: " . $theme; ?>
		</div>
	<?php }
}
 
add_action( 'wp_footer', 'which_template_is_loaded' );

// show which template file is used
add_filter( 'manage_pages_columns', 'page_column_views' );
add_action( 'manage_pages_custom_column', 'page_custom_column_views', 5, 2 );
function page_column_views( $defaults )
{
   $defaults['page-layout'] = __('Template');
   return $defaults;
}
function page_custom_column_views( $column_name, $id )
{
   if ( $column_name === 'page-layout' ) {
       $set_template = get_post_meta( get_the_ID(), '_wp_page_template', true );
       if ( $set_template == 'default' ) {
           echo 'Default';
       }
       $templates = get_page_templates();
       ksort( $templates );
       foreach ( array_keys( $templates ) as $template ) :
           if ( $set_template == $templates[$template] ) echo $template;
       endforeach;
   }
}
?>