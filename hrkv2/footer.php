<?php 
	$address = get_field('address','option');
	$zipcode = get_field('zipcode','option');
	$city = get_field('city','option');
	$phonenumber = get_field('phonenumber','option');
	$emailaddress = get_field('emailaddress','option');
	$footer_text = get_field('footer_text','option');
	$google_script = get_field('google_script','option');
	$chat_provider = get_field('chatprovider','option');
	$chat_replain_snippet = get_field('chat_replain_snippet','option');
 ?>

<?php if (!is_404()){ ?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					<h4><?php echo $footer_text; ?></h4>
				</div>
				<div class="col-12 col-xs-6 col-md-3">
					<?php
					wp_nav_menu( array(
						'theme_location'	=> 'footer_sitmap',
						'depth'				=> 1,
						'container'			=> 'div',
						'container_class'	=> 'footer-nav',
						'container_id'		=> 'footer-nav',
						'menu_class'		=> 'nav navbar-nav',
						'fallback_cb'			=> '__return_false',
						'walker'				=> new bootstrap_5_wp_nav_menu_walker(),
						'items_wrap'			=> '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
					) );							
					?>
				</div>
				<div class="col-12 col-xs-6 col-md-3 mt-3 mt-sm-0">
					<?php
					wp_nav_menu( array(
						'theme_location'	=> 'footer_general',
						'depth'				=> 1,
						'container'			=> 'div',
						'container_class'	=> 'footer-nav',
						'container_id'		=> 'footer-nav',
						'menu_class'		=> 'nav navbar-nav',
						'fallback_cb'			=> '__return_false',
						'walker'				=> new bootstrap_5_wp_nav_menu_walker(),
						'items_wrap'			=> '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
					) );
					?>
					<p></p>
					<p>
					<?php echo $address; ?><br>
					<?php echo $zipcode; ?> <?php echo $city; ?><br>
					<?php echo $phonenumber; ?><br>
					<a href="<?php echo $emailaddress; ?>"><?php echo $emailaddress; ?></a>
					</p>
				</div>
			</div>
		</div>
	</footer>

	<?php get_template_part('resources/views/layouts/copyright_footer');

}

		if($google_script === 'analytics_code'){
			get_template_part('resources/components/analytics');
		}
		
		if($chat_provider === 'replain'){
			echo $chat_replain_snippet;
		}
		wp_footer();?>
		
		<script src="<?php echo get_template_directory_uri(); ?>/js/min/bootstrap.bundle.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/global.js"></script>
		
	</body>
</html>