<?php 
	$company_name = get_field('company_name','option');
?>

<section id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-12">
				© copyright <?php echo date('Y') . $company_name;?> <span class="spacer"> | </span><a href="http://frenkdesign.nl" target="_blank">ontwerp: frenkdesign</a> | <a href="https://razend.net">ontwikkeling: razend</a> | <a href="http://niqui.nu" target="_blank">teksten: niqui</a> | <a href="http://www.reedersphotography.myportfolio.com/" target="_blank">fotografie: reeders photography</a>
			</div>
		</div>
	</div>
</section>