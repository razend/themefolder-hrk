<?php 
$blog_title = get_bloginfo(); 
$blog_description = get_bloginfo('description');
$page_title = get_the_title();
$google_script = get_field('google_script','option');
$google_tm_head = get_field('google_tagmanager_code_snippet_head','option');
$google_tm_body = get_field('google_tagmanager_code_snippet_body','option');
$chatprovider = get_field('chatprovider','option');
$chat_zendesk_snippet = get_field('chat_zendesk_snippet','option');

?><!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="utf-8">
	<title><?php echo $page_title . " | " . $blog_title . " - " . $blog_description; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
	<meta name="author" content="Razend" />
	<?php get_template_part( '/resources/components/favicon');
	
	if($google_script == 'gtag_manager'){
		echo $google_tm_head;
	}
	wp_head();
	if($chatprovider == 'zendesk') {
		echo $chat_zendesk_snippet;
	} ?>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
</head>
<body class="<?php if (is_404()): ?> error_404 <?php endif;?> ">
	<?php if($google_script == 'gtag_manager'){
		echo $google_tm_body;
	}
	if (!is_404()): ?>
	
	<header>
		<section id="nav" class="no-padding">
			<div class="container">
				<nav class="navbar navbar-expand-lg align-items-lg-center" role="navigation">
					<?php the_custom_logo(); ?>
					<!-- Brand and toggle get grouped for better mobile display -->
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navBar" aria-bs-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
					<!-- <a class="navbar-brand" href="#">Navbar</a> -->
		
						<?php
						wp_nav_menu( array(
							'theme_location' 		=> 'primary',
							'container'				=> 'div',
							'container_class'		=> 'collapse navbar-collapse',
							'container_id'			=> 'navBar',
							'menu_class'			=> 'nav navbar-nav align-items-xl-center ',
							'fallback_cb'			=> '__return_false',
							'items_wrap'			=> '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
							'depth'					=> 2,
							'walker'				=> new bootstrap_5_wp_nav_menu_walker()
						) );
						?>
				</nav>
				
			</div>
		</section>
	</header>
	<?php endif; ?>