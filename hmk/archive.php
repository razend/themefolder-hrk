<?php
	get_header();

	$headerImage = get_field('ArchiveHeaderImage','option'); 
	$headerImageTitle = get_field('title_in_image_top','option');
	$customheaderimage = get_field('custom_archive_image_header','option');
	$show_colored_space = get_field('show_colored_space','option');
	$page_for_posts = get_option( 'page_for_posts' );
?>


<?php if(get_field('ArchiveHeaderImage','option') == 'custom_image'): ?>
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom less-height <?= $show_colored_space; ?>" style="background-image: url(<?= $customheaderimage; ?>);">
<?php else: ?>
<section id="fullwidth-image" class="<?= $headerImage; ?> less-height single d-flex justify-content-start align-items-center">
<?php endif; ?>
	<h1><?=	$headerImageTitle ?></h1>
	<div class="white-bar"></div>
</section>

<?php get_template_part('resources/components/breadcrumbs'); ?>

<section id="knowledgebase">
	<div class="container">
		<div class="row">
			<?php get_template_part('resources/components/category-selector');
			
			get_template_part('resources/components/knowledgebase'); ?>
						
		</div>
	</div>
</section>

<?php get_footer(); ?>