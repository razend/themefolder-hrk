<?php 
wp_reset_query();
$postid = get_the_ID();
$carousel_number = rand(0, 1000); ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php if(is_page_template( 'page-extra_landingspage.php' )) : echo "col-lg-8"; endif; ?> text-center">
				<div id="quotes_carousel_<?php echo $carousel_number; ?>" class="carousel slide" data-ride="carousel" data-interval="false">	
					<div class="carousel-inner">
						<?php 
							$repeaterCounter = 0;
							$slide_cols_setting = get_sub_field('number_of_cols_slide');
							$slide_cols = " ";
							$slide_divide = 1;
							if ($slide_cols_setting == "1"):
								$slide_cols = " ";
								$slide_divide = 1;
							elseif ($slide_cols_setting == "2"):
								$slide_cols = "col-md-6";
								$slide_divide = 2;
							elseif ($slide_cols_setting == "3"):
								$slide_cols = "col-md-4";
								$slide_divide = 3;
							elseif ($slide_cols_setting == "4"):
								$slide_cols = "col-md-3";
								$slide_divide = 4;
							endif;

						 ?>
						<div class="carousel-item <?php if($repeaterCounter == 0): ?> active <?php endif; ?>">
							<div class="row">
								<?php if( have_rows('quotes') ): while( have_rows('quotes') ) : the_row(); ?>
									<?php 
										$image =  get_sub_field('quote_image');
										$company = get_sub_field('quote_company');
										$name = get_sub_field('quote_name-function');
										$quote = get_sub_field('quote_quote');
									 ?>
									<div class="col-12 <?php echo $slide_cols; ?> quote">
										<div class="image">
											<img src="<?php echo $image; ?>" alt="">
										</div>
										<h3><?php echo $company; ?></h3>
										<strong><?php echo $name; ?>&nbsp;</strong>
										<p><?php echo $quote; ?></p>
									
									</div>
									<?php $repeaterCounter++; ?>
									<?php if($repeaterCounter == $slide_divide): ?>
										</div>
										</div>
										<?php $repeaterCounter = 0; ?>
										<div class="carousel-item">
										<div class="row">
									<?php endif; ?>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
					<ol class="carousel-indicators">
						<?php 
							$slideCounter = 0;
							$count = count(get_sub_field('quotes'));
							$numOfIndicators = (round($count / $slide_divide));
							if( have_rows('quotes') ): while( have_rows('quotes') ) : the_row(); ?>
							
							<li data-target="#quotes_carousel_<?php echo $carousel_number; ?>" data-slide-to="<?php echo $slideCounter; ?>" class="<?php if($slideCounter == 0): ?> active <?php endif; ?>"></li>
							
							<?php $slideCounter++;?>
							<?php if($slideCounter ==  $numOfIndicators){ break; }; ?>		
						<?php endwhile; endif; ?>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>