<?php 
$blog_title = get_bloginfo(); 
$blog_description = get_bloginfo('description');
$page_title = get_the_title();
$google_script = get_field('google_script','option');
$google_tagmanager_code_snippet_head = get_field('google_tagmanager_code_snippet_head','option');
$google_tagmanager_code_snippet_body = get_field('google_tagmanager_code_snippet_body','option');
$chatprovider = get_field('chatprovider','option');

?><!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="utf-8">
	<title><?php echo $page_title . " | " . $blog_title . " - " . $blog_description; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
	<meta name="author" content="Razend" />
	<?php get_template_part( '/resources/components/favicon');
	
	if($google_script === 'gtag_manager'){
		echo $google_tagmanager_code_snippet_head;
	} 
	wp_head();
	if($chatprovider === 'zendesk'){
		echo $chatprovider;
	}
	 ?>
	 <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
</head>
<body class="<?php if (is_404()) { echo "error_404"; } ?> ">
	<?php if($google_script === 'gtag_manager'){ 
		echo $google_tagmanager_code_snippet_body;
	};
	if (!is_404()): ?>
	
	<header>
		<section id="nav" class="no-padding">
			<div class="container">
				<nav class="navbar navbar-expand-lg align-items-lg-center" role="navigation">
					<?php the_custom_logo(); ?>
					<!-- Brand and toggle get grouped for better mobile display -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
					<!-- <a class="navbar-brand" href="#">Navbar</a> -->
		
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'primary',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'collapse navbar-collapse',
							'container_id'		=> 'navBar',
							'menu_class'		=> 'nav navbar-nav align-items-xl-center',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>

						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'socials',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'collapse navbar-collapse',
							'container_id'		=> 'navBar',
							'menu_class'		=> 'nav navbar-nav align-items-xl-center',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
				</nav>
				
			</div>
		</section>
	</header>
	<?php endif; ?>