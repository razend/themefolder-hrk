
<section id="content" class="no-padding">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="timeline">
					@acfrepeater('timeline',$annual_program_page)
					<li>
						<div class="list_type"></div>
						<div class="content">
							{{the_sub_field('timeline_rule')}}
						</div>
					</li>
					@acfend
				</ul>
			</div>
		</div>
	</div>
</section>
