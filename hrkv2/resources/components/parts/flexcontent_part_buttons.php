<?php 
// Nieuw button toevoegsysteem
$add_buttons = get_sub_field('group_add_buttons'); // knopjes group 
$cta_button_align = "text-" . $add_buttons['cta_button_alignment'];
$cta_button_style = $add_buttons['cta_style'];

 ?>
	<?php  if( have_rows('group_add_buttons') ) : while( have_rows('group_add_buttons')) : the_row(); ?>
		<?php if( have_rows('cta_adding') ) : ?>
			<div class="<?php echo $cta_button_align; ?> cta-buttons">
			 <?php while( have_rows('cta_adding') ) : the_row();
			 	$cta_button_text = get_sub_field('cta_button_text');
				$cta_button_link = get_sub_field('cta_button_link');
				$cta_button_target = get_sub_field('cta_button_target'); ?>
				<a class="btn <?php echo $cta_button_style; ?>" href="<?php echo $cta_button_link; ?>" target="<?php echo $cta_button_target; ?>"><?php echo $cta_button_text; ?></a>
			 <?php endwhile; ?>
			</div>
		<?php endif; ?>
	<?php endwhile; endif;?>		
