<?php
$section_id = get_sub_field('section_id');
$vacancie_block_settings = get_sub_field('vacancie_block_settings');
$number_of_vacancies = get_sub_field('number_of_vacancies');
$vacancie_selection = get_sub_field('select_vacancies');
$featured_vacancie = get_sub_field('featured_vacancie');

$vacancie_description = get_field('vacancie_description',$featured_vacancie);
$vacancie_state = get_field('vacancie_state',$featured_vacancie);
$vacancie_opertunities = get_field('vacancie_opertunities',$featured_vacancie);
$vacancie_job_type = get_field('vacancie_job_type',$featured_vacancie);
$vacancie_experience = get_field('vacancie_experience',$featured_vacancie);
$vacancie_hours = get_field('vacancie_hours',$featured_vacancie);
$vacancie_start = get_field('vacancie_start',$featured_vacancie);
$vacancie_wages = get_field('vacancie_wages',$featured_vacancie);
$taxonomy  = 'vacancie_categories';
$tax_terms = get_cat_name($featured_vacancie);

$vacancie_companyname = get_field('vacancie_companyname',$featured_vacancie);
$vacancie_contact_email = get_field('vacancie_contact_email',$featured_vacancie);
$vacancie_company_website = get_field('vacancie_company_website',$featured_vacancie);
$vacancie_recruiters = get_field('vacancie_recruiters',$featured_vacancie);
$form_id = get_field('form_id',$featured_vacancie);
?>

<?php if($section_id): ?>
<div id="<?php echo $section_id; ?>"></div>
<?php endif; ?>

<?php
if($vacancie_block_settings == 'featured_vacancie'):
	$term_list = wp_get_post_terms($featured_vacancie, 'vacancie_categories', array("fields" => "all")); ?>

<section id="vacancie">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 ">
					
					<div class="content">
						<?php echo $vacancie_description; ?>
						
					</div>
				</div>
				<div class="col-12 col-lg-4 sidebar">
					<div class="sticky-top">
						<a class="btn mt-5 mb-4" href="#form">Direct soliciteren</a>
						<div class="sidebar-box">
							<div class="box-title">Vacature details</div>
							<div class="box-content">
								<ul class="post-list">
									<?php if($vacancie_state): ?>
										<li><span><strong>Regio: </strong><?php echo implode( ', ', $vacancie_state ) ?></span></li>
									<?php endif; ?>
									<li>
										<span>
											<strong>Functiecategorie: </strong>
											<?php foreach($term_list as $term_single):
											    echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>';
											endforeach; ?>
										</span>
									</li>

									<?php if($vacancie_opertunities): ?>
										<li><span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span><br></li>
									<?php endif; ?>

									<?php if($vacancie_job_type): ?>
									<li><span><strong>Dienstverband: </strong><?php echo $vacancie_job_type; ?></span></li>
									<?php endif; ?>
									<?php if($vacancie_experience): ?>
									<li><span><strong>Ervaring: </strong>
										<?php foreach( $vacancie_experience as $vacancie_experience_single ):
									        echo $vacancie_experience_single .", "; 
									    endforeach; ?>
									</span></li>
									<?php endif; ?>
									<?php if($vacancie_hours): ?>
									<li><span><strong>Uren: </strong>
										<?php foreach( $vacancie_hours as $vacancie_hour ):
									        echo $vacancie_hour .", "; 
									    endforeach; ?>
									</span></li>
									<?php endif; ?>
									<?php if($vacancie_start): ?>
									<li><span><strong>Startdatum: </strong><?php echo $vacancie_start; ?></span></li>
									<?php endif; ?>
									<?php if($vacancie_wages): ?>
									<li><span><strong>Salaris / Tarief: </strong>€ <?php echo $vacancie_wages; ?></span></li>
									<?php endif; ?>
									<li><span><strong>Geplaatst op: </strong><?php echo get_the_date(); ?></span></li>
								</ul>
							</div>
						</div>
						<?php if($vacancie_companyname): ?>
						<div class="sidebar-box">
							<div class="box-title">Bedrijfsinformatie</div>
							<div class="box-content">
								<ul class="post-list">
									<?php if($vacancie_companyname): ?>
									<li><span><strong>Aangeboden door: </strong><?php echo $vacancie_companyname; ?> </span></li>
									<?php endif; ?>
									<?php if($vacancie_contact_email): ?>
									<li><span><strong>Contact: </strong><a href="mailto:<?php echo $vacancie_contact_email; ?> "><?php echo $vacancie_contact_email; ?> </a></span></li>
									<?php endif; ?>
									<?php if($vacancie_companyname): ?>
									<li><span><strong>Website: </strong><a href="<?php echo $vacancie_company_website; ?>"><?php echo $vacancie_companyname; ?></a> </span></li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
						<?php endif; ?>
						<div class="sidebar-box">
							<div class="box-title">Recruiter(s)</div>
							<div class="box-content">
								
								<?php foreach ($vacancie_recruiters as $post): ?>
									<div class="row teammember">
										<div class="col-4 col-lg-5 col-xl-4">
											<div class="thumbnail teamimage" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);">
											</div>
										</div>
										<div class="col-8  col-lg-7 col-xl-8 pl-0">
											<p class="team_name pb-0"><strong><?php echo the_title(); ?></strong><br> <?php echo the_field('team_function'); ?></p>
											<ul class="socialmedia icons-circle d-flex justify-content-start ">
												<?php if(get_field('team_phonenumber')): ?>
												<li><a href="tel:<?php echo the_field('team_phonenumber'); ?>" target="_blank"><i class="fas fa-phone"></i></i></a></li>
												<?php endif; ?>
												<?php if(get_field('team_emailaddress')): ?>
												<li><a href="mailto:<?php echo the_field('team_emailaddress'); ?>" target="_blank"><i class="far fa-envelope"></i></a></li>
												<?php endif; ?>
												<?php if(get_field('linkedin_profiel_url')): ?>
												<li><a href="<?php echo the_field('linkedin_profiel_url'); ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
												<?php endif; ?>
											</ul>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php wp_reset_query(); ?>
<?php elseif($vacancie_block_settings == 'selection'): ?>

<section id="vacancie">
		<div class="container">
			<div class="row">
			<?php foreach ($vacancie_selection as $post):
					$postID = get_the_ID(); 
					$vacancie_companyname = get_field('vacancie_companyname',$postID);
					$vacancie_opertunities = get_field('vacancie_opertunities',$postID);
					$vacancie_state = get_field('vacancie_state',$postID);
					$vacancie_hours = get_field('vacancie_hours',$postID);
					$vacancie_job_type = get_field('vacancie_job_type',$postID);
					$vacancie_wages = get_field('vacancie_wages',$postID);
					$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));
					 ?>
				<div class="col-12 col-md-4">
					<div class="post <?php echo $firstCategory; ?> ">
						<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
						<div class="post-body">
							<h5 class="post-title mb-3"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
							<p class="post-details compact pb-0">
								<?php if($vacancie_companyname): ?>
									<span><strong>Bij: </strong><?php echo $vacancie_companyname; ?> </span><br>
								<?php endif; ?> 

								<?php if($vacancie_opertunities): ?>
									<span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span><br>
								<?php endif; ?>

								<strong>Regio: </strong><?php echo implode( ', ', $vacancie_state ) ?><br>
								<strong>Functiecategorie: </strong>
								<?php foreach($term_list as $term_single): ?>
									    <?php echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>'; ?>
								<?php endforeach; ?>
								<br>
								<?php if($vacancie_hours): ?>
									<strong>Uren: </strong>
										<?php foreach( $vacancie_hours as $vacancie_hour ):
									        echo $vacancie_hour .", "; 
									    endforeach; ?>
									<br>
								<?php endif; ?>
								<?php if($vacancie_job_type): ?>
									<strong>Type contract: </strong><?php echo $vacancie_job_type; ?><br>
								<?php endif; ?>
								<?php if($vacancie_wages): ?>
									<strong>Salaris / Tarief: </strong>€ <?php echo $vacancie_wages; ?>
								<?php endif; ?>
							</p>
							
							<p class="post-text"><?php echo get_the_excerpt(); ?></p>
							<div class="d-flex justify-content-left	">
								<a href="<?php echo the_permalink(); ?>" class="btn small">Bekijk vacature</a>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			</div>
		</div>
	</section>
	<?php wp_reset_query(); ?> 
<?php elseif($vacancie_block_settings == 'automatic'): ?>

<section id="vacancie">
		<div class="container">
			<div class="row">
				<?php 
				$the_query = new WP_Query(array('post_type' => 'vacancies', 'posts_per_page' => $number_of_vacancies));
				if ( $the_query->have_posts() ) :
					while ( $the_query->have_posts() ) : $the_query->the_post();
					$postID = get_the_ID(); 
					$vacancie_companyname = get_field('vacancie_companyname',$postID);
					$vacancie_opertunities = get_field('vacancie_opertunities',$postID);
					$vacancie_state = get_field('vacancie_state',$postID);
					$vacancie_hours = get_field('vacancie_hours',$postID);
					$vacancie_job_type = get_field('vacancie_job_type',$postID);
					$vacancie_wages = get_field('vacancie_wages',$postID);
					$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));
				?>
				<div class="col-12 col-md-4">
					<div class="post <?php echo $firstCategory; ?> ">
						<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url('$postID'); ?>" alt=""></a> 
						<div class="post-body">
							<h5 class="post-title  mb-3"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
							<p class="post-details compact pb-0">
								<?php if($vacancie_companyname): ?>
									<span><strong>Bij: </strong><?php echo $vacancie_companyname; ?> </span><br>
								<?php endif; ?> 

								<?php if($vacancie_opertunities): ?>
									<span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span><br>
								<?php endif; ?>

								<strong>Regio: </strong><?php echo implode( ', ', $vacancie_state ) ?><br>
								<strong>Functiecategorie: </strong>
								<span class="functie_cats">
									<?php foreach($term_list as $term_single):
									    echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>';
									endforeach; ?>
								</span>
								<br>
								<?php if($vacancie_hours): ?>
									<strong>Uren: </strong>
										<?php foreach( $vacancie_hours as $vacancie_hour ):
									        echo $vacancie_hour .", ";
									   	endforeach; ?>
									<br>
								<?php endif; ?>
								<?php if($vacancie_job_type): ?>
									<strong>Type contract: </strong><?php echo $vacancie_job_type; ?><br>
								<?php endif; ?>
								<?php if($vacancie_wages): ?>
									<strong>Salaris / Tarief: </strong>€ <?php echo $vacancie_wages; ?>
								<?php endif; ?>
							</p>
							<div class="d-flex justify-content-left">
								<a href="<?php echo the_permalink(); ?>" class="btn small">Bekijk vacature</a>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
	<?php wp_reset_query(); ?>
<?php endif; ?>
