<?php 
	$headerimage = get_field('headerimage');
	$headerImageTitle = get_field('title_in_image_top');
	$headerImageSubTitle = get_field('subtitle_in_image_top');
	$customheaderimage = get_field('custom_image_header');
	$show_colored_space = get_field('show_colored_space');
	$form_id = get_field('form_id');
	$h3_header = get_field('h3_header');

	$header_less_height = get_field('header-image-less-height');

	$company_logos_title = get_field('company_logos_title');
?>
<?php if(get_field('headerimage') == 'custom_image'):?>

<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space; ?> <?php if($header_less_height == 'yes'): echo "less-height"; endif; ?>" style="background-image: url(<?php echo $customheaderimage; ?>);">
<?php else: ?>
<section id="fullwidth-image" class="<?php echo $headerimage; ?> d-flex justify-content-start align-items-center <?php if($header_less_height == 'yes'): echo "less-height"; endif; ?>">
<?php endif; ?>
		<span><?php echo $headerImageTitle; ?></span>
		<?php if(get_field('subtitle_in_image_top')):?>
		<span><?php echo $headerImageSubTitle; ?></span>
		<?php endif; ?>
	<div class="white-bar"></div>
</section>

<?php if( get_field('orangebar') == 'yes' ): ?>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
<?php endif; ?>

<section id="content">
	<div class="landingpage">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<p><?php the_content(); ?></p>
					<?php endwhile; else : ?>
						<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
					<?php if($h3_header):?>
					<h3><?php echo $h3_header; ?></h3>
					<?php endif; ?>
					<ul class="row usps">
						<?php if( have_rows('usps') ): while( have_rows('usps') ) : the_row(); ?>
						<li class="col-12 col-md-6">
							<?php if(get_sub_field('usp_title')):?>
							<b><?php echo the_sub_field('usp_title'); ?></b><br>
							<?php endif; ?>
							<p><?php the_sub_field('usp_content'); ?></p>
						</li>
						<?php endwhile; endif; ?>
					</ul>
				</div>
				<div class="col-12 col-lg-4">
					<div class="contact_block">
						<?php gravity_form( $form_id, false, false, false, '', false ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_template_part('resources/components/quote_slider');
if(get_field('company_logos')): ?>
<div class="container">
	<div class="company_logos">
		<div class="row">
			<div class="col-12">
				<h3><?php if($company_logos_title): echo $company_logos_title; else:  echo "deze bedrijven gingen je al voor"; endif; ?></h3>
			</div>
			<?php 
				$count = count(get_field('company_logos'));
				$logos = 0;
			 ?>
			<?php if( have_rows('company_logos') ): while( have_rows('company_logos') ) : the_row(); ?>
				<div class="col-6 col-md-3 mt-5">
					<img src="<?php echo the_sub_field('company_logo'); ?>" alt="" class="company_logo">
				</div>
				<?php $logos++; ?>
				<?php if($logos == 8): ?>
				<div class="collapse container" id="collapseLogos">
					<div class="row">
				<?php endif; ?>
			<?php endwhile; endif; ?>
			</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-12 d-flex justify-content-center">
				<a class="mt-5 mb-5" data-toggle="collapse" href="#collapseLogos" role="button" aria-expanded="false" aria-controls="collapseLogos"></a>
			</div>
		</div>
		
	</div>
	</div>
<?php endif; ?>

<?php if( get_field('orangebar_footer') == 'yes' ): ?>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
<?php endif; ?>