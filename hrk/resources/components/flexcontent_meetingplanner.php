<?php 
	$person1 = get_sub_field('person1');
	$person2 = get_sub_field('person2');
	$content = get_sub_field('content');
	$btn_text = get_sub_field('btn_text');
?>
<section id="blue" class="meetingbanner">
	<div class="container">
		<div class="row ">
			<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo get_the_post_thumbnail_url($person1); ?>" alt="">
				</div>
				<div class="teammember-name"><?php echo get_the_title($person1); ?></div>
			</div>
			<?php if($person2): ?>
			<div class="col-12 col-md-8 d-flex">
				<h1><?php echo $content; ?></h1>
			<?php else : ?>
			<div class="col-12 col-md-10 align-items-top ">
				<h1 style="text-align: left;"><?php echo $content ?></h1>
				<a class="btn mettingbanner-button" data-appointlet-organization="hetrecruitingkantoor"><?php echo $btn_text; ?><script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
			<?php endif; ?>

				
			</div>

			<?php if($person2): ?>
			<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo get_the_post_thumbnail_url($person2); ?>" alt="">
				</div>
				<div class="teammember-name"><?php echo get_the_title($person2); ?></div>
			</div>
			
			<div class="col-12 d-flex align-items-center justify-content-center">
				<a class="btn mettingbanner-button" data-appointlet-organization="hetrecruitingkantoor"><?php echo $btn_text; ?><script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
			</div>
			<?php endif; ?>

		</div>
	</div>
</section>