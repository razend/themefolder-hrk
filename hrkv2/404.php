<?php get_header(); ?>

<section id="error_404" class="no-padding">
	<div class="container-fluid">
		<div class="row">
			<?php the_custom_logo(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8">
				<h1 class="dark"></h1>
			</div>
		</div>
	</div> 
</section>
<?php get_footer(); ?>