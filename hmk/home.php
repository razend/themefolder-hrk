<?php
	get_header();

	$headerImage = get_field('ArchiveHeaderImage','option'); 
	$headerImageTitle = get_field('title_in_image_top','option');
	$customheaderimage = get_field('custom_archive_image_header','option');
	$show_colored_space = get_field('show_colored_space','option');
	$page_for_posts = get_option( 'page_for_posts' );

	$knowledgebase_intro = get_field('knowledgebase_intro','option');

if(get_field('ArchiveHeaderImage','option') == 'custom_image'): ?>
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom less-height <?php echo $show_colored_space; ?>" style="background-image: url(<?php echo $customheaderimage; ?>);">
<?php else: ?>
<section id="fullwidth-image" class="<?php echo $headerImage; ?> less-height single d-flex justify-content-start align-items-center">
<?php endif; ?>
	<h1><?php echo $headerImageTitle; ?></h1>
	<div class="white-bar"></div>
</section>

<?php get_template_part('resources/components/breadcrumbs'); ?>

<section id="knowledgebase" class="pt-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php echo $knowledgebase_intro; ?>
			</div>

			<?php 
				get_template_part('resources/components/category-selector');
				get_template_part('resources/components/knowledgebase');
				get_template_part('resources/components/pagination');
			 ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>