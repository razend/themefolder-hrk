<div class="container">
	<div class="row justify-content-around">
		<?php 
			if( have_rows('usps') ): while( have_rows('usps') ): the_row();
				$usp_title = get_sub_field('title');
				$usp_subtitle = get_sub_field('subtitle');
				$usp_image = get_sub_field('icon');?>
				<div class="col-12 col-sm-6 col-lg-3 usp d-flex flex-column align-items-center">
					<img src="<?php echo $usp_image; ?>" class="icon" alt="">
					<p><span class="title"><?php echo $usp_title; ?></span>
					<?php if ($usp_subtitle): ?>
						<br><?php echo $usp_subtitle; ?>
					<?php endif; ?>
					</p>
				</div>						
			<?php endwhile; endif; ?>
		
	</div>
</div>
<?php wp_reset_query(); ?>