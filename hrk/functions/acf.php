<?php 
if( function_exists('acf_add_options_page') ) {
 
	$page = acf_add_options_page(array(
		'page_title' 	=> 'Theme opties',
		'menu_title' 	=> 'Theme opties',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'manage_options',
		'redirect' 	=> false
	)); 


	if( function_exists('acf_add_options_sub_page') ) {
		// acf_add_options_sub_page(array(
		// 	'title' => 'Homepage',
		// 	'parent' => 'theme-general-settings',
		// 	'capability' => 'manage_options',
		// ));
		acf_add_options_sub_page(array(
			'title' => 'Bedrijfsgegevens',
			'parent' => 'theme-general-settings',
			'capability' => 'manage_options'
		));
	}
}



?>