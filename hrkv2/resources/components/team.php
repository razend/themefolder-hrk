<?php $teamOrder = get_field('teamOrder','option'); ?>
<section id="team">
	<div class="container">
		<div class="row">
			<?php foreach ($teamOrder as $post):
				$team_function = get_field('team_function');
				$team_quality = get_field('team_quality');
				$team_phonenumber = get_field('team_phonenumber');
				$team_emailaddress = get_field('team_emailaddress');
				$linkedin_profiel_url = get_field('linkedin_profiel_url');
			 ?>
			<div class="col-6 col-md-4 col-lg-3 teammember ">
				<div class="thumbnail teamimage" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);">

				</div>
				<p class="team_name pb-0"><?php echo the_title(); ?></p>
				<p class="team_function pb-0"><?php echo $team_function; ?></p>
				<p class="team_quality tone"><?php echo $team_quality; ?></p>
				<ul class="socialmedia ml-auto icons-circle d-flex justify-content-end">
					<?php if($team_phonenumber){ ?>
					<li><a href="tel:<?php echo $team_phonenumber; ?>" target="_blank"><i class="fas fa-phone"></i></a></li>
					<?php }
					if($team_emailaddress){ ?>
					<li><a href="mailto:<?php echo $team_emailaddress; ?>" target="_blank"><i class="far fa-envelope"></i></a></li>
					<?php }
					if($linkedin_profiel_url){ ?>
					<li><a href="<?php echo $linkedin_profiel_url; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
					<?php } ?>
				</ul>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>