<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_634406f3d78ea',
	'title' => 'Tekstveld met media',
	'fields' => array(
		array(
			'key' => 'field_634407c40ca3c',
			'label' => '<span class="dashicons dashicons-layout"></span> Layout',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_634407f80ca41',
			'label' => 'Layout',
			'name' => 'layout_selection',
			'aria-label' => '',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => 'layout-selector',
				'id' => '',
			),
			'choices' => array(
				'layout2' => '<img src="/wp-content/themes/hrkv2/images/pagebuilder/text-media_2.webp" />',
				'layout3' => '<img src="/wp-content/themes/hrkv2/images/pagebuilder/text-media_3.webp" />',
				'layout4' => '<img src="/wp-content/themes/hrkv2/images/pagebuilder/text-media_4.webp" />',
				'layout5' => '<img src="/wp-content/themes/hrkv2/images/pagebuilder/text-media_5.webp" />',
			),
			'default_value' => 'layout5',
			'return_format' => 'value',
			'allow_null' => 0,
			'other_choice' => 0,
			'layout' => 'horizontal',
			'acfe_field_group_condition' => 0,
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_634407c20ca3b',
			'label' => '<span class="dashicons dashicons-text"></span> Inhoud',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63454d92bdd2b',
			'label' => 'Titel',
			'name' => 'title',
			'aria-label' => '',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout1',
					),
				),
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout4',
					),
				),
			),
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'acfe_textarea_code' => 0,
			'maxlength' => '',
			'rows' => 2,
			'placeholder' => '',
			'new_lines' => 'br',
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63454a31cefda',
			'label' => 'Content',
			'name' => 'content',
			'aria-label' => '',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'acfe_wysiwyg_min_height' => 100,
			'acfe_wysiwyg_max_height' => '',
			'acfe_wysiwyg_valid_elements' => '',
			'acfe_wysiwyg_custom_style' => '',
			'acfe_wysiwyg_disable_wp_style' => 0,
			'acfe_wysiwyg_autoresize' => 1,
			'acfe_wysiwyg_disable_resize' => 0,
			'acfe_wysiwyg_remove_path' => 0,
			'acfe_wysiwyg_menubar' => 0,
			'acfe_wysiwyg_transparent' => 0,
			'acfe_wysiwyg_merge_toolbar' => 1,
			'acfe_wysiwyg_custom_toolbar' => 0,
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 1,
			'acfe_wysiwyg_auto_init' => 1,
			'acfe_field_group_condition' => 0,
			'acfe_wysiwyg_height' => 300,
			'acfe_wysiwyg_toolbar_buttons' => array(
			),
		),
		array(
			'key' => 'field_634407ed0ca40',
			'label' => 'Buttons',
			'name' => 'buttons',
			'aria-label' => '',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_6329b05352abd',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
			'acfe_field_group_condition' => 0,
			'acfe_seamless_style' => 0,
			'acfe_clone_modal' => 0,
			'acfe_clone_modal_close' => 0,
			'acfe_clone_modal_button' => '',
			'acfe_clone_modal_size' => 'large',
		),
		array(
			'key' => 'field_63454a25cefd9',
			'label' => '<span class="dashicons dashicons-admin-media"></span> Media',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63299f670313e',
			'label' => 'Afbeelding',
			'name' => 'image',
			'aria-label' => '',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '!=',
						'value' => 'layout2',
					),
				),
			),
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'uploader' => '',
			'acfe_thumbnail' => 0,
			'return_format' => 'url',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => 'jpg, jpeg, png',
			'preview_size' => 'thumbnail',
			'acfe_field_group_condition' => 0,
			'library' => 'all',
		),
		array(
			'key' => 'field_6376018a0ad3a',
			'label' => 'Uitleg',
			'name' => 'dm_text_media_image',
			'aria-label' => '',
			'type' => 'acfe_dynamic_render',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '!=',
						'value' => 'layout2',
					),
				),
			),
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'acfe_field_group_condition' => 0,
			'render' => '',
		),
		array(
			'key' => 'field_63bd554cadba2',
			'label' => 'Plaatsing',
			'name' => 'image_placement',
			'aria-label' => '',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout5',
					),
				),
			),
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'left' => 'Links',
				'right' => 'Rechts',
			),
			'default_value' => 'links',
			'return_format' => 'value',
			'allow_null' => 0,
			'layout' => 'horizontal',
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_6375fe35235d5',
			'label' => '(Column 6/12)',
			'name' => '',
			'aria-label' => '',
			'type' => 'acfe_column',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout2',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'columns' => '6/12',
			'endpoint' => 0,
			'border' => array(
				0 => 'fields',
			),
			'acfe_field_group_condition' => 0,
			'border_endpoint' => array(
				0 => 'endpoint',
			),
		),
		array(
			'key' => 'field_6375fdd03ebc3',
			'label' => 'Video bron',
			'name' => 'video_source',
			'aria-label' => '',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout2',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'youtube' => 'YouTube',
				'vimeo' => 'Vimeo',
				'getcontrast' => 'GetContrast',
			),
			'default_value' => '',
			'return_format' => 'value',
			'allow_null' => 0,
			'layout' => 'horizontal',
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63299f6703142',
			'label' => 'Video',
			'name' => 'video',
			'aria-label' => '',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout2',
					),
					array(
						'field' => 'field_6375fdd03ebc3',
						'operator' => '!=',
						'value' => 'getcontrast',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_65bb68b4e1bdf',
			'label' => 'Video embedcode',
			'name' => 'video_script',
			'aria-label' => '',
			'type' => 'acfe_code_editor',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout2',
					),
					array(
						'field' => 'field_6375fdd03ebc3',
						'operator' => '==',
						'value' => 'getcontrast',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'mode' => 'text/html',
			'lines' => 1,
			'indent_unit' => 4,
			'maxlength' => '',
			'rows' => 10,
			'max_rows' => '',
			'return_entities' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_6375fe62235d7',
			'label' => '(Column 6/12)',
			'name' => '',
			'aria-label' => '',
			'type' => 'acfe_column',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout2',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'columns' => '6/12',
			'endpoint' => 0,
			'border' => '',
			'acfe_field_group_condition' => 0,
			'border_endpoint' => array(
				0 => 'endpoint',
			),
		),
		array(
			'key' => 'field_63454597ffe01',
			'label' => 'Uitleg',
			'name' => 'dm_text_media_video',
			'aria-label' => '',
			'type' => 'acfe_dynamic_render',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout2',
					),
				),
			),
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'acfe_field_group_condition' => 0,
			'render' => '',
		),
		array(
			'key' => 'field_6375fe4b235d6',
			'label' => '(Column Endpoint)',
			'name' => '',
			'aria-label' => '',
			'type' => 'acfe_column',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_634407f80ca41',
						'operator' => '==',
						'value' => 'layout2',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'endpoint' => 1,
			'border_endpoint' => array(
				0 => 'endpoint',
			),
			'acfe_field_group_condition' => 0,
			'columns' => '6/12',
			'border' => '',
		),
		array(
			'key' => 'field_634407b70ca39',
			'label' => '<span class="dashicons dashicons-admin-settings"></span>Instellingen',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_634407d50ca3e',
			'label' => 'Instellingen',
			'name' => 'section_settings',
			'aria-label' => '',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_632c245445a1e',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
			'acfe_field_group_condition' => 0,
			'acfe_seamless_style' => 0,
			'acfe_clone_modal' => 0,
			'acfe_clone_modal_close' => 0,
			'acfe_clone_modal_button' => '',
			'acfe_clone_modal_size' => 'large',
		),
		array(
			'key' => 'field_634407ba0ca3a',
			'label' => '<span class="dashicons dashicons-cover-image"></span> Achtergrond',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_634546132c6a5',
			'label' => 'Sectie achtergrond',
			'name' => 'section_bg',
			'aria-label' => '',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_632ad2d5cf523',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
			'acfe_field_group_condition' => 0,
			'acfe_seamless_style' => 0,
			'acfe_clone_modal' => 0,
			'acfe_clone_modal_close' => 0,
			'acfe_clone_modal_button' => '',
			'acfe_clone_modal_size' => 'large',
		),
		array(
			'key' => 'field_634407b10ca38',
			'label' => '<span class="dashicons dashicons-editor-code"></span> Ontwikkelaar',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_634407cb0ca3d',
			'label' => 'Ontwikkelaar',
			'name' => 'ontwikkelaar',
			'aria-label' => '',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_6329a16266701',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
			'acfe_field_group_condition' => 0,
			'acfe_seamless_style' => 0,
			'acfe_clone_modal' => 0,
			'acfe_clone_modal_close' => 0,
			'acfe_clone_modal_button' => '',
			'acfe_clone_modal_size' => 'large',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => false,
	'description' => '',
	'show_in_rest' => 0,
	'acfe_autosync' => array(
		0 => 'php',
	),
	'acfe_form' => 0,
	'acfe_display_title' => '',
	'acfe_meta' => '',
	'acfe_note' => '',
	'acfe_categories' => array(
		'pagebuilder' => 'Pagebuilder',
	),
	'modified' => 1706781535,
));

endif;