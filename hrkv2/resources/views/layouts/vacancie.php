<?php

// Wordt gebruikt voor single weergave vacature

// Tab: Vacature informatie
$vacancie_description = get_field('vacancie_description_v2');
$vacancie_description_clean = get_field('vacancie_description_clean');
$vacancie_opertunities = get_field('vacancie_opertunities');
$vacancie_state = get_field('vacancie_state');
$vacancie_job_type = get_field('vacancie_job_type');
$vacancie_experience = get_field('vacancie_experience');
$vacancie_hours = get_field('vacancie_hours');
$vacancie_wages = get_field('vacancie_wages');
$vacancie_start = get_field('vacancie_start');
$taxonomy  = 'vacancie_categories';
$tax_terms = get_terms($taxonomy, array('hide_empty' => false));

// V2
$vacancies_your_work = get_field('vacancies_your_work');
$vacancies_rewards = get_field('vacancies_rewards');
$vacancie_description = get_field('vacancie_description_v2');
$vacancie_description_v2_left = get_field('vacancie_description_v2_left');
$vacancie_description_v2_right = get_field('vacancie_description_v2_right');

$vacancie_date_start = get_field('vacancie_start_exact_date');

// Tab: Bedrijfs informatie
$vacancie_companyname = get_field('vacancie_companyname');
$vacancie_contact_email = get_field('vacancie_contact_email');
$vacancie_company_website = get_field('vacancie_company_website');

// Tab: Locatie
$vacancie_postalcode = get_field('vacancie_postalcode');
$vacancie_state = get_field('vacancie_state');
$vacancie_country = get_field('vacancie_country');

// Recruiters
$vacancie_recruiters = get_field('vacancie_recruiters');
$recruiter = get_field('vacancie_recruiters');
$recruiter_function = get_field('team_function', $recruiter->ID);
$featured_image = get_the_post_thumbnail_url( $recruiter->ID, "thumbnail");

// Vacature instellingen
$vacancie_validThrough = get_field('vacancie_validThrough');
$vacancie_logo = get_the_post_thumbnail_url();

// Algemene vacature instellingen
$vacancie_footer_hero_title = get_field('vacancie_footer_hero_title','option');

?>


<script type="application/ld+json"> 
    {
        "@context" : "http://schema.org/",
        "@type" : "JobPosting",
        "title" : "<?php echo get_the_title(); ?>",
        "description" : "<?php echo $vacancie_description_clean; ?>",
        "identifier": {
            "@type": "PropertyValue",
            "name": "<?php echo $vacancie_companyname; ?>",
            "value": "<?php echo get_the_ID(); ?>"
        },	
        "datePosted" : "<?php echo get_the_date('Y-m-d'); ?>",
        "validThrough" : "<?php echo $vacancie_validThrough; ?>",
        "employmentType" : "<?php echo $vacancie_job_type; ?>",
        "hiringOrganization" : {
            "@type" : "Organization",
            "name" : "<?php echo get_bloginfo( 'name' ); ?>",
            "sameAs" : "<?php echo site_url(); ?>",
            "logo" : "<?php echo $vacancie_logo; ?>"
        },
        "jobLocation" : {
            "@type" : "Place",
            "address" : {
                "@type" : "PostalAddress",
                "streetAddress" : "<?php ?>",
                "addressLocality" : "<?php ?>",
                "addressRegion" : "<?php echo implode( ', ', $vacancie_state ); ?>",
                "postalCode" : "<?php echo $vacancie_postalcode; ?>",
                "addressCountry": "NL"
            }
        },
        "baseSalary": {
            "@type": "MonetaryAmount",
            "currency": "<?php echo "EUR"; ?>",
            "value": {
                "@type": "QuantitativeValue",
                "value": "<?php echo $vacancie_wages; ?>",
                "unitText": "MONTH"
            }
        }
    }
</script>

<?php
// Enkele pagina
if(is_singular('vacancies')):
	$postID = get_the_ID();  
	$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all")); 
    $vacancie_state_tag = get_field('vacancie_state_tag',$postID);
    $vacancie_hours_tag = get_field('vacancie_hours_tag',$postID);
    $vacancie_wages_tag = get_field('vacancie_wages_tag',$postID);
?>
	<section id="vacancie_title" class="orange small_padding no-image d-flex justify-content-start align-items-center">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1>vacature <?php echo the_title(); ?> bij <?php echo $vacancie_companyname; ?></h1>
					<div class="details">
						<!-- <span><i class="fa-solid fa-location-dot"></i> <?php echo implode( ', ', $vacancie_state ); ?></span>
						<span><i class="fa-solid fa-clock"></i> <?php echo $vacancie_job_type; ?></span> -->

 							<?php if ($vacancie_state_tag): ?>
 								<span title="Locatie / regio"><i class="fa-solid fa-location-dot"></i> <?php echo $vacancie_state_tag; ?></span>
                            <?php endif; ?>
                            <?php if ($vacancie_wages_tag): ?>
                                <span title="Salaris"><i class="fa-solid fa-euro-sign"></i><?php echo $vacancie_wages_tag; ?></span>
                            <?php endif; ?>
                            <?php if ($vacancie_hours_tag): ?>
                                <span title="Uren"><i class="fa-solid fa-clock"></i><?php echo $vacancie_hours_tag; ?></span>
                            <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="vacancie">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 pe-lg-5">
					<?php echo $vacancies_your_work; ?>
				</div>
				<div class="col-12 col-lg-6 ps-lg-5">
					<?php echo $vacancies_rewards; ?>
				</div>
				<div class="col-12">
					<a class="btn style_filled_orange size_big" href="#form_section">solliciteer direct</a>
				</div>
				<div class="col-12">
					<div class="question-box d-lg-flex align-items-center">
						<span class="title flex-grow-1">heb je een vraag over deze vacature?</span>
						<div class="d-flex flex-column flex-lg-row">
							<div class="contact-box d-flex me-3">
								<div class="contact-items d-flex">
								<?php get_template_part('resources/components/vacancies/recruiter_contact_options'); ?>
								</div>
							</div>
							<div class="recruiter">
								<div class="d-flex align-items-center">
									<i class="fa-solid fa-user"></i>
									<p class="team_name pb-0"><strong><?php echo ( $recruiter->post_title ); ?></strong><br><?php echo $recruiter_function; ?></p>
									<div class="recruiter-photo">
										<img src="<?php echo $featured_image; ?>" alt="" class="thumbnail">
									</div>
									<?php // wp_reset_query(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 ">
					<div class="title pt-5 pb-5"><h3>omschrijving vacature <?php the_title(); ?> </h3></div>
					<div class="content">
						<?php echo $vacancie_description; ?>
					</div>					
				</div>
				<div class="col-12 col-lg-6 pe-lg-5">
					<?php echo $vacancie_description_v2_left; ?>
				</div>
				<div class="col-12 col-lg-6 ps-lg-5">
					<?php echo $vacancie_description_v2_right; ?>
				</div>
			</div>
		</div>
	</section>
	
<?php get_template_part('resources/components/vacancies/about_company'); ?>

<?php get_template_part('resources/components/vacancies/form_section'); ?>

<section class="hero_block d-flex align-items-center" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/hero_vacancies.jpg'); background-size: cover; background-position: top center; height: 450px;">
	<div class="container-fluid ps-lg-5 pe-lg-5">
		<div class="row justify-content-<?php echo $align_x; ?>">
			<div class="col-12 col-xl-10 p-5 d-flex justify-content-<?php echo $align_x; ?>">

				<div class="title">
					<?php echo $vacancie_footer_hero_title; ?>
				</div>

			</div>
		</div>
	</div>
</section>


<?php else :
// Overzicht: Wordt gebruikt bij oa. alle vacatures van een bep. categorie --> 
	if (have_posts()) : while ( have_posts() ) : the_post();
	$postID = get_the_ID();  
		$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));  
		$vacancie_companyname = get_field('vacancie_companyname',$postID);
		$vacancie_state = get_field('vacancie_state',$postID);
		$vacancie_opertunities = get_field('vacancie_opertunities',$postID);
		$vacancie_hours = get_field('vacancie_hours',$postID);
		$vacancie_job_type = get_field('vacancie_job_type',$postID);
		$vacancie_wages = get_field('vacancie_wages',$postID);
		$term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));
	?>
	<div class="col-12 col-md-4">
			<div class="card">
				<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
				<div class="card-body">
					<h5 class="card-title mb-3"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
						<p class="card-details compact pb-0">
							<?php if($vacancie_companyname): ?>
							<span><strong>Bij: </strong><?php echo $vacancie_companyname; ?> </span><br>
							<?php endif; ?>

							<?php if($vacancie_opertunities): ?>
								<span><strong>Mogelijkheden: </strong><?php echo implode( ', ', $vacancie_opertunities ) ?></span><br>
							<?php endif; ?>
							
							<strong>Regio:</strong> <?php echo implode( ', ', $vacancie_state ) ?><br>

							<strong>Functiecategorie:</strong>
							<span class="functie_cats">
								<?php foreach($term_list as $term_single):
								    echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>';
								endforeach; ?>
							</span>
							<br>
							<strong>Uren:</strong> 
							<?php foreach( $vacancie_hours as $vacancie_hour ):
						        echo $vacancie_hour .", "; 
						    endforeach?><br>
							<strong>Type contract:</strong> <?php echo $vacancie_job_type; ?><br>
							<?php if($vacancie_wages): ?>
								<strong>Salaris:</strong> € <?php echo $vacancie_wages; ?>
							<?php endif; ?>
						</p>
						<div class="d-flex justify-content-left">
							<a href="<?php echo the_permalink(); ?>" class="btn small">Bekijk vacature</a>
						</div>
				</div>
			</div>
		</div>
	<?php endwhile; endif; ?>
<?php endif; ?>