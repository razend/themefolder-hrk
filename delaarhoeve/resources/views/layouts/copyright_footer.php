<section id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-12">
				© copyright <?php echo date('Y') . " " . the_field('company_name','option');?> <span class="spacer"> | <a href="https://razend.net">ontwikkeling: razend.net</a>
			</div>
		</div>
	</div>
</section>