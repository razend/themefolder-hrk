<?php 
$section_id = get_sub_field('section_id');
$bg_color = get_sub_field('testimonial_bg_color');
$text_color = get_sub_field('text_color');
$testimonial_id = get_sub_field('testimonial_id');
$testimonal_quote = get_field('quote',$testimonial_id);
$testimonal_name = get_field('name',$testimonial_id);
$testimonal_function = get_field('function',$testimonial_id);
$testimonal_image = get_field('image',$testimonial_id);
?>
<?php if ($section_id): ?>
	<div id="<?php echo $section_id; ?>"></div>
<?php endif ?>

<section id="content" class="testimonial" style="background-color: <?php echo $bg_color; ?>; color: <?php echo $text_color; ?>;">
	<div class="container">
		<div class="row">
			<div class="col-9">
				<h2 style="color: <?php echo $text_color; ?>;"><em><?php echo $testimonal_quote; ?> </em></h2>
				<p><span class="case_title"><?php echo $testimonal_name; ?> - <?php echo $testimonal_function; ?></span></p>
			</div>
			<div class="col-3 image">
				<img src="<?php echo $testimonal_image; ?>" alt="">
			</div>
		</div>
	</div>
</section>