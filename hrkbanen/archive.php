@include('header')

<?php
$headerImage = get_field('ArchiveHeaderImage','option'); 
$headerImageTitle = get_field('title_in_image_top','option');
?>

<section id="fullwidth-image" class="{{$headerImage['value']}} less-height d-flex justify-content-start align-items-center">
	<h1>{{$headerImageTitle}}</h1>
	<div class="white-bar"></div>
</section>

@include('resources/components/breadcrumbs')

<section id="knowledgebase">
	<div class="container">
		<div class="row">

			
			@include('resources/components/vacancies')
			

			
		</div>
	</div>
</section>

@include('footer')