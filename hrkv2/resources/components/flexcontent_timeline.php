<?php 
$padding_style = get_sub_field('section_padding');
?>

<section id="content" class="<?php echo $padding_style; ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="timeline">
					<?php if( have_rows('timeline') ): while( have_rows('timeline') ): the_row(); 
							$timeline_content = get_sub_field('timeline_rule'); ?>
					<li>
						<div class="list_type"></div>
						<div class="content">
							<?php echo $timeline_content; ?>
						</div>
					</li>
					<?php endwhile; endif; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
