<?php 
	$text = get_sub_field('content');
 ?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<?php 

			echo $text;
			
			get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); 
			
			?>
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>