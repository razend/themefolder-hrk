<?php
	// 30-8-21: blade engine opmaak verwijderd.
	$section_id = get_sub_field('section_id');
	$layout = get_sub_field('services_layout');
	$text = get_sub_field('content_services');
	$bg_color = get_sub_field('bg_color');
	$servicesOrder = get_sub_field('services_selection');
	$button_text = get_sub_field('button_text');
	$button_link = get_sub_field('button_link');
	$cta_button_align = get_sub_field('cta_button_alignment');
	$button_needed = get_sub_field('button_needed');
	$total = count($servicesOrder);
	$section_less_margin = get_sub_field('section_less_margin');
?>

<?php if ($section_id){ ?>
	<div id="<?php echo $section_id; ?>"></div>

<?php } 

if($section_less_margin && in_array('less_margin', $section_less_margin)): ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>; padding: 25px 0;">
<?php else : ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>;">
<?php endif; ?>
	<div class="container">
		<?php if($text){ ?>
			<div class="row">
				<div class="col-12 mb-5">
					<?php echo $text; ?>
				</div>
			</div>
		<?php } ?>
		<div class="row row-cols-1 row-cols-xl-2">
			<?php foreach ($servicesOrder as $post):
					if($total == 1): 
						$service_photo_text = get_field('service_photo_text');
						$service_content_excerpt = get_field('service_content_excerpt');
						$service_photo_text = get_field('service_photo_text');
						$service_content_excerpt = getfield('service_content_excerpt');
						?>
					<div class="col-md-6 col-lg-4">
						<div class="thumbnail d-flex align-items-end"style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);" onclick="window.location.href='<?php echo the_permalink(); ?>'">
							<a class="" href="<?php echo the_permalink(); ?>" >
								<?php echo $service_photo_text; ?>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-">
						<a class="title" href="<?php echo the_permalink(); ?>"><strong><?php echo the_title(); ?></strong></a>
						<?php echo $service_content_excerpt; ?>
						<a class="readmore" href="<?php echo the_permalink(); ?>">lees meer</a>
					</div>
					<?php elseif($total == 2): ?>
					<div class="col-12 col-md-6 service">
						<div class="row row-cols-xl-2">
							<div class="col-xl-5">
						<div class="thumbnail d-flex align-items-end"style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);" onclick="window.location.href='<?php echo the_permalink(); ?>'">
							<a class="" href="<?php echo the_permalink(); ?>" >
								<?php echo $service_photo_text; ?>
							</a>
						</div>
						</div>
						<div class="col-xl-7">
						<a class="title" href="<?php echo the_permalink(); ?>"><strong><?php echo the_title(); ?></strong></a>
							<?php echo $service_content_excerpt; ?>
						<!-- <a class="readmore" href="<?php echo the_permalink(); ?>">lees meer</a> -->
						</div>
						</div>
					</div>
					<?php elseif($total == 3): ?>
					<div class="col-12 col-md service">
						<div class="thumbnail d-flex align-items-end"style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);" onclick="window.location.href='<?php echo the_permalink(); ?>'">
							<a class="" href="<?php echo the_permalink(); ?>" >
								<?php echo $service_photo_text; ?>
							</a>
						</div>
						<a class="title" href="<?php echo the_permalink(); ?>"><strong><?php echo the_title(); ?></strong></a>
							<?php echo $service_content_excerpt; ?>
						<a class="readmore" href="<?php echo the_permalink(); ?>">lees meer</a>
					</div>
					<?php elseif($total == 4): ?>
					<div class="col-12 col-md-6 col-lg-3 service">
						<div class="thumbnail d-flex align-items-end"style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);" onclick="window.location.href='<?php echo the_permalink(); ?>'">
							<a class="" href="<?php echo the_permalink(); ?>" >
								<?php echo $service_photo_text; ?>
							</a>
						</div>
						<a class="title" href="<?php echo the_permalink(); ?>"><strong><?php echo the_title(); ?></strong></a>
							<?php echo $service_content_excerpt; ?>
						<a class="readmore" href="<?php echo the_permalink(); ?>">lees meer </a>
					</div>
					<?php endif; ?>
				<?php endforeach; ?>
				
			</div>
		</div>
		<?php if($button_needed == 'yes'): ?>
		<div class="row">
			<div class="col-12 mt-5 text-<?php echo $cta_button_align; ?>">
				<a class="btn" href="<?php echo $button_link; ?>"><?php echo $button_text; ?></a>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section> 
<?php wp_reset_query(); ?>