<?php 
$title = get_sub_field('title');
$text = get_sub_field('content');

$img = get_sub_field('image');

?>

<div class="container">
	<div class="row">
		<div class="col-12 col-lg-6 image pe-5">
			<img src="<?php echo $img; ?>" alt="" style="width: 100%;">
		</div>
		<div class="col-12 content col-lg-6 d-flex align-items-start flex-column">
			<div class="text">
				<?php echo $text;
				get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
			</div>			
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>