<?php 
	$headerimage = get_field('headerimage');
	$customheaderimage = get_field('custom_image_header');
	$show_colored_space = get_field('show_colored_space');
	$headerImageTitle = get_field('title_in_image_top');
	$midimage = get_field('midimage');
	$footerimage = get_field('footerimage');
	$footerImageTitle = get_field('title_in_image_bottom');
	$customfooterimage = get_field('custom_image_footer');
	$show_colored_space_footer = get_field('show_colored_space_footer');

if(get_field('headerimage') == 'custom_image'): ?>
	<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space; ?>" style="background-image: url(<?php echo $customheaderimage; ?>);">
<?php else: ?>
	<section id="fullwidth-image" class="<?php echo $headerimage; ?> d-flex justify-content-start align-items-center">
<?php endif; ?>
	<h1><?php echo $headerImageTitle; ?></h1>
	<div class="white-bar"></div>
</section>	
<?php if( get_field('orangebar') == 'yes' ): ?>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
<?php endif; ?>

<section id="content">
	<div class="container">
		<div class="row justify-content-center">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="col-12 col-lg-10 col-xl-8">
				<?php echo the_content(); ?>
			</div>
			<?php endwhile; endif; ?> 
		</div>
	</div> 
</section>

<?php get_template_part('resources/components/services'); ?>


<?php if(get_field('footerimage') == 'custom_image'): ?>
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space_footer; ?>" style="background-image: url(<?php echo $customfooterimage; ?>);">
<?php else : ?>
<section id="fullwidth-image" class="<?php echo $footerimage; ?> d-flex justify-content-start align-items-center">
<?php endif; ?>

	<h1><?php echo $footerImageTitle; ?></h1>
	<div class="white-bar"></div>
</section>	
<?php if( get_field('orangebar_footer') == 'yes' ): ?>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
<?php endif; ?>