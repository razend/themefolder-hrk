<?php 
// 30-8-21: blade engine opmaak verwijderd.

	// Layout settings
	$layout = get_sub_field('layout_selection');

	get_template_part( '/resources/components/parts/general/section_id');
	
	get_template_part('resources/components/parts/general/section_opening'); 

	if($layout == 'layout1'):

		get_template_part('resources/components/parts/paragraph/layout1');	

	elseif($layout == 'layout2'):

		get_template_part('resources/components/parts/paragraph/layout2');	

	endif;

get_template_part('resources/components/parts/general/section_close'); 

wp_reset_query(); ?>