<?php 
	$headerimage = get_field('headerimage');
	$headerImageTitle = get_field('title_in_image_top');
	$customheaderimage = get_field('custom_image_header');
	$show_colored_space = get_field('show_colored_space');
	$mailingFormTitle = get_field('mailing_form_title','option');
	$whitepaperFormTitle = get_field('whitepaper_form_title','option');
	$relatedPostTitle = get_field('related_post_title','option');
	$numberOfRecentPosts = get_field('number_of_recent_posts','option');
	$numberOfPopulairPosts = get_field('number_of_populair_posts','option');
	$knowledgebaseThankyouPage = get_field('knowledgebase_thankyou_page','option');
	$whitepaperThankyouPage = get_field('whitepaper_thankyou_page','option');
	$whitepaperFormID = get_field('whitepapier_from_id');
	$relatedPosts = get_field('related_blogs');
?>


<?php if(get_field('headerimage') == 'custom_image'): ?>
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom less-height <?= $show_colored_space; ?>" style="background-image: url(<?= $customheaderimage; ?>);">
<?php else: ?>
<section id="fullwidth-image" class="<?= $headerimage; ?> less-height single d-flex justify-content-start align-items-center">
<?php endif; ?>
	<h1><?= $headerImageTitle; ?></h1>
	<div class="white-bar"></div>
</section>

<?php get_template_part('resources/components/breadcrumbs'); ?>

<section id="knowledgebase">
	<div class="container">
		<div class="row">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			setPostViews(get_the_ID()); ?> 
			<div class="col-12 col-lg-8 col-xl-8">
				<div class="d-flex">
					<div class="post_thumb">
						<?php the_post_thumbnail(); ?>
					</div>
					<h1 class="post-title"><?php echo the_title(); ?></h1>
				</div>
				<div class="post-details d-flex">
					<div class="author-image ">
					<?php get_avatar( get_the_author_meta('user_email'), $size = '30'); ?>
					</div>
					<div>
						<a href="<?php get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' )); ?>"><?php the_author(); ?></a> - <?php get_the_date(); ?> 
					</div>
					<div class="ml-auto"><?php get_the_category_list(); ?> </div>
				</div>
				<?php the_content(); ?>
				
				<?php if($relatedPosts):?>
				<div class="post-box grey">
					<h3><?php echo $relatedPostTitle; ?></h3>
					<div class="related-posts d-flex">

					<?php foreach ($relatedPosts as $post): ?>
						<div class="post-related col-4">
								<div class="post-thumbnail">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
								</div>
								<div class="post-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute() ?>">
										<?php the_title(); ?>
									</a>
								</div>
							</div>
					<?php endforeach; ?>
					<?php wp_reset_query(); ?>
					</div>
				</div>
				<?php endif; ?>
			
			</div>
			<div class="col-12 col-lg-4 col-xl-4 sidebar d-none d-lg-block">
				<div class="sidebar-box">
					<div class="box-title">
						Recente artikelen
					</div>
					<div class="box-content">
						<ul class="post-list">
						<?php
						global $post;
						$category = get_the_category($post->ID);
						$category = $category[0]->cat_ID;
						$myposts = get_posts(array('numberposts' => $numberOfRecentPosts, 'offset' => 0, 'category__in' => array($category), 'post_status'=>'publish' ));
						foreach($myposts as $post) :
						setup_postdata($post);
						?>
						<li>
							<div class="d-flex">
								<div class="post-thumbnail">
									
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
									
								</div>
								<div class="post-body">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <br> <span class="post-date"><?php get_the_date(); ?></span>
								</div>
							</div>
						</li>
						<?php endforeach; ?>
						</ul>
						<?php wp_reset_query(); ?>
					</div>
				</div>
				<div class="sidebar-box">
					<div class="box-title">
						Populaire artikelen 
					</div>
					<div class="box-content">
						<ul class="post-list">
						<?php 
						 $args = array(
						    'posts_per_page' => $numberOfPopulairPosts,
							'meta_key' => 'post_views_count',
							'orderby' => 'meta_value_num',
							'order' => 'DESC'
						  );
						  ?>
						<?php query_posts($args);
						if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<li>
								<div class="d-flex">
									<div class="post-thumbnail">
										
											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
										
									</div>
									<div class="post-body">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <br> <span class="post-date"><?php get_the_date(); ?></span>
									</div>
								</div>
							</li>
						<?php else: endif; ?>
						</ul>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>