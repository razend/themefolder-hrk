<?php 
$title = get_sub_field('title');
$text = get_sub_field('content');

$img = get_sub_field('image');

?>

<div class="container">
	<div class="row">
		<?php if ($title): ?>
			<div class="col-12 mb-5 text-start text-lg-center">
				<h1><?php echo $title; ?> </h1>
			</div>
		<?php endif; ?>
		<div class="col-12 content col-lg-6 d-flex align-items-start flex-column">
			<div class="content">
				<?php echo $text;
				get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
			</div>			
		</div>
		<div class="col-12 col-lg-6 d-flex align-items-center">
			<div class="image">
				<img src="<?php echo $img; ?>" alt="" style="width: 100%;">
			</div>
		</div>
	</div>
</div>
<?php wp_reset_query(); ?>