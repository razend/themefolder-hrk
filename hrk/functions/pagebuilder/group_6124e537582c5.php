<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_6124e537582c5',
	'title' => 'Kleurcodes',
	'fields' => array(
		array(
			'key' => 'field_5e14972d7a061',
			'label' => 'kleurcodes',
			'name' => '',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'message' => 'Thema kleuren:
oranje: #ed6c05
blauw: #0080a4
grijs: #a9a9a9
paars: #0e3175

licht blauw: #e1eff3
licht grijs: #f6f6f6
licht oranje: #ffefe1
licht groen: f2f2f0

midden oranje: #ffc691
midden blauw: #8dcbda
zandkleur bruin: d8c6b0
zandkleur geel: #efd0a1

Tekstkleuren
paars: #0e3175
blauw: #0080a4
groen: # 056283',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-marketing.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'acfe_display_title' => '',
	'acfe_autosync' => array(
		0 => 'php',
	),
	'acfe_form' => 0,
	'acfe_meta' => '',
	'acfe_note' => '',
	'modified' => 1629808122,
));

endif;