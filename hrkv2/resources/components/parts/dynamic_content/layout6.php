<?php 
	// Blogs
	$content = get_sub_field('content');

	$items_selection = get_sub_field('post_selection'); 
	$posttype = get_post_type(get_the_ID());

	$bg_color = get_sub_field('bg_color');
 ?>


<?php 

// Slider instellingen
// $slider_effect = get_sub_field('slider_effect');
// $slider_interval = get_sub_field('slider_interval');
// $interval_speed = get_sub_field('slide_interval_speed');
// $show_indicators = get_sub_field('show_indicators');
// $indicator_style = get_sub_field('indicator_style');
// $show_controls = get_sub_field('show_controls');
// $carousel_height = get_sub_field('carousel_height');


$carousel_number = rand(0, 1000); ?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<?php echo $content; ?>
		</div>
		<div class="col-12 position-relative">
			<div class="swiper <?php echo "swiper_" . $carousel_number; ?> mb-5">
				<div class="swiper-wrapper">
					<?php foreach( $items_selection as $post ): 
						setup_postdata($post); 
							 ?>
							 <div class="swiper-slide">

								<div class="card h-100">
									<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
									<div class="card-body">
										<p class="card-details pb-0">
											
										</p>
										<h5 class="card-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
										<p class="card-text"><?php echo get_the_excerpt(); ?></p>
										<div class="d-flex justify-content-left mt-3">
											<a href="<?php echo the_permalink(); ?>" class="btn style_filled_orange size_small">lees verder</a>
										</div>
									</div>
									<div class="card-footer <?php echo $bg_color; ?>">
										<?php if(has_category('',$post->ID)): ?>
											<div class="category_list">
												<strong>categorie:</strong>
												<?php echo the_category(', '); ?>
											</div>
										<?php endif; ?>
										<?php if(has_tag()): ?>
											<?php the_tags( '<strong>tags</strong>: ', ); ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>


					
				</div>
				<div class="swiper-pagination"></div>

				
			</div>
			<div class="swiper-button-next navigation_<?php echo $carousel_number; ?>"></div>
	    	<div class="swiper-button-prev navigation_<?php echo $carousel_number; ?>"></div>	


		  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins/swiper/swiper-bundle.min.js"></script>

		  <!-- Initialize Swiper -->
		  <script>
		    var swiper = new Swiper(".<?php echo "swiper_" . $carousel_number; ?>", {

		    	// autoheight: true,
		      navigation: {
		        nextEl: ".swiper-button-next.navigation_<?php echo $carousel_number; ?>",
		        prevEl: ".swiper-button-prev.navigation_<?php echo $carousel_number; ?>",
		      },
		      slidesPerView: 1,
			spaceBetween: 15,
		       breakpoints: {
			    // when window width is >= 320px
				    576: {
				      slidesPerView: 1
				    },
				    // when window width is >= 640px
				    992: {
				      slidesPerView: 3
				    }
				  },
		      pagination: {
				    el: '.swiper-pagination',
				    type: 'bullets',
				    clickable: true,
				  },
		    });
		  </script>
		</div>
	</div>
</div>

<div class="col-12">
			<?php get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
		</div>
