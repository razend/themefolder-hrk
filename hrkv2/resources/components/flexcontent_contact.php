<?php 

	// Layout settings
	$layout = get_sub_field('layout_selection');

	get_template_part( '/resources/components/parts/general/section_id');
	
	get_template_part('resources/components/parts/general/section_opening'); 

	if($layout == 'layout1'):

		get_template_part('resources/components/parts/contact/layout1');

	endif;

get_template_part('resources/components/parts/general/section_close');

wp_reset_query(); ?>