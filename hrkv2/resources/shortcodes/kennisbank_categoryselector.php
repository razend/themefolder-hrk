<div class="category-selector w-100">
	<nav class="navbar navbar-expand-lg align-items-lg-center justify-content-center" role="navigation">
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#kennisbank_cats" aria-bs-controls="navBar" aria-bs-expanded="false" aria-bs-label="Toggle navigation">
			<div class="d-flex align-items-lg-center">
				<i class="fas fa-bars"></i> <span style="padding: 4px 0 0 17px;">bekijk categorieën</span></div>
		</button>
	
		<?php
			wp_nav_menu( array(
				'theme_location'	=> 'kennisbank',
				'depth'				=> 2,
				'container'			=> 'div',
				'container_class'	=> 'collapse navbar-collapse',
				'container_id'		=> 'kennisbank_cats',
				'menu_class'		=> 'kennisbank d-flex flex-wrap justify-content-center nav',
				'fallback_cb'		=> '__return_false',
				'walker'			=> new bootstrap_5_wp_nav_menu_walker()
			) );				
		?>
	</nav>
</div> 