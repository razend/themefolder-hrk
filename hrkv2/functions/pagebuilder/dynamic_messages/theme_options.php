<?php 

// Nav height
add_action('acf/render_field/name=dm_nav_height', 'dm_nav_height');
function dm_nav_height(){
    echo 'Stel hier de hoogte in van de navigatiebalk. Deze instelling geldt niet voor mobiele apparaten, hier wordt hoogte automatisch bepaald.';
}
// Nav effect
add_action('acf/render_field/name=dm_nav_effect', 'dm_nav_effect');
function dm_nav_effect(){
    echo '- Geen effect: Staat vast boven het eerste blok en scrolt gewoon mee naar boven.<br>
- Sticky: Navigatiebalk blijft tijdens het scrollen altijd bovenaan het venster staan.<br>
- Zwevend: Navigatiebalk zweeft boven eerst content blok.<br>
- Verklein tijdens scrollen: Zodra de bezoeker naar onder scrolt, wordt de navigatie wat compacter. Dit heeft enkel effect bij normale en hoge navigaties.';
}

// Algemene kleuren
add_action('acf/render_field/name=dm_small_elements', 'dm_small_elements');
function dm_small_elements(){
    echo 'Hiermee stel je de kleuren in voor bijvoorbeeld de pijltjes en dots van een slider, quote tekentjes bij testimonials of pijltjes bij een accordion blok';
}



?>