<?php
// Selectie van activiteiten op homepage
$servicesOrder = get_field('servicesOrder','option');

$headerimage = get_field('headerimage');
$customheaderimage = get_field('custom_image_header');
$show_colored_space = get_field('show_colored_space');
$show_colored_space_footer = get_field('show_colored_space_footer');
$headerImageTitle = get_field('title_in_image_top');
$footerimage = get_field('footerimage');
$footerImageTitle = get_field('title_in_image_bottom');
$customfooterimage = get_field('custom_image_footer');
$show_colored_space_footer = get_field('show_colored_space_footer');

?>
<!-- Enkele pagina -->
@if(is_singular('services'))
	@if(get_field('headerimage') == 'custom_image')
	<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom {{$show_colored_space}}" style="background-image: url({{$customheaderimage}});">
	@else
	<section id="fullwidth-image" class="{{$headerimage}} d-flex justify-content-start align-items-center">
	@endif
		<h1>{{$headerImageTitle}}</h1>
		<div class="white-bar"></div>
	</section>
	@if( get_field('orangebar') == 'yes' )
	<section id="fullwidth-image" class="orangebar">
		<div class="white-bar"></div>
	</section>
	@endif
	<section>
		<div class="container">
			<div class="row">
				<div class="col-12 text-left">
					<h1>{{the_title()}}</h1>
					<div class="content">
						{{the_field('service_content')}}
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="servicepoints">
		<div class="container">
			<div class="row">
				@acfrepeater('service_points_row_1')
				<div class="col servicepoint justify-content-center">
					<img src="{{the_sub_field('service_icon')}}" class="service_icon" alt="">
					<p>{{ get_sub_field( 'service_point' ) }}</p>
				</div>
				@acfend
			</div>
			<div class="row">
				@acfrepeater('service_points_row_2')
				<div class="col servicepoint justify-content-center">
					<img src="{{the_sub_field('service_icon')}}" class="service_icon" alt="">
					<p>{{ get_sub_field( 'service_point' ) }}</p>
				</div>
				@acfend
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-12 text-left">
					<div class="content">
						{{the_field('service_content_part2')}}
					</div>
				</div>
				<div class="col-12 text-center mt-5 mb-5">
					<h1 >{{the_field('service_quote')}}</h1>
				</div>
			</div>
		</div>
	</section>

	@if(get_field('show_meetingbanner') == 'meetingbanner_on')
		
		<?php 
	$person1 = get_field('person1');
	$person2 = get_field('person2');
	$content = get_field('content');
	$btn_text = get_field('btn_text');
	?>
	<section id="blue" class="meetingbanner">
		<div class="container">
			<div class="row ">
				<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
					<div class="teammember-image rounded">
						<img src="{{get_the_post_thumbnail_url($person1)}}" alt="">
					</div>
					<div class="teammember-name">{{get_the_title($person1)}}</div>
				</div>
				@if($person2)
				<div class="col-12 col-md-8 d-flex">
					<h1>{{$content}}</h1>
				@else
				<div class="col-12 col-md-10 align-items-top ">
					<h1 style="text-align: left;">{{$content}}</h1>
					<a class="btn mettingbanner-button" data-appointlet-organization="hetrecruitingkantoor">{{$btn_text}}<script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
				@endif

					
				</div>

				@if($person2)
				<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
					<div class="teammember-image rounded">
						<img src="{{get_the_post_thumbnail_url($person2)}}" alt="">
					</div>
					<div class="teammember-name">{{get_the_title($person2)}}</div>
				</div>
				
				<div class="col-12 d-flex align-items-center justify-content-center">
					<a class="btn mettingbanner-button" data-appointlet-organization="hetrecruitingkantoor">{{$btn_text}}<script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
				</div>
				@endif

			</div>
		</div>
	</section>

	@endif

	@if(get_field('footerimage') == 'custom_image')
	<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom {{$show_colored_space_footer}}" style="background-image: url({{$customfooterimage}});">
	@else
	<section id="fullwidth-image" class="{{$footerimage}} d-flex justify-content-start align-items-center">
	@endif
		<h1>{{$footerImageTitle}}</h1>
		<div class="white-bar"></div>
	</section>
	@if( get_field('orangebar_footer') == 'yes' )
		<section id="fullwidth-image" class="orangebar">
			<div class="white-bar"></div>
		</section>
	@endif
@else
<!-- Overzicht -->

<section id="services">
	<div class="container">
		<div class="row row-eq-height">
			@foreach ($servicesOrder as $post)
	
			<div class="col-12 col-md-6 service ">
				<div class="thumbnail d-flex align-items-end "style="background-image: url({{the_post_thumbnail_url()}});" onclick="window.location.href='{{the_permalink()}}'">

						<a class="" href="{{the_permalink()}}" >
						{{the_field('service_photo_text')}}
					</a>

				</div>
				<a class="title" href="{{the_permalink()}}">{{the_title()}}</a>
					{{the_field('service_content_excerpt')}}
				<a class="readmore" href="{{the_permalink()}}">lees meer</a>
			</div>
			@endforeach
		</div>
	</div>
</section>
<?php wp_reset_query(); ?>

@endif