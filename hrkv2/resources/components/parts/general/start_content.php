<?php 
	$content = get_sub_field('content');
 ?>

<div class="container">
	<div class="row justify-content-center mb-5">
		<div class="col-12 col-md-8">
			<?php echo $content; ?>
		</div>
	</div>
</div>