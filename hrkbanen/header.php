<!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="utf-8">
	<title>@if (is_front_page())
	{{bloginfo()}} - {{bloginfo('description')}}
	@elseif (is_category(array('portfolio','blog')))
	{{single_cat_title()}} | {{bloginfo()}} - {{bloginfo('description')}}
	@else
	{{the_title()}} | {{bloginfo()}} - {{bloginfo('description')}}
	@endif</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
	<meta name="author" content="Razend" />
	<link rel="apple-touch-icon" sizes="180x180" href="{{get_stylesheet_directory_uri()}}/images/favicons/apple-touch-icon.png?v=BG7kXY0LoJ">
	<link rel="icon" type="image/png" sizes="32x32" href="{{get_stylesheet_directory_uri()}}/images/favicons/favicon-32x32.png?v=BG7kXY0LoJ">
	<link rel="icon" type="image/png" sizes="16x16" href="{{get_stylesheet_directory_uri()}}/images/favicons/favicon-16x16.png?v=BG7kXY0LoJ">
	<link rel="manifest" href="{{get_stylesheet_directory_uri()}}/images/favicons/site.webmanifest?v=BG7kXY0LoJ">
	<link rel="mask-icon" href="{{get_stylesheet_directory_uri()}}/images/favicons/safari-pinned-tab.svg?v=BG7kXY0LoJ" color="#5bbad5">
	<link rel="shortcut icon" href="{{get_stylesheet_directory_uri()}}/images/favicons/favicon.ico?v=BG7kXY0LoJ">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-config" content="{{get_stylesheet_directory_uri()}}/images/favicons/browserconfig.xml?v=BG7kXY0LoJ">
	<meta name="theme-color" content="#ffffff">
	
	<?php if(get_field('google_script','option') == 'gtag_manager'):
		the_field('google_tagmanager_code_snippet_head','option');
	endif; ?>
	<?php wp_head() ?>
	<?php if(get_field('chatprovider','option') == 'zendesk'):
		the_field('chat_zendesk_snippet','option');
		endif;
	 ?>
	 <script src="{{get_template_directory_uri()}}/js/bootstrap.min.js"></script>
</head>
<body class="@if (is_404()) error_404 @endif" >
	@if(get_field('google_script','option') == 'gtag_manager')
		{{the_field('google_tagmanager_code_snippet_body','option')}}
	@endif
	@if (!is_404())
	<style>
.flip-card {
  background-color: transparent;
  width: 148px;
  height: 80px;
  perspective: 1000px;
}

.left-side {
	width: 53px;
	height: 40px;
}

.flip-card-inner {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.6s;
  transform-style: preserve-3d;
}

.flip-card:hover .flip-card-inner {
  transform: rotateX(180deg);
}

.flip-card-front, .flip-card-back {
  position: absolute;
  width: 100%;
  height: 100%;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
}

.flip-card-front {
  color: black;
}

.flip-card-back {
  color: white;
  transform: rotateX(180deg);
}
	</style>
	<header>
		<section id="nav" class="no-padding">
			<div class="container-fluid d-lg-flex justify-content-lg-center">
				<nav class="navbar navbar-expand-lg align-items-lg-center" role="navigation">
					
					<div class="flippin_logo d-flex pt-2 pb-2">
						<div class="left-side">
							<img src="{{get_stylesheet_directory_uri()}}/images/logo_left_side.png" alt=""  style="width: 100%">
						</div>
						<div class="flip-card">
							<div class="flip-card-inner">
								<div class="flip-card-front">
									<img src="{{get_stylesheet_directory_uri()}}/images/logo_hmk_right_side.png" alt="" style="width: 100%">
								</div>
								<div class="flip-card-back">
									<img src="{{get_stylesheet_directory_uri()}}/images/logo_hrk_right_side.png" alt="" style="width: 100%">
								</div>
							</div>	
						</div>
						
					</div>

					<!-- Brand and toggle get grouped for better mobile display -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
					<!-- <a class="navbar-brand" href="#">Navbar</a> -->
		
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'primary',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'collapse navbar-collapse',
							'container_id'		=> 'navBar',
							'menu_class'		=> 'nav navbar-nav align-items-xl-center',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
				</nav>
				
			</div>
		</section>
	</header>
	@endif