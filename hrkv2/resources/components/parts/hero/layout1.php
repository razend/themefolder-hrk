<?php 
	// 50/50 block
	$text = get_sub_field('content');
	$subtext = get_sub_field('subtext');

	// Layout settings
	$layout = get_sub_field('layout_selection');

	// Background settings
	$background_settings = get_sub_field('section_background_settings');
	$section_bg = $background_settings['section_bg_img'];
	$background_position_vertical = $background_settings['background_position'];
	$background_position_horizontal = $background_settings['horizontale_uitlijning_van_afbeelding'];

	// Dev instellingen
	$developer_options = get_sub_field('developer_options');
	$section_class = $developer_options['section_class'];

 ?>

<section class="hero_block <?php echo $section_class . " " . $layout; ?>">
	<div class="container-fluid px-0 ">
		<div class="row g-0">
			<div class="col-12 col-lg-6 content_block">
				<div class="content">
					<div class="title">
						<?php echo $text; ?>
					</div>
					<div class="subcontent">
						<?php echo $subtext; ?>
					</div>
					<?php get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
				</div>

			</div>
			<div class="col-12 col-lg-6 image_block">
				<div class="d-flex h-100">
					<img src="<?php echo $section_bg; ?>" class="w-100" alt="" style="object-position: <?php echo $background_position_vertical . " " . $background_position_horizontal;  ?>">
				</div>
			</div>
		</div>
	</div>
</section>

<?php wp_reset_query(); ?>