<?php 
	$headerimage = get_field('headerimage');
	$custom_image_top = get_field('custom_image_top');
	$headerImageTitle = get_field('title_in_image_top');
	$headerImageSubTitle = get_field('subtitle_in_image_top');
	$customheaderimage = get_field('custom_image_header');
	$form_id = get_field('form_id');
?>
<?php if(get_field('headerimage') == 'custom_image'): ?>

<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space; ?> <?php if($header_less_height == 'yes'): echo "less-height"; endif; ?>" style="background-image: url(<?php echo $customheaderimage; ?>);">
<?php else: ?>
<section id="fullwidth-image" class="<?php echo $headerimage; ?> d-flex justify-content-start align-items-center <?php if($header_less_height == 'yes'): echo "less-height"; endif; ?>">
<?php endif; ?>
		<span><?php echo $headerImageTitle; ?></span>
		<?php if (get_field('subtitle_in_image_top')): ?>
		<span><?php echo $headerImageSubTitle; ?></span>
		<?php endif; ?>
	<div class="white-bar"></div>
</section>

<?php if( get_field('orangebar') == 'yes' ): ?>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
<?php endif; ?>

<section id="content">
	<div class="landingpage">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8">
					<p><?php echo the_content(); ?></p>
				</div>
				<div class="col-12 col-lg-4">
					<div class="contact_block">
						<?php gravity_form( $form_id, false, false, false, '', false ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if( get_field('orangebar_footer') == 'yes' ): ?>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
<?php endif; ?>