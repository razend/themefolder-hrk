<?php 
$title = get_sub_field('title');
$text = get_sub_field('content');

$img = get_sub_field('image');

?>

<div class="container">
	<div class="row">
		<div class="col-12 col-md-6 image">
			<img src="<?php echo $img; ?>" alt="" style="width: 100%;">
		</div>
		<div class="col-12 content col-md-6 d-flex align-items-start flex-column">
			<div class="title">
				<h2><?php echo $title; ?></h2>
			</div>
			<div class="text mt-auto">
				<?php echo $text;
				get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
			</div>			
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>