<?php 

$layout = get_sub_field('layout_selection');

wp_reset_query();
$postid = get_the_ID();

?>

<div class="container <?php echo $layout; ?>">
	<div class="row justify-content-center">
		<?php $swiper_number = rand(0, 1000); ?>
		<div class="col-12 position-relative <?php echo "swipernav" . $swiper_number; ?>">
			<div class="swiper <?php echo "swiper" . $swiper_number; ?>">
				  <div class="swiper-wrapper" style="padding-bottom: 30px;">
				 
					<?php if( have_rows('company_logos') ): while( have_rows('company_logos') ) : the_row();
						$image =  get_sub_field('company_logo');
						?>
						<div class="swiper-slide d-flex align-items-stretch">
						<div class="card">
							<div class="quote d-flex justify-content-center align-items-center h-100">
								<div class="image">
									<img src="<?php echo $image; ?>" alt="">
								</div>
							</div>
						</div>
					</div>
						<?php endwhile; endif; ?>
				  </div>
				  <div class="swiper-pagination"></div>
			</div>

			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins/swiper/swiper-bundle.min.js"></script>
	<!-- Initialize Swiper -->
	<script>
	  var swiper = new Swiper("<?php echo ".swiper" . $swiper_number; ?>", {
		// autoHeight: true,
		loop: true,
		loopFillGroupWithBlank: true,
		pagination: {
		  el: ".swiper-pagination",
		  clickable: true,
		  type: 'bullets',
		},
		navigation: {
		  nextEl: "<?php echo ".swipernav" . $swiper_number; ?> .swiper-button-next",
		  prevEl: "<?php echo ".swipernav" . $swiper_number; ?> .swiper-button-prev",
		},
		breakpoints: {
		// when window width is >= 320px
		576: {
		  slidesPerView: 1,
		  spaceBetween: 20
		},
		// when window width is >= 480px
		768: {
		  slidesPerView: 2,
		  spaceBetween: 30
		},
		// when window width is >= 640px
		1200: {
		  slidesPerView: 3,
		  spaceBetween: 40
		}
	  }
	  });
	</script>
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>