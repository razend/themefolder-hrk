<?php 
	$headerimage = get_field('headerimage');
	$customheaderimage = get_field('custom_image_header');
	$show_colored_space = get_field('show_colored_space');
	$headerImageTitle = get_field('title_in_image_top');
	$midimage = get_field('midimage');
	$footerimage = get_field('footerimage');
	$footerImageTitle = get_field('title_in_image_bottom');
	$customfooterimage = get_field('custom_image_footer');
	$show_colored_space_footer = get_field('show_colored_space_footer');

	$servicesOrder = get_field('servicesOrder','option');

?>
@if(get_field('headerimage') == 'custom_image')
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom {{$show_colored_space}}" style="background-image: url({{$customheaderimage}});">
@else
<section id="fullwidth-image" class="{{$headerimage}} d-flex justify-content-start align-items-center">
@endif
	<span>{{$headerImageTitle}}</span>
	<div class="white-bar"></div>
</section>	
@if( get_field('orangebar') == 'yes' )
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
@endif

<section id="content">
	<div class="container">
		<div class="row justify-content-center">
			@wpposts
			<div class="col-12 col-lg-10 col-xl-8">
				{{the_content()}}
			</div>
			@wpempty
			404
			@wpend
		</div>
	</div> 
</section>

@include ('resources/components/services')



@if(get_field('footerimage') == 'custom_image')
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom {{$show_colored_space_footer}}" style="background-image: url({{$customfooterimage}});">
@else
<section id="fullwidth-image" class="{{$footerimage}} d-flex justify-content-start align-items-center">
@endif

	<span>{{$footerImageTitle}}</span>
	<div class="white-bar"></div>
</section>	
@if( get_field('orangebar_footer') == 'yes' )
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
@endif