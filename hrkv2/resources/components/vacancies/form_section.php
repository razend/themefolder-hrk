<?php 
	// Recruiters
	$recruiter = get_field('vacancie_recruiters');
	$recruiter_function = get_field('team_function', $recruiter->ID);
	$featured_image = get_the_post_thumbnail_url( $recruiter->ID, "medium");

	// Form instellingen
	$form_id = get_field('form_id');
	// $form_url = get_field('form_url');
	$formbox_content = get_field('formbox_content');

	// vacature instellingen
 ?>

<section id="form_section">
	<div class="container form-container">
		<div class="row">
			<div class="col-12 col-xl-6 form">
				<?php // if($form_id): ?>
					<div id="form">
						<h4>solliciteer direct</h4>
						<?php gravity_form( $form_id, false, false, false, '', false ); ?>
					</div>
				<?php // elseif($form_url): ?>
					<!-- <iframe id="form" width="100%" height="1000px;" src="<?php echo $form_url; ?>" height=""></iframe> -->
				<?php // endif; ?>
			</div>
			<div class="col-12 col-xl-6 content">
				<?php echo $formbox_content; ?>
				<div class="d-flex contact-box">
					<div class="contact-items d-flex flex-grow-1">

						<?php get_template_part('resources/components/vacancies/recruiter_contact_options'); ?>

					</div>
					<div class="recruiter">
						<div class="d-flex align-items-center">
							<i class="fa-solid fa-user"></i>
							<p class="team_name pb-0"><strong><?php echo ( $recruiter->post_title ); ?></strong><br><?php echo $recruiter_function; ?></p>
							<div class="recruiter-photo">
								<img src="<?php echo $featured_image; ?>" alt="" class="thumbnail">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>