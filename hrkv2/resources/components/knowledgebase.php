<?php 
	$category = get_the_category();
	$firstCategory = $category[0]->cat_name;
 ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="col-12 col-lg-4">
					<div class="card">
						<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
						<div class="card-body">
							<p class="card-details pb-0">
								
							</p>
							<h5 class="card-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
							<p class="card-text"><?php echo get_the_excerpt(); ?></p>
							<div class="d-flex justify-content-center">
								<a href="<?php echo the_permalink(); ?>" class="btn filled small rounded-corners">lees verder</a>
							</div>
						</div>
						<div class="card-footer <?php echo $bg_color; ?>">
							<div class="category_list">
								<strong>categorie:</strong>
								<?php echo the_category(', '); ?>
							</div>
							<?php the_tags( '<strong>tags</strong>: ', ); ?>
						</div>
					</div>
				</div>
<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>