<?php 
if( function_exists('acf_add_options_page') ) {
 
	$page = acf_add_options_page(array(
		'page_title' 	=> 'Theme opties',
		'menu_title' 	=> 'Theme opties',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'manage_options',
		'redirect' 	=> false
	)); 

	acf_add_options_page(array(
	'page_title' 	=> 'Vacature opties',
	'menu_title' 	=> 'Vacature opties',
	'menu_slug' 	=> 'vacancie_options',
	'capability' 	=> 'edit_posts', 
	'parent_slug'	=> 'edit.php?post_type=vacancies',
	'position'		=> false,
	'icon_url' 		=> 'dashicons-images-alt2',
	'redirect'		=> false,
	));

	acf_add_options_page(array(
	'page_title' 	=> 'Diensten opties',
	'menu_title' 	=> 'Diensten opties',
	'menu_slug' 	=> 'services_options',
	'capability' 	=> 'edit_posts', 
	'parent_slug'	=> 'edit.php?post_type=services',
	'position'		=> false,
	'icon_url' 		=> 'dashicons-images-alt2',
	'redirect'		=> false,
	));


	if( function_exists('acf_add_options_sub_page') ) {
		// acf_add_options_sub_page(array(
		// 	'title' => 'Homepage',
		// 	'parent' => 'theme-general-settings',
		// 	'capability' => 'manage_options',
		// ));
		acf_add_options_sub_page(array(
			'title' => 'Bedrijfsgegevens',
			'parent' => 'theme-general-settings',
			'capability' => 'manage_options'
		));
	}
}

?>