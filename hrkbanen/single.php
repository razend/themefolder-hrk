@include('header')

	@if(is_singular( 'services' ))
		@include ('resources/components/services')
	@elseif(is_singular( 'team' ))
		@include ('resources/components/teammember')
	@elseif(is_singular( 'vacancies' ))
		@include ('resources/components/vacancies')
	@else
		@include ('resources/views/layouts/single')
	@endif
	
@include('footer')