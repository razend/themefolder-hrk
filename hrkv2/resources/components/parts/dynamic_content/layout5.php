<?php 
	// Blogs
	$content = get_sub_field('content');

	$items_selection = get_sub_field('post_selection'); 
	$posttype = get_post_type(get_the_ID());

	$bg_color = get_sub_field('bg_color');
 ?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<?php echo $content; ?>
		</div>

			<?php foreach( $items_selection as $post ): 
		setup_postdata($post); 
			 ?>
			 <div class="col-12 col-lg-4">
				<div class="card">
					<a href="<?php echo the_permalink(); ?>"><img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt=""></a> 
					<div class="card-body">
						<p class="card-details pb-0">
							
						</p>
						<h5 class="card-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
						<p class="card-text"><?php echo get_the_excerpt(); ?></p>
						<div class="d-flex justify-content-left mt-3">
							<a href="<?php echo the_permalink(); ?>" class="btn style_filled_orange size_small">lees verder</a>
						</div>
					</div>
					<div class="card-footer <?php echo $bg_color; ?>">
						<?php if(has_category('',$post->ID)): ?>
							<div class="category_list">
								<strong>categorie:</strong>
								<?php echo the_category(', '); ?>
							</div>
						<?php endif; ?>
						<?php if(has_tag()): ?>
							<?php the_tags( '<strong>tags</strong>: ', ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
		<div class="col-12">
			<?php get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
		</div>
	</div>
</div>