<?php 
	//Layout: Services

	$services_orange_box = get_field('services_orange_box','option');
	$show_orange_box = get_sub_field("show_orange_box");

	$items_selection = get_sub_field('post_selection'); 
	$posttype = get_post_type(get_the_ID());
 ?>

<div class="container">
	<div class="row gx-md-5">
		<?php foreach( $items_selection as $post ): 
		setup_postdata($post); 
		
		$title = get_field('title_for_overview');
		$subtitle = get_field('subtitle_for_overview');
		$cardcontent = get_field('service_content_excerpt'); 
		$overview_subtext = get_field('subtext_for_overview'); ?>

		<div class="col-12 col-lg-6 col-xl-4 mb-5">
			<div class="card <?php echo $posttype; ?> h-100">

				<?php if(has_post_thumbnail()): ?>
					<a class="featured_image" href="<?php echo get_permalink(); ?>">
						<img class="img-fluid" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_url(['alt' => get_the_title()]); ?>" />
					</a>
				<?php endif; ?>
				<div class="background-color"></div>
				<div class="card-body">
					<h4 class="card-title">
						<a href="<?php the_permalink(); ?>">
							<?php if ($title):
								echo $title;
							else:
								the_title();
							endif; ?>
								
						</a>
					</h4>
					<div class="subtitle"><?php echo $subtitle; ?></div>
					<div class="card-text">
						<p><?php echo $overview_subtext; ?></p>
						<a href="<?php echo get_permalink(); ?>" class="">Lees verder</a>
					</div>
					
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		<?php if ($show_orange_box  && in_array('show', $show_orange_box) ): ?>
		<div class="col-12 col-lg-6 col-xl-4 mb-5">
			<div class="card <?php echo $posttype; ?> h-100 orangebox">
				<div class="background-color" style="background-color: #b8b8b8;"></div>
				<div class="card-body">
					<div class="card-text">
						<h3><?php echo $services_orange_box; ?></h3>
					</div>

				</div>
			</div>
		</div>
	<?php endif; ?>
	</div>
</div>

<?php wp_reset_query(); ?>