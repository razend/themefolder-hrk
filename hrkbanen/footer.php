	<?php if (!is_404()): ?>
		<footer>
			<div class="white-bar"></div>
			<div class="container">
				<div class="row">
					<div class="col-6 col-md-4 col-lg-2 col-xl-3">
						<h4>sitemap</h4>
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'footer_sitmap',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'footer-nav',
							'container_id'		=> 'footer-nav',
							'menu_class'		=> 'nav navbar-nav',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-3">
						<h4>algemeen</h4>
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'footer_general',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'footer-nav',
							'container_id'		=> 'footer-nav',
							'menu_class'		=> 'nav navbar-nav',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
					</div>
					<div class="col-12 col-md-4">
						<h4>contact</h4>
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<div class="contact-part">
								<span itemprop="streetAddress"><i class="fas fa-map-marker-alt float-left"></i></i>{{the_field('address','option')}}<br>
								<?php if (get_field('address_2','option')):
									echo the_field('address_2','option'). "<br>";
								endif; ?>
								</span>
								<span itemprop="postalCode" class="zipcode"><?php echo the_field('zipcode','option'); ?></span>
								<span itemprop="addressLocality"><?php echo the_field('city','option'); ?></span>
							</div>
							<div class="contact-part">
								<?php if(get_field('mobilenumber','option')): ?>
					 			<a href="tel:<?php echo the_field('mobilenumber','option'); ?>"><i class="fas fa-phone float-left"></i><span itemprop="telephone"><?php echo the_field('mobilenumber','option'); ?></span></a><br>
					 			<?php endif; ?>
					 			<?php if(get_field('phonenumber','option')): ?>
								<a href="tel:<?php echo the_field('phonenumber','option'); ?>"><i class="fas fa-phone float-left"></i><span itemprop="telephone"><?php echo the_field('phonenumber','option'); ?></span></a>
								<?php endif; ?>
							</div>
							<?php if(get_field('emailaddress','option')): ?>
							<div class="contact-part">
								<a href="mailto:<?php echo the_field('emailaddress','option'); ?>"><i class="far fa-envelope float-left"></i><span itemprop="email"><?php echo the_field('emailaddress','option'); ?></span></a>
							</div>
							<?php endif; ?>
							<div class="contact-part">
								<?php get_template_part( '/resources/components/socialmedia'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="vertical-bar  d-none d-lg-block">
				
			</div>
			<div class="logo-area d-none d-lg-block">
				<div class="flippin_logo d-flex">
						<div class="left-side">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_left_side.png" alt=""  style="width: 100%">
						</div>
						<div class="flip-card">
							<div class="flip-card-inner">
								<div class="flip-card-front">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_hmk_right_side.png" alt="" style="width: 100%">
								</div>
								<div class="flip-card-back">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_hrk_right_side.png" alt="" style="width: 100%">
								</div>
							</div>	
						</div>
						
					</div>
			</div>
		</footer>
		<section id="copyright">
			<div class="container">
				<div class="row">
					<div class="col-12">
						© copyright <?php echo date(Y) . the_field('company_name','option');?> <span class="spacer"> | </span><a href="http://frenkdesign.nl" target="_blank">ontwerp: frenkdesign</a> | <a href="https://razend.net">ontwikkeling: razend</a> | <a href="http://niqui.nu" target="_blank">teksten: niqui</a> | <a href="http://www.reedersphotography.myportfolio.com/" target="_blank">fotografie: reeders photography</a>
					</div>
				</div>
			</div>
		</section>
		<?php endif; ?>
		<?php if(get_field('google_script','option') == 'analytics_code'):
			get_template_part('resources/components/analytics');
		endif;
		
		if(get_field('chatprovider','option') == 'replain'):
			echo the_field('chat_replain_snippet','option');
		endif;
		wp_footer();?>
		
		<script src="<?php echo get_template_directory_uri();?>/js/min/bootstrap-4-navbar.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/global.js"></script>
		
	</body>
</html>