<?php 
wp_reset_query();
$postid = get_the_ID();
$carousel_number = rand(0, 1000);

$content = get_sub_field('content');
$layout = get_sub_field('layout_selection');

get_template_part( '/resources/components/parts/general/section_id');

get_template_part( '/resources/components/parts/general/section_opening'); 

	if ($content):
		
		get_template_part( '/resources/components/parts/general/start_content');

	endif;

	if ($layout == 'layout1'):

		get_template_part( '/resources/components/parts/quote_slider/layout1');

	elseif ($layout == 'layout2'):

		get_template_part( '/resources/components/parts/quote_slider/layout2');

	endif; 

get_template_part( '/resources/components/parts/general/section_close');

wp_reset_query(); ?>
