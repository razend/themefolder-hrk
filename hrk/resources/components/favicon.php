<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicons/apple-touch-icon.png?v=BG7kXY0LoJ">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicons/favicon-32x32.png?v=BG7kXY0LoJ">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicons/favicon-16x16.png?v=BG7kXY0LoJ">
<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicons/site.webmanifest?v=BG7kXY0LoJ">
<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicons/safari-pinned-tab.svg?v=BG7kXY0LoJ" color="#5bbad5">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicons/favicon.ico?v=BG7kXY0LoJ">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="<?php echo get_stylesheet_directory_uri(); ?>/images/favicons/browserconfig.xml?v=BG7kXY0LoJ">
<meta name="theme-color" content="#ffffff">