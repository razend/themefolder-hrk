<?php
// Set the Current Author Variable $curauth
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
	$author_id = get_the_author_meta('ID');
	$team_id = get_field('user_teammember_id','user_'. $author_id);
?>
<div class="row author_block">	
	<div class="col-4 col-md-3 col-xl-2">
		<div class="author-photo">
			<?php get_avatar( $curauth->user_email , '260 '); ?>
		</div>
	</div>
	<div class="col-8 col-md-9 col-xl-10">
		<div class="author_details">
			<div class="author_function"><?php echo the_field('team_function',$team_id) ?></div>
			<div class="author_name"><?php echo $curauth->nickname; ?></div> 
			<div class="author_bio"><?php echo the_field('team_bio',$team_id); ?></div>
		</div>
		<ul class="socialmedia ml-auto <?php if(get_field('icon_or_text','option') == 'icon'): ?> icons-circle <?php endif; ?> d-flex justify-content-end">
			<?php if( have_rows('user_social_media_networks',$team_id) ): while( have_rows('user_social_media_networks',$team_id) ) : the_row(); ?>
				<li>
					<a href="<?php echo the_sub_field('social_media_url',$team_id); ?>" target="_blank">
						<i class="<?php echo the_sub_field('icon',$team_id); ?>"></i>
					</a>
				</li>
			<?php endwhile; endif; ?>
		</ul>
	</div>
</div>