<?php 
	$headerImage = get_field('ArchiveHeaderImage','option'); 
	$headerImageTitle = get_field('title_in_image_top','option');
	$mailingFormTitle = get_field('mailing_form_title','option');
	$relatedPostTitle = get_field('related_post_title','option');
	$numberOfRecentPosts = get_field('number_of_recent_posts','option');
	$numberOfPopulairPosts = get_field('number_of_populair_posts','option');
	$knowledgebaseThankyouPage = get_field('knowledgebase_thankyou_page','option');
	$relatedPosts = get_field('related_blogs');
?>

<section id="fullwidth-image" class="{{$headerImage['value']}} less-height single d-flex justify-content-start align-items-center">
	<span>{{$headerImageTitle}}</span>
	<div class="white-bar"></div>
</section>

@include('resources/components/breadcrumbs')

<section id="knowledgebase">
	<div class="container">
		<div class="row">
			@wpposts
			<?php setPostViews(get_the_ID()); ?> 
			<div class="col-12 col-lg-8 col-xl-8">
				<div class="d-flex">
					<div class="post_thumb">
						{{the_post_thumbnail()}}
					</div>
					<h1 class="post-title">{{the_title()}}</h1>
				</div>
				<div class="post-details d-flex">
					<div class="author-image ">
					{{get_avatar( get_the_author_meta('user_email'), $size = '30')}}
					</div>
					<div>
						<a href="{{get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )}}"><?php the_author(); ?></a> - {{get_the_date()}} 
					</div>
					<div class="ml-auto">{{get_the_category_list()}} </div>
				</div>
				{{the_content()}}
				
				
				<div class="post-box grey">
					<h3>informatie</h3>
					<table class="post-table">
						<tr>
							<td>Geplaatst op</td>
							<td class="text-right">{{get_the_date()}} </td>
						</tr>
						<tr>
							<td>Categorie</td>
							<td class="text-right">{{get_the_category_list()}}</td>
						</tr>
						<tr>
							<td>Tags</td>
							<td class="text-right">{{get_the_tag_list('<ul class="post_tags"><li>','</li><li>','</li></ul>')}}</td>
						</tr>
						<tr>
							<td>Delen</td>
							<td class="text-right">
								<ul class="socialmedia share-buttons d-flex justify-content-end">
									<li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{the_permalink()}}&title={{the_title()}}"><i class="fab fa-linkedin-in"></i>LinkedIn</a></li>
									<li><a href="http://www.facebook.com/sharer.php?u={{the_permalink()}}&p[title]={{the_title()}}"><i class="fab fa-facebook-f"></i>Facebook</a></li>
									<li><a href="http://twitter.com/share?text={{the_title()}}&url={{the_permalink()}}"><i class="fab fa-twitter"></i>Twitter</a></li>
								</ul>
							</td>
					</table>
				</div>
				@if($relatedPosts)
				<div class="post-box grey">
					<h3>{{$relatedPostTitle}}</h3>
					<div class="related-posts d-flex">

					@foreach ($relatedPosts as $post)
						<div class="post-related col-4">
								<div class="post-thumbnail">
									<a href="{{the_permalink()}}" title="{{the_title_attribute()}}">
										{{the_post_thumbnail()}}
									</a>
								</div>
								<div class="post-title">
									<a href="{{the_permalink()}}" title="{{the_title_attribute()}}">
										{{the_title()}}
									</a>
								</div>
							</div>
					@endforeach
					</div>
				</div>
				@endif
			@wpempty
				404
			@wpend
			</div>
			<div class="col-12 col-lg-4 col-xl-4 sidebar d-none d-lg-block">
				<div class="sidebar-box">
					<div class="box-title">
						Recente artikelen
					</div>
					<div class="box-content">
						<ul class="post-list">
						<?php
						global $post;
						$category = get_the_category($post->ID);
						$category = $category[0]->cat_ID;
						$myposts = get_posts(array('numberposts' => $numberOfRecentPosts, 'offset' => 0, 'category__in' => array($category), 'post_status'=>'publish' ));
						foreach($myposts as $post) :
						setup_postdata($post);
						?>
						<li>
							<div class="d-flex">
								<div class="post-thumbnail">
									
										<a href="{{the_permalink()}}">{{the_post_thumbnail()}}</a>
									
								</div>
								<div class="post-body">
									<a href="{{the_permalink()}}">{{the_title()}}</a> <br> <span class="post-date">{{get_the_date()}}</span>
								</div>
							</div>
						</li>
						<?php endforeach; ?>
						</ul>
						<?php wp_reset_query(); ?>
					</div>
				</div>
				<div class="sidebar-box">
					<div class="box-title">
						Populaire artikelen 
					</div>
					<div class="box-content">
						<ul class="post-list">
						<?php 
						 $args = array(
						    'posts_per_page' => $numberOfPopulairPosts,
							'meta_key' => 'post_views_count',
							'orderby' => 'meta_value_num',
							'order' => 'DESC'
						  );
						  ?>
						<?php query_posts($args); ?>
						@wpposts
							<li>
								<div class="d-flex">
									<div class="post-thumbnail">
										
											<a href="{{the_permalink()}}">{{the_post_thumbnail()}}</a>
										
									</div>
									<div class="post-body">
										<a href="{{the_permalink()}}">{{the_title()}}</a> <br> <span class="post-date">{{get_the_date()}}</span>
									</div>
								</div>
							</li>
						@wpempty
						404
						@wpend
						</ul>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>