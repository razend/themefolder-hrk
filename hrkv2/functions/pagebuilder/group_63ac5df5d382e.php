<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_63ac5df5d382e',
	'title' => 'Contactblok',
	'fields' => array(
		array(
			'key' => 'field_63ac5df5d9abd',
			'label' => '<span class="dashicons dashicons-layout"></span> Layout',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ac5df5d9ad6',
			'label' => 'Layout',
			'name' => 'layout_selection',
			'aria-label' => '',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => 'layout-selector',
				'id' => '',
			),
			'choices' => array(
				'layout1' => '<img src="/wp-content/themes/hrkv2/images/pagebuilder/contact_1.webp" />',
			),
			'default_value' => 'layout1',
			'return_format' => 'value',
			'allow_null' => 0,
			'other_choice' => 0,
			'layout' => 'horizontal',
			'acfe_field_group_condition' => 0,
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_63ac5df5d9adc',
			'label' => '<span class="dashicons dashicons-text"></span> Inhoud',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ac5df5d9ae2',
			'label' => 'Titel',
			'name' => 'title',
			'aria-label' => '',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_63ac5df5d9ad6',
						'operator' => '==',
						'value' => 'layout1',
					),
				),
				array(
					array(
						'field' => 'field_63ac5df5d9ad6',
						'operator' => '==',
						'value' => 'layout4',
					),
				),
			),
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'acfe_textarea_code' => 0,
			'maxlength' => '',
			'rows' => 2,
			'placeholder' => '',
			'new_lines' => 'br',
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ac5df5d9ae7',
			'label' => 'Content linkerkolom',
			'name' => 'content',
			'aria-label' => '',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'acfe_wysiwyg_min_height' => '',
			'acfe_wysiwyg_max_height' => '',
			'acfe_wysiwyg_valid_elements' => '',
			'acfe_wysiwyg_custom_style' => '',
			'acfe_wysiwyg_disable_wp_style' => 0,
			'acfe_wysiwyg_autoresize' => 1,
			'acfe_wysiwyg_disable_resize' => 0,
			'acfe_wysiwyg_remove_path' => 0,
			'acfe_wysiwyg_menubar' => 0,
			'acfe_wysiwyg_transparent' => 0,
			'acfe_wysiwyg_merge_toolbar' => 1,
			'acfe_wysiwyg_custom_toolbar' => 0,
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 1,
			'acfe_wysiwyg_auto_init' => 1,
			'acfe_field_group_condition' => 0,
			'acfe_wysiwyg_height' => 300,
			'acfe_wysiwyg_toolbar_buttons' => array(
			),
		),
		array(
			'key' => 'field_63c92b4c6b9a6',
			'label' => 'Content rechterkolom',
			'name' => 'content_right',
			'aria-label' => '',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'acfe_wysiwyg_min_height' => '',
			'acfe_wysiwyg_max_height' => '',
			'acfe_wysiwyg_valid_elements' => '',
			'acfe_wysiwyg_custom_style' => '',
			'acfe_wysiwyg_disable_wp_style' => 0,
			'acfe_wysiwyg_autoresize' => 1,
			'acfe_wysiwyg_disable_resize' => 0,
			'acfe_wysiwyg_remove_path' => 0,
			'acfe_wysiwyg_menubar' => 0,
			'acfe_wysiwyg_transparent' => 0,
			'acfe_wysiwyg_merge_toolbar' => 1,
			'acfe_wysiwyg_custom_toolbar' => 0,
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 1,
			'acfe_wysiwyg_auto_init' => 1,
			'acfe_field_group_condition' => 0,
			'acfe_wysiwyg_height' => 300,
			'acfe_wysiwyg_toolbar_buttons' => array(
			),
		),
		array(
			'key' => 'field_63ad65d142cab',
			'label' => 'Formulier ID',
			'name' => 'form_id',
			'aria-label' => '',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'min' => '',
			'max' => '',
			'placeholder' => '',
			'step' => '',
			'prepend' => '',
			'append' => '',
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ad655642caa',
			'label' => '<span class="dashicons dashicons-admin-users"></span> Recruiter',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ad68c303318',
			'label' => 'Selecteer een recruiter',
			'name' => 'recruiter_id',
			'aria-label' => '',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'team',
			),
			'taxonomy' => '',
			'return_format' => 'id',
			'multiple' => 0,
			'max' => '',
			'save_custom' => 0,
			'save_post_status' => 'publish',
			'acfe_add_post' => 0,
			'acfe_edit_post' => 0,
			'acfe_bidirectional' => array(
				'acfe_bidirectional_enabled' => '0',
			),
			'allow_null' => 0,
			'acfe_field_group_condition' => 0,
			'ui' => 1,
			'save_post_type' => '',
			'min' => '',
		),
		array(
			'key' => 'field_63ac5df5d9b23',
			'label' => '<span class="dashicons dashicons-admin-settings"></span> Instellingen',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ac5df5d9b29',
			'label' => 'Instellingen',
			'name' => 'section_settings',
			'aria-label' => '',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_632c245445a1e',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
			'acfe_field_group_condition' => 0,
			'acfe_seamless_style' => 0,
			'acfe_clone_modal' => 0,
			'acfe_clone_modal_close' => 0,
			'acfe_clone_modal_button' => '',
			'acfe_clone_modal_size' => 'large',
		),
		array(
			'key' => 'field_63ac5df5d9b2e',
			'label' => '<span class="dashicons dashicons-cover-image"></span> Achtergrond',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ac5df5d9b34',
			'label' => 'Sectie achtergrond',
			'name' => 'section_bg',
			'aria-label' => '',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_632ad2d5cf523',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
			'acfe_field_group_condition' => 0,
			'acfe_seamless_style' => 0,
			'acfe_clone_modal' => 0,
			'acfe_clone_modal_close' => 0,
			'acfe_clone_modal_button' => '',
			'acfe_clone_modal_size' => 'large',
		),
		array(
			'key' => 'field_63ac5df5d9b39',
			'label' => '<span class="dashicons dashicons-editor-code"></span> Ontwikkelaar',
			'name' => '',
			'aria-label' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'no_preference' => 0,
			'acfe_field_group_condition' => 0,
		),
		array(
			'key' => 'field_63ac5df5d9b3f',
			'label' => 'Ontwikkelaar',
			'name' => 'ontwikkelaar',
			'aria-label' => '',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_6329a16266701',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
			'acfe_field_group_condition' => 0,
			'acfe_seamless_style' => 0,
			'acfe_clone_modal' => 0,
			'acfe_clone_modal_close' => 0,
			'acfe_clone_modal_button' => '',
			'acfe_clone_modal_size' => 'large',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => false,
	'description' => '',
	'show_in_rest' => 0,
	'acfe_autosync' => array(
		0 => 'php',
	),
	'acfe_form' => 0,
	'acfe_display_title' => '',
	'acfe_meta' => '',
	'acfe_note' => '',
	'acfe_categories' => array(
		'pagebuilder' => 'Pagebuilder',
	),
	'modified' => 1674128574,
));

endif;