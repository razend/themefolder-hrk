<?php 
$title = get_sub_field('title');
$text = get_sub_field('content');

$img = get_sub_field('image');
$img_placement = get_sub_field('image_placement');

?>
<div class="container">
	<div class="row">
		<?php if ($title): ?>
		<div class="col-12 mb-5 text-center">
			<h1><?php echo $title; ?> </h1>
		</div>
		<?php endif; ?>

		<?php if ($img_placement == 'left'): ?>
		<div class="col-12 col-lg-6">
			<div class="image h-100" style="background: url(<?php echo $img; ?>) no-repeat center center; background-size: cover;">
			</div>
		</div>
		<?php endif; ?>

		<div class="col-12 content col-lg-6 h-100">
			<div class="content <?php echo $img_placement; ?>">
				<?php echo $text;
				get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
			</div>			
		</div>

		<?php if ($img_placement == 'right'): ?>
		<div class="col-12 col-lg-6">
			<div class="image h-100" style="background: url(<?php echo $img; ?>) no-repeat center center; background-size: cover;">
			</div>
		</div>
		<?php endif; ?>
		
	</div>
</div>
<?php wp_reset_query(); ?>