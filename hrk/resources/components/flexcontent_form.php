<?php 
// 30-8-21: blade engine opmaak verwijderd.
$section_id = get_sub_field('section_id');
$layout = get_sub_field('layout');
$bg_color = get_sub_field('bg_color');
$text = get_sub_field('content');
$text_above_form = get_sub_field('text_above_form');
$form_id = get_sub_field('form_id');
$formblock_text_color = get_sub_field('formblock_text_color');

?>
		<?php if($layout == 'text_top'): ?>
		<style>
			.form_block h1, .form_block h2, .form_block h3, .form_block h4, .form_block p, .form_block label {
				color: <?php echo $formblock_text_color; ?>;
			}
		</style>
		<?php endif; ?>
		<?php if($layout == 'text_top'): ?>
		<section id="content" class="form_block" style="background-color: <?php echo $bg_color; ?>">
		<?php elseif($layout == 'text_left'): ?>
		<section id="content" class="form_block">
		<?php endif; ?>
			<div id="<?php echo $section_id; ?>"></div>
			<div class="container">
				<div class="row  justify-content-center">
					<?php if($layout == 'text_top'): ?>
						<div class="col-12  col-lg-8">
					<?php elseif($layout == 'text_left'): ?>
						<div class="col-12 col-lg-8">
					<?php endif; ?>
							<p><?php echo $text; ?></p>
						</div>

					<?php if($layout == 'text_top'): ?>
						<div class="col-12 col-lg-8">
					<?php elseif($layout == 'text_left'): ?>
						<div class="col-12 col-lg-4 sidebar">
					<?php endif; ?>
						<div class="contact_block">
							<?php if($layout == 'text_left'): ?>
							<div class="content">
								<?php echo $text_above_form; ?>
							</div>
							<?php endif; ?>
							<?php gravity_form( $form_id, false, false, false, '', false ); ?>
						</div>
					</div>
				</div>
			</div>
		</section> 