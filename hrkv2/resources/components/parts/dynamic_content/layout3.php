<?php 
	// Layout: Ref. project
	$item_selection = get_sub_field('featured_item'); 

 ?>
<?php foreach( $item_selection as $post ): 
	setup_postdata($post); ?>
<div class="container-fluid">
	<div class="row h-100">
		<div class="col-12 col-lg-5 image" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>);">

		</div>
		<div class="col-12 col-lg-7 content">
			
				<?php the_content(); ?>
				<div class="d-flex">
					<a href="<?php echo get_permalink(); ?>" class="btn">Lees verder</a>
					<a href="<?php echo get_permalink(); ?>" class="btn">Bekijk ander cases</a>
				</div>
		
		</div>
	</div>
</div>
<?php endforeach; ?>

<?php wp_reset_query(); ?>