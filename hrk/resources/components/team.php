<?php $teamOrder = get_field('teamOrder','option'); ?>
<section id="team">
	<div class="container">
		<div class="row">
			<?php foreach ($teamOrder as $post): ?>
			<div class="col-6 col-md-4 col-lg-3 teammember ">
				<div class="thumbnail teamimage" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);">

				</div>
				<p class="team_name pb-0"><?php echo the_title(); ?></p>
				<p class="team_function pb-0"><?php echo the_field('team_function'); ?></p>
				<p class="team_quality tone"><?php echo the_field('team_quality'); ?></p>
				<ul class="socialmedia ml-auto icons-circle d-flex justify-content-end">
					<?php if(get_field('team_phonenumber')): ?>
					<li><a href="tel:<?php echo the_field('team_phonenumber'); ?>" target="_blank"><i class="fas fa-phone"></i></i></a></li>
					<?php endif; ?>
					<?php if(get_field('team_emailaddress')):?>
					<li><a href="mailto:<?php echo the_field('team_emailaddress'); ?>" target="_blank"><i class="far fa-envelope"></i></a></li>
					<?php endif; ?>
					<?php if(get_field('linkedin_profiel_url')): ?>
					<li><a href="<?php echo the_field('linkedin_profiel_url'); ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
					<?php endif; ?>
				</ul>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>