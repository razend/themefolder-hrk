<?php get_header();
$headerImage = get_field('ArchiveHeaderImage','option'); 
$headerImageTitle = get_field('title_in_image_top','option');
?>

<section id="fullwidth-image" class="<?php echo $headerImage['value']; ?> less-height d-flex justify-content-start align-items-center">
	<h1><?php echo $headerImageTitle; ?></h1>
	<div class="white-bar"></div>
</section>

<?php get_template_part('resources/components/breadcrumbs'); ?>

<section id="knowledgebase">
	<div class="container">
		<?php get_template_part('resources/components/author_information'); ?>
		<div class="row">

			<?php get_template_part('resources/components/knowledgebase'); ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>