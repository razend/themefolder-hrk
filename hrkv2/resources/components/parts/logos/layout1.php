<?php 
	$btn_text = get_sub_field('btn_text');
	$btn_style = get_sub_field('btn_style');
	$title = get_sub_field('title');
	$layout = get_sub_field('layout_selection');
	$number_of_visible_logos = get_sub_field('visible_rows');
?>

<div class="container">
	<div class="company_logos <?php echo $layout; ?>">
		<div class="row align-items-center justify-content-center">

			<div class="col-12 text-center mb-5">
				<h1> <?php echo $title; ?> </h1>
			</div>
			<?php 	
				if ($number_of_visible_logos = "all"):
					if( have_rows('company_logos') ): while( have_rows('company_logos') ) : the_row();
						$img = get_sub_field('company_logo');?>
						<div class="col-6 col-md-3">
							<div class="logo-container">
								<img src="<?php echo $img; ?>" alt="" class="company_logo">
							</div>
						</div>
					<?php endwhile; endif; 
				else :

					$count = count(get_sub_field('company_logos'));
					$logos = 0;
			 		if( have_rows('company_logos') ): while( have_rows('company_logos') ) : the_row(); 
						$img = get_sub_field('company_logo');?>
						<div class="col-6 col-md-3 ">
							<img src="<?php echo $img; ?>" alt="" class="company_logo">
						</div>
						<?php $logos++; ?>
						<?php if($logos == $number_of_visible_logos): ?>
							<div class="collapse container" id="collapseLogos">
								<div class="row">
							<?php endif; ?>
						<?php endwhile; 
					endif; ?>
					</div>
					</div>
					<?php if($count > $number_of_visible_logos): ?>
						<div class="row">
							<div class="col-12 d-flex justify-content-center">
								<a class="mt-5 mb-5 btn <?php echo $btn_style; ?>" data-bs-toggle="collapse" href="#collapseLogos" role="button" aria-expanded="false" aria-controls="collapseLogos"><?php echo $btn_text; ?></a>
							</div>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			
		</div>
		
	</div>
</div>

<?php wp_reset_query(); ?>