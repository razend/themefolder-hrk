<?php 
	//Layout: multi team members
	$content = get_sub_field('content');	

	$items_selection = get_sub_field('selected_team_members');  
	$posttype = get_post_type(get_the_ID());
 ?>

<div class="container">
	<div class="row gx-lg-5">
		<div class="col-12"> 
			<?php echo $content; ?>
		</div>
		<?php foreach( $items_selection as $post ): 
		setup_postdata($post); 
		

		$team_name = get_the_title();
		$team_function = get_field('team_function');
		$team_bio = get_field('team_bio'); 
		$thumb_url = get_the_post_thumbnail_url(); 
		$linkedin = get_field('linkedin_profiel_url'); 
		$thumb_title = get_the_post_thumbnail_url(['alt' => get_the_title()]); ?>

		<div class="col-6 col-sm-4 col-xl-3 teammember">
			<div class="card h-100">
				<?php if(has_post_thumbnail()): ?>
					<div class="image">
						<img src="<?php echo $thumb_url; ?>" alt="<?php echo $thumb_title; ?>" />
					</div>
				<?php endif; ?>
				<div class="card-body">
					<p class="card-text">
						<span class="name"><?php echo $team_name; ?></span> <br> <span class="function"><?php echo $team_function; ?></span>
					</p>
					<?php if ($linkedin): ?>
						<div class="socials">
							<a href="<?php echo $linkedin; ?>"><i class="fa-brands fa-linkedin-in"></i></a>
						</div>
					<?php endif ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>

<?php wp_reset_query(); ?>