<?php 
	// volledig brede foto
	$text = get_sub_field('content');
	$subtext = get_sub_field('subtext');

	// Layout settings
	$layout = get_sub_field('layout_selection');

	// Background settings
	$background_settings = get_sub_field('section_background_settings');
	$section_bg = $background_settings['section_bg_img'];

	// Blok settings
	$align_x = get_sub_field ('content_alignment_x'); // horizontal
	$align_y = get_sub_field ('content_alignment_y'); // vertical

	// Dev instellingen
	$developer_options = get_sub_field('developer_options');
	$section_class = $developer_options['section_class'];

 ?>

<section class="hero_block <?php echo $layout . " " . $section_class; ?> d-flex align-items-<?php echo $align_y; ?>" style="background-image: url('<?php echo $section_bg; ?>'); background-size: cover; background-position: center center;">
	<div class="container">
		<div class="row justify-content-<?php echo $align_x; ?>">
			<div class="col-12 col-lg-6 p-5 d-flex flex-column justify-content-<?php echo $align_x; ?> ">
				<?php if ($text): ?>
					<div class="title">
						<?php echo $text; ?>
					</div>

					<div class="subcontent">
						<?php echo $subtext; ?>
					</div>
				<?php endif; ?>
				<?php get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>

			</div>
		</div>
	</div>
</section>

<?php wp_reset_query(); ?>