<?php get_header();

	if(is_singular('services')):
		// get_template_part('resources/components/services');
		get_template_part('resources/views/layouts/pagebuilderv2');
	elseif(is_singular('work')):
		get_template_part('resources/components/work');
	elseif(is_singular( 'team' )):
		get_template_part('resources/components/teammember');
	else:
		get_template_part('resources/views/layouts/single');
	endif; 
	
get_footer(); ?>