<section id="carousel" class="no-padding">
	<div id="carouselControls" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<?php $i = 0; ?>
			@acfrepeater('slider_images')
				<li data-target="#carouselControls" data-slide-to="{{$i}}" class="@if($i == 0) active @endif"></li>
			<?php $i++;  ?>
			@acfend
		</ol>
		<div class="carousel-inner">
			<?php $i = 0; ?>				
			@acfrepeater('slider_images')
			<?php $image = get_sub_field('slider_image'); ?>
				<div class="carousel-item @if($i == 0) active @endif item">
					<div class="container">
						<div class="row">
							<div class="col-12 col-lg-10 col-xl-8 d-flex">
								<div class="slide-content">
									<h1>{{the_sub_field('title_txt')}}</h1>
									<p>{{the_sub_field('content_txt')}}</p>
									<a href="{{the_sub_field('btn_url')}}" class="btn btn-inverse base">{{the_sub_field('btn_txt')}}</a>
								</div>
							</div>
						</div>
					</div>
					<img class="d-block w-100" src="{{$image['url']}}" alt="">
				</div>
				<?php $i++;  ?>
			@acfend
		</div>
		<a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<script src="{{get_template_directory_uri()}}/js/assets/carousel.js"></script>
</section>
