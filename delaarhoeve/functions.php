<?php 

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
 
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

// Load menu's
function loadMenuDL() {
  register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'Delaarhoeve' ),
	'footer_sitmap' => __( 'Footer Sitemap', 'Delaarhoeve' ),
	'footer_general' => __( 'Footer Algemeen', 'Delaarhoeve' ),
	'about' => __( 'Over De Laarhoeve', 'Delaarhoeve' ),
	'nieuws' => __( 'Nieuws', 'Delaarhoeve' ),
	'socials' => __( 'Socials', 'Delaarhoeve' ),
) );
}
add_action( 'init', 'loadMenuDL' );


?>