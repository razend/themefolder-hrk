<?php 
	$headerimage = get_field('headerimage');
	$customheaderimage = get_field('custom_image_header');
	$show_colored_space = get_field('show_colored_space');
	$headerImageTitle = get_field('title_in_image_top');
	$midimage = get_field('midimage');
	$footerimage = get_field('footerimage');
	$footerImageTitle = get_field('title_in_image_bottom');
	$customfooterimage = get_field('custom_image_footer');
	$show_colored_space_footer = get_field('show_colored_space_footer');
	$jargonbuster_parent_page = get_field('jargonbuster-parent-page','option');
	$jbHeaderImage = get_field('jbHeaderImage','option'); 
	$big_title_bottom = get_field('big_title_bottom');
	$orangebar = get_field('orangebar');
	$orangebar_footer = get_field('orangebar_footer');


?>

<?php if($post->post_parent == $jargonbuster_parent_page): ?>
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom less-height <?php echo $jbHeaderImage; ?>">
<?php elseif($headerimage === 'custom_image'):?>
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space; ?>" style="background-image: url(<?php echo $customheaderimage; ?>);">
<?php else: ?>
<section id="fullwidth-image" class="<?php echo $headerimage; ?> d-flex justify-content-start align-items-center">
<?php endif; ?>
	<span><?php echo $headerImageTitle; ?></span>
	<div class="white-bar"></div>
</section>	
<?php if(!$post->post_parent === $jargonbuster_parent_page) {
	if($orangebar === 'yes' ){ ?>
	<section id="fullwidth-image" class="orangebar">
		<div class="white-bar"></div>
	</section>
	<?php }
} ?>

<?php if($post->post_parent == $jargonbuster_parent_page):
	get_template_part('resources/components/jargonbuster');
endif; ?>

<section id="content">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<p><?php the_content(); ?></p>
				<?php endwhile; else : ?>
					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div>
			<?php if($big_title_bottom):?>
			<div class="col-12">
				<h1 class="text-center"><?php echo $big_title_bottom; ?></h1>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php /*if(!$post->post_parent == $jargonbuster_parent_page):
	if( get_field('midimage')): ?>
	<section id="fullwidth-image" class="<?php echo $midimage['value']; ?>">
		<div class="white-bar"></div>
	</section>
	<?php endif; ?>
	<?php if( get_field('orangebar_mid') == 'yes' ): ?>
	<section id="fullwidth-image" class="orangebar">
		<div class="white-bar"></div>
	</section>
<?php endif; endif; */

if(is_page('vacatures')):
	get_template_part('resources/components/vacancies');
endif; 

if(is_page('hrk')):
	get_template_part('resources/components/team');
endif;

if(!$post->post_parent == $jargonbuster_parent_page):
	if($footerimage === 'custom_image'): ?>
	<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space_footer; ?>" style="background-image: url(<?php echo $customfooterimage; ?>);">
	<?php else: ?>
	<section id="fullwidth-image" class="<?php echo $footerimage; ?> d-flex justify-content-start align-items-center">
	<?php endif; ?>
		<span><?php echo $footerImageTitle; ?></span>
		<div class="white-bar"></div>
	</section>	
	<?php if($orangebar_footer === 'yes' ): ?>
	<section id="fullwidth-image" class="orangebar">
		<div class="white-bar"></div>
	</section>
<?php endif; endif; ?>