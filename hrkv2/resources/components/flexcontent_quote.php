<?php 
	// 30-8-21: blade engine opmaak verwijderd.
$section_id = get_sub_field('section_id');
$quote = get_sub_field('quote');
$name = get_sub_field('name');
$function = get_sub_field('function');
$image = get_sub_field('image');
$media_location = get_sub_field('media_location');
$bg_color = get_sub_field('bg_color');
?>
<?php if ($section_id): ?>
	<div id="<?php echo $section_id; ?>"></div>
<?php endif; ?>
<div id="<?php echo $section_id; ?>"></div>
<section id="content" class="quote_block" style="background-color: <?php echo $bg_color; ?>">
	<div class="container">
		<div class="row">
			<?php if($media_location == 'right'): ?>
				<div class="col-12 col-md-9">
					<h2>"<?php echo $quote; ?>"</h2>
					<p><?php echo $name; ?><br>
					<?php echo $function; ?></p>
				</div>
				<div class="col-12 col-md-3 image">
					<img src="<?php echo $image; ?>"  alt="">
				</div>
			<?php elseif($media_location == 'left'): ?>
				<div class="col-12 col-md-3 image">
					<img src="<?php echo $image; ?>" alt="">
				</div>
				<div class="col-12 col-md-9">
					<h2>"<?php echo $quote; ?>"</h2>
					<p><?php echo $name; ?><br>
					<?php echo $function; ?></p>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>