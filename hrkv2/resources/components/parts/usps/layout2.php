<?php 
	$title = get_sub_field('title');
 ?>

<div class="container">
	<div class="row justify-content-around">
		<?php if ($title): ?>
			<div class="col-12 text-center mb-5">
				<h1><?php echo $title; ?></h1>
			</div>	
		<?php endif;
		
			if( have_rows('usps') ): while( have_rows('usps') ): the_row();
				$content = get_sub_field('content');
				$usp_image = get_sub_field('icon');?>
				<div class="col-12 col-lg-6 text-center text-sm-start usp d-flex flex-column align-items-center">
					<img src="<?php echo $usp_image; ?>" class="icon" alt="">
					<?php echo $content; ?>
				</div>						
			<?php endwhile; endif; ?>
		
	</div>
</div>
<?php wp_reset_query(); ?>