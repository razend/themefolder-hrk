<?php
	$social_media_url = get_sub_field('social_media_url','option');
	$icon_or_text = get_field('icon_or_text','option');
?>

<ul class="socialmedia <?php if($icon_or_text == 'icon'): ?> icons-circle <?php endif; ?> d-flex">
	<?php if( have_rows('social_media_networks','option') ): while( have_rows('social_media_networks','option') ) : the_row();
		$icon_array = get_sub_field('icon');
	 	$icon = $icon_array['value'];
	 	$text = $icon_array['label']; 
	?>
		<li>
			<a href="<?php echo $social_media_url; ?>" target="_blank">
				<?php if($icon_or_text === 'icon'): ?>
					<i class="fab <?php echo $icon; ?>"></i>
				<?php elseif($icon_or_text == 'text'):
					echo $text;
				endif; ?>
			</a>
		</li>
	<?php endwhile; endif; ?>
</ul>