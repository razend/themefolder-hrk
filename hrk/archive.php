<?php get_header();
$headerImage = get_field('ArchiveHeaderImage','option'); 
$headerImageTitle = get_field('title_in_image_top','option');
?>

<section id="fullwidth-image" class="<?php echo $headerImage['value']; ?> less-height d-flex justify-content-start align-items-center">
	<h1><?php echo $headerImageTitle; ?></h1>
	<div class="white-bar"></div>
</section>

<?php get_template_part('resources/components/breadcrumbs'); ?>

<section id="knowledgebase" class="<?php echo get_post_type( get_the_ID()); ?>">
	<div class="container">
		<div class="row">

			<?php
				if ( get_post_type( get_the_ID()) == 'post' ):
					
					get_template_part('resources/components/category-selector');
					get_template_part('resources/components/knowledgebase');
				endif;
				

				if ( get_post_type( get_the_ID() ) == 'vacancies' ):
					get_template_part('resources/components/vacancies');				
			endif;

			?>
						
		</div>
	</div>
</section>

<?php get_footer(); ?>