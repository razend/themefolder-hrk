<?php
if( have_rows('marketingblock') ):

    // Loop through rows.
    while ( have_rows('marketingblock') ) : the_row();

        // Case: Paragraph layout.
        if( get_row_layout() == 'textual_section' ):
        	get_template_part('resources/components/flexcontent_paragraph');
        // Case: Paragraph with image or video layout.
	    elseif( get_row_layout() == 'textual_video_img_section' ):
	    	get_template_part('resources/components/flexcontent_paragraph_img_vid');
		 // Case: Hero section.
        elseif( get_row_layout() == 'hero_section' ): 
			get_template_part('resources/components/flexcontent_hero');
		// case: Services section
		elseif (get_row_layout() == 'services_section'):
			get_template_part('resources/components/flexcontent_services');
		// Case: Case blok
	    elseif( get_row_layout() == 'case_section' ): 
	    	get_template_part('resources/components/flexcontent_case');
		// case: Quote blok
		elseif (get_row_layout() == 'quote_section'):
			get_template_part( '/resources/components/flexcontent_quote');
		// case: Quote slider
		elseif (get_row_layout() == 'quote_slider'):
			get_template_part( '/resources/components/flexcontent_quoteslider');
		// case: Form blok
		elseif (get_row_layout() == 'form_section'):
			get_template_part('resources/components/flexcontent_form');
		// case: Speaker section
		elseif( get_row_layout() == 'speaker_section' ):
			get_template_part('resources/components/flexcontent_speaker');
		// case: testimonial section
		elseif( get_row_layout() == 'testimonial_section' ):
			get_template_part('resources/components/flexcontent_testimonial');
		// case: Photo row section
		elseif( get_row_layout() == 'photorow_section' ):
			get_template_part('resources/components/flexcontent_photorow');
		// case: Meeting section
		elseif( get_row_layout() == 'meetingplanner_section' ):
			get_template_part('resources/components/flexcontent_meetingplanner');
		// case: USP's section
		elseif( get_row_layout() == 'usps_section' ):
			get_template_part('resources/components/flexcontent_usps');
		// case: Vacancie section
		elseif( get_row_layout() == 'vacancie_section' ):
			get_template_part('resources/components/flexcontent_vacancies');
		// case: Blogs
		elseif( get_row_layout() == 'blogs' ):
			get_template_part('resources/components/flexcontent_blogs');
		// case: Tijdlijn
		elseif( get_row_layout() == 'timeline_section' ):
			get_template_part('resources/components/flexcontent_timeline');
    	endif;

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;
	
?>
