<?php 
// 30-8-21: blade engine opmaak verwijderd.
$section_id = get_sub_field('section_id');
$section_class = get_sub_field('section_class');
$text = get_sub_field('content');
$text_color = get_sub_field('text_color');
$bg_color = get_sub_field('bg_color');
$section_less_margin = get_sub_field('section_less_margin');
$sticky = get_sub_field('sticky-top');
$cta_button_align = get_sub_field('cta_button_alignment');
?>

<?php if ($section_id): ?>
<div id="<?php echo $section_id; ?>"></div>	
<?php endif ?>


<?php if($section_less_margin && in_array('less_margin', $section_less_margin)): ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>; padding: 25px 0;" class="<?php if( $sticky && in_array('sticky-top', $sticky) ): echo "sticky-top"; endif; ?> <?php echo $section_class; ?>">
<?php else : ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>;" class="<?php if( $sticky && in_array('sticky-top', $sticky) ): echo "sticky-top"; endif; ?> <?php echo $section_class; ?>">
<?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-12" style="color: <?php echo $text_color; ?>!important;">
				<?php echo $text; ?>
				<?php if( have_rows('cta_adding')):?>
					<div class="text-<?php echo $cta_button_align; ?>">
						<?php while ( have_rows('cta_adding') ) : the_row();
							$cta_button_text = get_sub_field('cta_button_text');
							$cta_button_link = get_sub_field('cta_button_link');
							$cta_button_target = get_sub_field('cta_button_target');
						?>
						<a class="btn base" href="<?php echo $cta_button_link; ?>" target="<?php echo $cta_button_target; ?>"><?php echo $cta_button_text; ?></a>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>