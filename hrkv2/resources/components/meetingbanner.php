<?php 
	$meetingbanner_team_image = get_field('meetingbanner-team-image');
	$meetingbanner_team_name = get_field('meetingbanner-team-name');
	$meetingbanner_content = get_field('meetingbanner-content');
	$meetingbanner_team_image2 = get_field('meetingbanner-team-image2');
	$meetingbanner_team_name2 = get_field('meetingbanner-team-name2');
	$meetingbanner_data_organization = get_field('meetingbanner-data-organization');
	$meetingbanner_button_text = get_field('meetingbanner-button-text');
 ?>

<section id="blue" class="meetingbanner">
	<div class="container">
		<div class="row ">
			<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo $meetingbanner_team_image; ?>" alt="">
				</div>
				<div class="teammember-name"><?php echo $meetingbanner_team_name; ?></div>
			</div>
			<div class="col-12 col-md-8 d-flex align-items-center ">
				<h1><?php echo $meetingbanner_content; ?></h1>
			</div>
			<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo $meetingbanner_team_image2 ?>" alt="">
				</div>
				<div class="teammember-name"><?php echo $meetingbanner_team_name2 ?></div>
			</div>
			<div class="col-12 d-flex align-items-center justify-content-center">
				<a class="btn mettingbanner-button" data-appointlet-organization="<?php echo $meetingbanner_data_organization; ?>"><?php echo $meetingbanner_button_text; ?><script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
			</div>
		</div>
	</div>
</section>