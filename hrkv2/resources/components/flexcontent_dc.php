<?php

$layout = get_sub_field('layout_selection');
$content = get_sub_field('content');

get_template_part( '/resources/components/parts/general/section_id');

get_template_part( '/resources/components/parts/general/section_opening'); 


	if ($layout == 'layout1'): // Services v1

		get_template_part( '/resources/components/parts/general/start_content');

		get_template_part( '/resources/components/parts/dynamic_content/layout1');

	elseif ($layout == 'layout2'): // Services v2

		get_template_part( '/resources/components/parts/general/start_content');

		get_template_part( '/resources/components/parts/dynamic_content/layout2');

	elseif ($layout == 'layout3'): // Ref.project

		get_template_part( '/resources/components/parts/dynamic_content/layout3');

	elseif ($layout == 'layout4'): // Vacancies

		get_template_part( '/resources/components/parts/dynamic_content/layout4');

	elseif ($layout == 'layout5'): // Blogs

		get_template_part( '/resources/components/parts/dynamic_content/layout5');

	elseif ($layout == 'layout6'): // Blogs

		get_template_part( '/resources/components/parts/dynamic_content/layout6');

	endif; 

get_template_part( '/resources/components/parts/general/section_close');

wp_reset_query(); ?>