<?php
$jargonbuster_parent_page = get_field('jargonbuster-parent-page','option');

if( have_rows('pagebuilder') ):

    // Loop through rows.
    while ( have_rows('pagebuilder') ) : the_row();
       
		// case: Carousel
		if( get_row_layout() == 'carousel_section' ):
			get_template_part('resources/components/flexcontent_carousel');

		// case: Dynamic content
		elseif( get_row_layout() == 'cpt_section' ):
			get_template_part('resources/components/flexcontent_dc');
		 
		 // Case: Hero section.
        elseif( get_row_layout() == 'hero_section' ): 
			get_template_part('resources/components/flexcontent_hero_v2');

		 // Case: Logo section.
        elseif( get_row_layout() == 'logo_section' ): 
			get_template_part('resources/components/flexcontent_logos');

		// case: Quote slider
		elseif (get_row_layout() == 'quote_slider'):
			get_template_part( '/resources/components/flexcontent_quoteslider_v2');

		// case: USPs
		elseif (get_row_layout() == 'usp_section'):
			get_template_part( '/resources/components/flexcontent_usps');

		// Case: Paragraph layout.
        elseif( get_row_layout() == 'textual_section' ):
        	get_template_part('resources/components/flexcontent_paragraph_v2');
		
		// Case: Paragraph with image or video layout.
	    elseif( get_row_layout() == 'textual_video_img_section' ):
	    	get_template_part('resources/components/flexcontent_paragraph_media_v2');

		// Case: Contact section.
	    elseif( get_row_layout() == 'contact_section' ):
	    	get_template_part('resources/components/flexcontent_contact');

		// Case: Team section.
	    elseif( get_row_layout() == 'team_section' ):
	    	get_template_part('resources/components/flexcontent_team');

    	endif;

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;

if($post->post_parent == $jargonbuster_parent_page):
	get_template_part('resources/components/jargonbuster');
endif;	
?>
