<?php if( have_rows('cta_adding')): 
	$cta_button_align = get_sub_field('cta_button_alignment'); ?>
	<div class="text-<?php echo $cta_button_align; ?>">
		<?php while ( have_rows('cta_adding') ) : the_row();
			$cta_button_text = get_sub_field('cta_button_text');
			$cta_button_link = get_sub_field('cta_button_link');
			$cta_button_target = get_sub_field('cta_button_target');
		?>
		<a class="btn" href="<?php echo $cta_button_link; ?>" target="<?php echo $cta_button_target; ?>"><?php echo $cta_button_text; ?></a>
		<?php endwhile; ?>
	</div>
<?php endif; ?>