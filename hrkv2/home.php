<?php 
	get_header();
	$kb_hero_image = get_field('kb_hero_image','option');
	$kb_title = get_field('title_in_image_top','option');
	$kb_intro = get_field('knowledgebase_intro','option');
?>

<section class="hero_block layout1">
	<div class="container-fluid px-0 ">
		<div class="row g-0">
			<div class="col-12 col-lg-6 content_block">
				<div class="content">
					<div class="title">
						<?php echo $kb_title; ?>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-6 image_block">
				<div class="d-flex h-100">
					<img src="<?php echo $kb_hero_image; ?>" class="w-100" alt="" style="object-position: center;">
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('resources/components/breadcrumbs'); ?>

<section id="knowledgebase" class="pt-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php echo $kb_intro; ?>
			</div>

			<?php get_template_part('resources/components/category-selector');
			
			get_template_part('resources/components/knowledgebase');
				
			get_template_part('resources/components/pagination'); ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>