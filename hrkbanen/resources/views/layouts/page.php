<?php 
	$headerimage = get_field('headerimage');
	$customheaderimage = get_field('custom_image_header');
	$show_colored_space = get_field('show_colored_space');
	$headerImageTitle = get_field('title_in_image_top');
	$midimage = get_field('midimage');
	$footerimage = get_field('footerimage');
	$footerImageTitle = get_field('title_in_image_bottom');
	$customfooterimage = get_field('custom_image_footer');
	$show_colored_space_footer = get_field('show_colored_space_footer');

	$job_application_page = get_field('job-apply-page','option');
	$annual_program_page = get_field('annual-program-page','option');

?>
@if(get_field('headerimage') == 'custom_image')
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom {{$show_colored_space}}" style="background-image: url({{$customheaderimage}});">
@else
<section id="fullwidth-image" class="{{$headerimage}} d-flex justify-content-start align-items-center">
@endif
	<span>{{$headerImageTitle}}</span>
	<div class="white-bar"></div>
</section>	
@if( get_field('orangebar') == 'yes' )
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
@endif

<section id="content">
	<div class="container">
		<div class="row">

				<div class="col-12">
					@wpposts
					<p>{{the_content()}}</p>
					@wpempty
						<p>404</p>
					@wpend
				</div>
			
			@if(get_field('big_title_bottom'))
			<div class="col-12">
				<h1 class="text-center">{{the_field('big_title_bottom')}}</h1>
			</div>
			@endif
		</div>
	</div>
</section>

@if(is_page($job_application_page))
	@include('resources/components/job-apply-steps')
@endif

@if(is_page($annual_program_page))
	@include('resources/components/timeline')
@endif

@if( get_field('midimage'))
<section id="fullwidth-image" class="{{$midimage['value']}}">
	<div class="white-bar"></div>
</section>
@endif
@if( get_field('orangebar_mid') == 'yes' )
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
@endif



@if(get_field('footerimage') == 'custom_image')
<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom {{$show_colored_space_footer}}" style="background-image: url({{$customfooterimage}});">
@else
<section id="fullwidth-image" class="{{$footerimage}} d-flex justify-content-start align-items-center">
@endif
	<span>{{$footerImageTitle}}</span>
	<div class="white-bar"></div>
</section>	
@if( get_field('orangebar_footer') == 'yes' )
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
@endif