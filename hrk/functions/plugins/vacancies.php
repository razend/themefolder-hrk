<?php 

// ACF
include( get_template_directory() . '/functions/pagebuilder/group_5c9cbd705d382.php' ); // Vacatures

// Custom taxonomy
function vacancie_taxonomy() {
    register_taxonomy(
        'vacancie_categories',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'vacancies',             // post type name
        array(
            'hierarchical' => true,
            'label' => 'Functie categorieën', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'functiecategorieen',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'vacancie_taxonomy');

function filter_post_type_link( $link, $post ) {
    if ( $post->post_type !== 'vacancies' )
        return $link;

    if ( $cats = get_the_terms($post->ID, 'vacancie_categories') )
        $link = str_replace('%vacancie_categories%', array_pop($cats)->slug, $link);

    return $link;
}
add_filter('post_type_link', 'filter_post_type_link', 10, 2);
	
// Custom post types
add_action( 'init', 'create_post_type_banen' );
function create_post_type_banen() {
	
	register_post_type( 'vacancies',
		array(
			'labels' 				=> array(
				'name' 				=> __( 'Vacatures' ),
				'singular_name' 	=> __( 'Vacature' ),
				'add_new'		 	=> __( 'Nieuwe vacature'),
				'view_items'		=> __( 'Alle vacatures'),
				'view_item' 		=> __( 'Bekijk vacature' ),
				'all_items'			=> __( 'Alle vacatures'),
				'add_new_item'		=> __( 'Nieuwe vacature toevoegen'),
			),
			'public'				=> true,
			'has_archive' 			=> true,
			'supports' 				=> array('title','editor','publicize','thumbnail','post-formats'),
			'rewrite' 				=> array( 'slug' => 'vacatures/%vacancie_categories%', 'with_front' => FALSE ),
			'taxonomies' 			=> array('vacancie_categories' ),
			'publicly_queryable'	=> true,
			'menu_icon'				=> 'dashicons-clipboard',
			'capability_type' 		=> 'post'
		)
	);
}
?>