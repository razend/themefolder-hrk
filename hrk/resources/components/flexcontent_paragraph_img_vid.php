<?php 
// 30-8-21: blade engine opmaak verwijderd.
$section_id = get_sub_field('section_id');
$text = get_sub_field('content');
$text_color = get_sub_field('text_color');
$bg_color = get_sub_field('bg_color');
$media_location = get_sub_field('media_location');
$img_vid = get_sub_field('img_vid_choise');
$img = get_sub_field('image');
$vid = get_sub_field('video');
$cta_button_align = get_sub_field('cta_button_alignment');
$section_less_margin = get_sub_field('section_less_margin');
?>

<?php if ($section_id): ?>
	<div id="<?php echo $section_id; ?>"></div>	
<?php endif ?>

<?php if($section_less_margin && in_array('less_margin', $section_less_margin)): ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>; padding: 25px 0;">
<?php else : ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>;">
<?php endif; ?>

	<div class="container">
		<div class="row">
			<?php if($media_location == 'right'): ?>
				<div class="col-12 col-md-8" <?php if($text_color): ?> style="color: <?php echo $text_color; ?>!important;" <?php endif; ?>>
					<?php echo $text; ?>
					<?php if( have_rows('cta_adding') ): ?>
						<div class="text-<?php echo $cta_button_align; ?>">
							<?php while ( have_rows('cta_adding') ) : the_row();
								$cta_button_text = get_sub_field('cta_button_text');
								$cta_button_link = get_sub_field('cta_button_link');
								$cta_button_target = get_sub_field('cta_button_target');
							?>
							<a class="btn" href="<?php echo $cta_button_link; ?>" target="<?php echo $cta_button_target; ?>"><?php echo $cta_button_text; ?></a>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-12 col-md-4">
					<?php if($img_vid == 'img'): ?>
						<img src="<?php echo $img; ?>" alt="" style="width: 100%;">
					<?php elseif($img_vid == 'video'):
						echo $vid;
					endif; ?>
				</div>
			<?php elseif($media_location == 'left'): ?>
				<div class="col-12 col-md-4">
					<?php if($img_vid == 'img'): ?>
						<img src="<?php echo $img; ?>" alt="" style="width: 100%;">
					<?php elseif($img_vid == 'video'):
						echo $vid;
					endif; ?>
				</div>
				<div class="col-12 col-md-8" <?php if($text_color): ?> style="color: <?php echo $text_color; ?>!important;"<?php endif; ?>>
					<?php echo $text; ?>
					<?php if( have_rows('cta_adding') ): ?>
						<div class="text-<?php echo $cta_button_align; ?>">
							<?php while ( have_rows('cta_adding') ) : the_row();
								$cta_button_text = get_sub_field('cta_button_text');
								$cta_button_link = get_sub_field('cta_button_link');
								$cta_button_target = get_sub_field('cta_button_target');
							?>
							<a class="btn" href="<?php echo $cta_button_link; ?>" target="<?php echo $cta_button_target; ?>"><?php echo $cta_button_text; ?></a>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>