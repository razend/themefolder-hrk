<?php // 30-8-21: blade engine opmaak verwijderd.
	$section_id = get_sub_field('section_id');

if ($section_id): ?>
	<div id="<?php echo $section_id; ?>"></div>	
<?php endif ?>
<section id="content" class="photorow">
	<div class="d-flex photos">
		<?php if( get_row_layout() == 'photorow_section' ):
			if( have_rows('img_selector') )
				while( have_rows('img_selector') ): the_row(); 
					$img_select = get_sub_field('img_select');?>
					<div class="flex-fill photo">
						<img src="<?php echo $img_select; ?>"  alt="">
					</div>

				<?php endwhile; ?>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</section>