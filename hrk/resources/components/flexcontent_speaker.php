<?php 
	// 30-8-21: blade engine opmaak verwijderd.
	$section_id = get_sub_field('section_id');
	$section_class = get_sub_field('section_class');
	$section_bg_color = get_sub_field('speaker_bg_color');
	$section_less_margin = get_sub_field('section_less_margin');
?>
<?php if ($section_id): ?>
	<div id="<?php echo $section_id; ?>" ></div>
<?php endif; ?>

<?php if($section_less_margin && in_array('less_margin', $section_less_margin)):?>
<section id="content" class="<?php echo $section_class; ?>" style="background-color: <?php echo $section_bg_color; ?>; padding: 25px 0;">
<?php else : ?>
<section id="content" class="<?php echo $section_class; ?>" style="background-color: <?php echo $section_bg_color; ?>;">
<?php endif; ?>
		<div class="container">
			<div class="row speakers">
				<?php if( get_row_layout() == 'speaker_section' ):
					if( have_rows('speakers') ): while( have_rows('speakers') ): the_row(); 
						$text = get_sub_field('speaker_content');
						$speaker_image = get_sub_field('speaker_image');
						$speaker_button_link = get_sub_field('speaker_button_link');
						$speaker_button_text = get_sub_field('speaker_button_text');
						?>
						<div class="speaker d-md-flex">
							<div class="col-12 col-md-9 content">
								<img src="<?php echo $speaker_image; ?> " alt="" class="d-md-none">
								<?php echo $text; 
								
								get_template_part('resources/components/button_repeater'); ?>

							</div>
							<div class="col-12 col-md-3 image d-none d-md-block">
								<img src="<?php echo $speaker_image; ?> " alt="">
							</div>
						</div>
					
					<?php endwhile; ?>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</section>