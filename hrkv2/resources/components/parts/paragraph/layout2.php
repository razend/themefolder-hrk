<?php 
	// Content
	$text = get_sub_field('content');
	$content_left = get_sub_field('content_left');
	$content_right = get_sub_field('content_right');
?>

<div class="container">
	<div class="row">
		<?php if ($text): ?>
			<div class="col-12 mb-5">
				<?php echo $text; ?>
			</div>
		<?php endif; ?>
		<div class="col-12 col-lg-6 pe-lg-5">
			<?php echo $content_left; ?>
		</div>
		<div class="col-12 col-lg-6 ps-lg-5">
			<?php echo $content_right; ?>
		</div>
		<div class="col-12">
			<?php get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
		</div>
	</div>
</div>	

<?php wp_reset_query(); ?>