<?php 

// Nieuw button toevoegsysteem
$add_buttons = get_sub_field('group_add_buttons'); // knopjes group 


if( have_rows('group_add_buttons') ) : while( have_rows('group_add_buttons')) : the_row();
	$cta_button_align = "text-" . $add_buttons['cta_button_alignment'];
	if( have_rows('cta_adding') ) : ?>
		<div class="<?php echo $cta_button_align; ?> cta-buttons">
		 <?php while( have_rows('cta_adding') ) : the_row();
		 	$cta_button_text = get_sub_field('cta_text');
			$cta_button_link = get_sub_field('cta_link');
			$cta_button_target = get_sub_field('cta_target');
			$cta_button_style = get_sub_field('cta_style');
			$cta_button_size = "size_" . get_sub_field('cta_size');
			 ?>
			<a class="btn <?php echo  $cta_button_style . " " . $cta_button_size; ?>" href="<?php echo $cta_button_link; ?>" target="<?php echo $cta_button_target; ?>"><?php echo $cta_button_text; ?></a>
		 <?php endwhile; ?>
		</div>
	<?php endif; ?>
<?php endwhile; endif;?>