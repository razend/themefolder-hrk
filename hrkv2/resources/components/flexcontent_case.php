<?php 
	// 30-8-21: blade engine opmaak verwijderd.
$section_id = get_sub_field('section_id');
$quote = get_sub_field('quote');
$title = get_sub_field('title');
$text = get_sub_field('content');
$text_color = get_sub_field('text_color');
$bg_color = get_sub_field('bg_color');
$img = get_sub_field('image');
$cta_button_align = get_sub_field('cta_button_alignment');
$sidebar_qoute = get_sub_field('sidebar_qoute');
$section_less_margin = get_sub_field('section_less_margin');
?>

<?php if ($section_id): ?>
	<div id="<?php echo $section_id; ?>"></div>
<?php endif ?>

<?php if($section_less_margin && in_array('less_margin', $section_less_margin)): ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>; padding: 25px 0;">
<?php else: ?>
<section id="content" style="background-color: <?php echo $bg_color; ?>;">
<?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2><em><?php echo $quote; ?></em></h2>
				<p><span class="case_title"><?php echo $title; ?></span></p>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4 col-lg-3 sidebar order-md-12">
				<div class="row">
					<div class="col-7 col-md-12 order-md-11">
					<?php if( get_row_layout() == 'case_section' ):
						if( have_rows('case_attributes') ): ?>
						<ul class="case_attributes">
							<?php while( have_rows('case_attributes') ): the_row(); 
								$case_attribute_title = get_sub_field('case_attribute_title');
								$case_attribute_content = get_sub_field('case_attribute_content')
								?>
								<li>
									<strong><?php echo $case_attribute_title; ?><br></strong>
									<?php echo $case_attribute_content; ?>
								</li>
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
					<?php endif; ?>
					</div>
					<div class="col-5 col-md-12">
						<img src="<?php echo $img; ?>" alt="" class="mb-5" style="width: 100%;">
					</div>
					<div class="col-12 mb-5 mb-md-0 order-md-12">
					<div class="sidebar_qoute">
						<?php echo $sidebar_qoute; ?>
					</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-8 col-lg-9">
				<?php echo $text; ?>
			</div>
		</div>
	</div>
</section>