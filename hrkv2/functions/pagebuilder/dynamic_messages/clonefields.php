<?php 

// Section padding
add_action('acf/render_field/name=dm_section_padding', 'dm_section_padding');
function dm_section_padding(){
    echo 'Geeft aan hoeveel witruimte er rondom de tekst komt.<br>
			geen padding: 0px<br>
			kleine padding: 30px<br>
			normale padding: 60px<br>
			grote padding: 90px<br>
			enorme padding: 120px';
}

// Ontwikkelaar
// Section ID
add_action('acf/render_field/name=dm_section_id', 'dm_section_id');
function dm_section_id(){
    echo 'Met een unieke naam aan een sectie kun je bijvoorbeeld een link maken naar een specifiek blok op de pagina. <br>
			Wil je bijvoorbeeld vanuit het hero blok linken naar een formulier verder op de pagina, maak dan een link zoals deze: ' . rtrim(get_permalink(),'/') . '#unieke-naam-sectie. <br><br>
				Let op! Gebruik een unieke id per pagina en kan geen spaties bevatten. Gebruik voor spaties - of _';
}
// Section Classes
add_action('acf/render_field/name=dm_section_class', 'dm_section_class');
function dm_section_class(){
    echo 'Met een classnaam aan een sectie kun je bijvoorbeeld css laden, specifiek voor secties met deze classnaam. <br><br>
			Let op! een classnaam kan geen spaties bevatten. Gebruik voor spaties - of _';   
}

// Section background
add_action('acf/render_field/name=dr_section_bg', 'dr_section_bg');
function dr_section_bg(){
    echo 'Toegestaane formaten: jpg, jpeg en png<br><br>
    <strong>Layout 1</strong>: Gekleurd vlek + afbeelding: Voorkeursformaat voor volledige breedte: 1000px x 1000px<br><br>
    <strong>Layout 2</strong>: Afbeelding over volledige breedte van het scherm: Voorkeursformaat voor volledige breedte: 1600px x 500px<br><br>'
    ;
}

add_action('acf/render_field/name=dr_section_bg_alignment', 'dr_section_bg_alignment');
function dr_section_bg_alignment(){
    echo 'De afbeelding staat zodaning ingesteld dat deze altijd een volledige breedte van de sectie vult. Met onderstaande instelling kun je aangeven of je de afbeelding aan de bovenkant, in het midden, of aan de onderkant wilt uitlijnen.';
}

add_action('acf/render_field/name=dr_section_bgcolor', 'dr_section_bgcolor');
function dr_section_bgcolor(){
    echo 'Selecteer hier een achtergrond kleur voor dit blok. Dit kan handig zijn voor wanneer je geen of een transparante afbeelding hebt gebruikt.';
}

// Settings
add_action('acf/render_field/name=dr_section_settings_less_margin', 'dr_section_settings_less_margin');
function dr_section_settings_less_margin(){
    echo 'Deze optie zal de standaard marge onder en boven de sectie aanpassen naar 25px ipv 55px';
}

?>