<?php 
	//Layout: multi team members
	$content = get_sub_field('content');	

	$items_selection = get_sub_field('selected_team_members');  
	$posttype = get_post_type(get_the_ID());
 ?>

<div class="container">
	<div class="row">
		<?php if ($content): ?>
			<div class="col-12">
				<?php echo $content; ?>
			</div>
		<?php endif; ?>
		<?php foreach( $items_selection as $post ): 
		setup_postdata($post); 
		

		$team_name = get_the_title();
		$team_function = get_field('team_function');
		$team_bio = get_field('team_bio'); 
		$thumb_url = get_the_post_thumbnail_url(); 
		$thumb_title = get_post(get_post_thumbnail_id())->post_title; ?>

		<div class="col-12 col-lg-6 teammember">
			<div class="card align-items-center h-100">
				<?php if(has_post_thumbnail()): ?>
					<div class="image">
						<img src="<?php echo $thumb_url; ?>" alt="<?php echo $thumb_title; ?>" />
					</div>
				<?php endif; ?>
				<div class="card-body">
					<div class="card-text h-100">
						<span class="quote"><?php echo $team_bio; ?></span>
						<p><span class="name"><?php echo $team_name; ?></span> | <span class="function"><?php echo $team_function; ?></span> </strong>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>

<?php wp_reset_query(); ?>