
<section id="content">
	<div class="container">
		<div class="row justify-content-center job_applying_steps flex-fill">

		@acfrepeater('job_application_steps',$job_application_page)
			<div class="col-6 col-lg-4 mb-5">
				<div class="step">
					<div class="d-flex justify-content-center align-items-center">
						<div class="image"><img src="{{the_sub_field('job_application_step_image')}}" alt=""></div>
					</div>
					<div class="title">
						{{the_sub_field('job_application_step_title')}}
					</div>
					<div class="content">
						{{the_sub_field('job_application_step_content')}}
					</div>
				</div>
			</div>
		@acfend

		</div>
	</div>
</section>
