<section id="team">
	<div class="container">
		<div class="row">
			<div class="col-4">
				<div class="thumbnail teamimage" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>);"></div>
			</div>
			<div class="col-8">
				<p class="team_name pb-0"><?php echo the_title(); ?></p>
				<p class="team_function pb-0"><?php echo the_field('team_function'); ?></p>
				<p class="team_quality tone"><?php echo the_field('team_quality'); ?></p>
				<a class="team_linkedin_profile" href="<?php echo the_field('linkedin_profiel_url'); ?>" target="_blank">Linkedin Profiel <i class="fas fa-external-link-alt"></i></a>
			</div>
		</div>
	</div>
</section>