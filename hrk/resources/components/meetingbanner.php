<section id="blue" class="meetingbanner">
	<div class="container">
		<div class="row ">
			<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo the_field('meetingbanner-team-image'); ?>" alt="">
				</div>
				<div class="teammember-name"><?php echo the_field('meetingbanner-team-name'); ?></div>
			</div>
			<div class="col-12 col-md-8 d-flex align-items-center ">
				<h1><?php echo the_field('meetingbanner-content')}; ?></h1>
			</div>
			<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo the_field('meetingbanner-team-image2'); ?>" alt="">
				</div>
				<div class="teammember-name"><?php echo the_field('meetingbanner-team-name2'); ?></div>
			</div>
			<div class="col-12 d-flex align-items-center justify-content-center">
				<a class="btn mettingbanner-button" data-appointlet-organization="<?php echo the_field('meetingbanner-data-organization'); ?>"><?php echo the_field('meetingbanner-button-text'); ?><script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
			</div>
		</div>
	</div>
</section>