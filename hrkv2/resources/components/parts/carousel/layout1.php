<?php 

$layout = get_sub_field('layout_selection');

wp_reset_query();
$postid = get_the_ID();

?>

<div class="container <?php echo $layout; ?>">
	<div class="row justify-content-center">
		<?php $swiper_number = rand(0, 1000); ?>
		<div class="col-12 position-relative <?php echo "swipernav" . $swiper_number; ?>">
			<div class="swiper <?php echo "swiper" . $swiper_number; ?>">
			      <div class="swiper-wrapper" style="padding-bottom: 30px;">
			     		
				        <?php
				        $count = count(get_sub_field('carousel_slides'));
				         if( have_rows('carousel_slides') ): while( have_rows('carousel_slides') ) : the_row();
							$image =  get_sub_field('carousel_slide_image');
							$title = get_sub_field('carousel_title');
							$content = get_sub_field('carousel_content');
							
							// echo $count;
							// print_r ($image);
						?>
						<div class="swiper-slide d-flex align-items-stretch">

							<div class="w-100" style="background-image: url(<?php echo $image; ?>); background-size: cover;">
								<div class="row">
									<div class="col-12 ">
										<div class="content">
											<?php echo $title; ?>
											<?php echo $content; ?>
											<?php get_template_part('resources/components/parts/flexcontent_part_buttons_v2'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endwhile; endif; ?>
			      </div>

			      <div class="swiper-pagination"></div>

    		</div>
    		<?php if ($count > 1): ?>
    		<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		<?php endif; ?>

  			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins/swiper/swiper-bundle.min.js"></script>
    <!-- Initialize Swiper -->
    <script>
      var swiper = new Swiper("<?php echo ".swiper" . $swiper_number; ?>", {
	    
        // autoHeight: true,
        loop: true,
        loopFillGroupWithBlank: true,
        <?php if ($count < 2): ?>
        allowTouchMove: false,
      <?php endif; ?>
        navigation: {
          nextEl: "<?php echo ".swipernav" . $swiper_number; ?> .swiper-button-next",
          prevEl: "<?php echo ".swipernav" . $swiper_number; ?> .swiper-button-prev",
        },
      });
    </script>
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>