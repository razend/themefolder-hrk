
<?php 
	$address = get_field('address','option');
	$address_2 = get_field('address_2','option');
	$zipcode = get_field('zipcode','option');
	$city = get_field('city','option');
	$mobilenumber = get_field('mobilenumber','option');
	$phonenumber = get_field('phonenumber','option');
	$emailaddress = get_field('emailaddress','option');
	$google_script = get_field('google_script','option');
	$chatprovider = get_field('chatprovider','option');
	$chat_replain_snippet = get_field('chat_replain_snippet','option');
if (!is_404()){ ?>
		<footer>
			<div class="white-bar"></div>
			<div class="container">
				<div class="row">
					<div class="col-6 col-md-4 col-lg-2 col-xl-3">
						<h4>sitemap</h4>
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'footer_sitmap',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'footer-nav',
							'container_id'		=> 'footer-nav',
							'menu_class'		=> 'nav navbar-nav',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-3">
						<h4>algemeen</h4>
						<?php
						wp_nav_menu( array(
							'theme_location'	=> 'footer_general',
							'depth'				=> 2,
							'container'			=> 'div',
							'container_class'	=> 'footer-nav',
							'container_id'		=> 'footer-nav',
							'menu_class'		=> 'nav navbar-nav',
							'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							'walker'			=> new WP_Bootstrap_Navwalker(),
						) );
						?>
					</div>
					<div class="col-12 col-md-4">
						<h4>contact</h4>
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<div class="contact-part">
								<span itemprop="streetAddress"><i class="fas fa-map-marker-alt float-left"></i></i><?php echo $address; ?><br>
								<?php if ($address_2){
									echo $address_2 . "<br>";
								} ?>
								</span>
								<span itemprop="postalCode" class="zipcode"><?php echo $zipcode; ?></span>
								<span itemprop="addressLocality"><?php echo $city; ?></span>
							</div>
							<div class="contact-part">
								<?php if($mobilenumber){ ?>
									<a href="tel:<?php echo $mobilenumber; ?>"><i class="fas fa-phone float-left"></i><span itemprop="telephone"><?php echo $mobilenumber; ?></span></a><br>
								<?php } ?>
					 			
					 			<?php if($phonenumber){ ?>
								<a href="tel:<?php echo $phonenumber; ?>"><i class="fas fa-phone float-left"></i><span itemprop="telephone"><?php echo $phonenumber; ?></span></a>
								<?php } ?>
							</div>
							<?php if($emailaddress) { ?>
							<div class="contact-part">
								<a href="mailto:<?php echo $emailaddress; ?>"><i class="far fa-envelope float-left"></i><span itemprop="email"><?php echo $emailaddress; ?></span></a>
							</div>
							<?php } ?>
							<div class="contact-part">
								<?php get_template_part( '/resources/components/socialmedia'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="vertical-bar  d-none d-lg-block">
				
			</div>
			<div class="logo-area d-none d-lg-block">
				<?php the_custom_logo(); ?>
			</div>
		</footer>
		
<?php get_template_part('resources/views/layouts/copyright_footer');

} 
if($google_script === "analytics_code"){
	get_template_part('resources/components/analytics');
}
if($chatprovider === 'replain'){
	echo $chat_replain_snippet;
}
wp_footer();?>
		
		<script src="<?php echo get_template_directory_uri();?>/js/min/bootstrap-4-navbar.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/global.js"></script>
		
	</body>
</html>