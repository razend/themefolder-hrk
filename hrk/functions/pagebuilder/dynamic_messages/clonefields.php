<?php 

// Section padding
add_action('acf/render_field/name=dm_section_padding', 'dm_section_padding');
function dm_section_padding(){
    echo 'Geeft aan hoeveel witruimte er rondom de tekst komt.<br>
			geen padding: 0px<br>
			kleine padding: 30px<br>
			normale padding: 60px<br>
			grote padding: 90px<br>
			enorme padding: 120px';
}


?>