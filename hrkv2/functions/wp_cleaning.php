<?php 

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
add_filter( 'emoji_svg_url', '__return_false' ); 

remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7 );

remove_action('wp_print_styles', 'print_emoji_styles' ); 
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
remove_action('template_redirect', 'rest_output_link_header', 11, 0);

// Remove Yoast comments
add_action( 'template_redirect', function () {
    if ( ! class_exists( 'WPSEO_Frontend' ) ) {
        return;
    }
    $instance = WPSEO_Frontend::get_instance();
    // make sure, future version of the plugin does not break our site.
    if ( ! method_exists( $instance, 'debug_mark') ) {
        return ;
    }
    // ok, let us remove the love letter.
     remove_action( 'wpseo_head', array( $instance, 'debug_mark' ), 2 );
}, 9999 );

// // Remove jQuery Migrate Script from header and Load jQuery from Google API
// function crunchify_stop_loading_wp_embed_and_jquery() {
// 	if (!is_admin()) {
// 		wp_deregister_script('wp-embed');
// 		wp_deregister_script('jquery');  // Bonus: remove jquery too if it's not required
// 	}
// }
// add_action('init', 'crunchify_stop_loading_wp_embed_and_jquery');


// Stop sending  update notifications
// Core
add_filter( 'auto_core_update_send_email', 'wpb_stop_auto_update_emails', 10, 4 );
  
function wpb_stop_update_emails( $send, $type, $core_update, $result ) {
if ( ! empty( $type ) && $type == 'success' ) {
return false;
}
return true;
}

// Plugins
add_filter( 'auto_plugin_update_send_email', '__return_false' );

// Themes
add_filter( 'auto_theme_update_send_email', '__return_false' );
// end disable notifications script 


?>