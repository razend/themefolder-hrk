<?php 
 // Layout: Vacancies
 ?>
 <div id="vacancie">
    <div class="container">
        <div class="row">
            <?php 
            $the_query = new WP_Query(array('post_type' => 'vacancies'));
            if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post();
                $postID = get_the_ID(); 
                $vacancie_companyname = get_field('vacancie_companyname',$postID);
                $vacancie_opertunities = get_field('vacancie_opertunities',$postID);
                $vacancie_state = get_field('vacancie_state',$postID);
                $vacancie_state_tag = get_field('vacancie_state_tag',$postID);
                $vacancie_experience = get_field('vacancie_experience',$postID);
                $vacancie_hours = get_field('vacancie_hours',$postID);
                $vacancie_hours_tag = get_field('vacancie_hours_tag',$postID);
                $vacancie_job_type = get_field('vacancie_job_type',$postID);
                $vacancie_wages = get_field('vacancie_wages',$postID);
                $vacancie_wages_tag = get_field('vacancie_wages_tag',$postID);
                $term_list = wp_get_post_terms($postID, 'vacancie_categories', array("fields" => "all"));
                $company_logo = get_field('company_logo',$postID);
            ?>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="card ">
                    <div class="imagebox">
                        <a href="<?php echo the_permalink(); ?>">
                            <img class="card-img-top" src="<?php echo the_post_thumbnail_url('$postID'); ?>" alt="">
                            <?php if ($company_logo): ?>
                                <img class="company_logo thumbnail" src="<?php echo $company_logo; ?>" alt="">
                            <?php endif; ?>
                        </a>
                        <div class="labels">
                            <?php if ($vacancie_state_tag): ?>
                                <div class="label" title="Locatie / regio"><span><i class="fa-solid fa-location-dot"></i> <?php echo $vacancie_state_tag; ?></span></div>
                            <?php endif; ?>
                            <?php if ($vacancie_wages_tag): ?>
                                <div class="label" title="Salaris"><i class="fa-solid fa-euro-sign"></i><?php echo $vacancie_wages_tag; ?></div>
                            <?php endif; ?>
                            <?php if ($vacancie_hours_tag): ?>
                                <div class="label" title="Uren"><i class="fa-solid fa-clock"></i><?php echo $vacancie_hours_tag; ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title  mb-3"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h5>
                        <p class="card-details compact pb-0">
                            <?php if($vacancie_companyname): ?>
                                <span><strong>je gaat werken bij: </strong><?php echo $vacancie_companyname; ?> </span><br>
                            <?php endif; ?> 
                            
                            <?php if ($vacancie_experience): ?>
                             <strong>gevraagde relevante ervaring: </strong>
                                <?php foreach( $vacancie_experience as $vacancie_experience ):
                                    echo $vacancie_experience .", ";
                                endforeach; ?>
                            <br>
                            <?php endif ?>
                           
                            <?php if($vacancie_job_type): ?>
                                <strong>soort dienstverband: </strong><?php echo $vacancie_job_type; ?><br>
                            <?php endif; ?>
                        </p>
                        <div class="d-flex justify-content-left mt-3 cta-buttons">
                            <a href="<?php echo the_permalink(); ?>" class="btn style_filled_orange size_small">bekijk vacature</a>
                            <a href="<?php echo the_permalink(); ?>#form_section" class="btn style_outlined_black size_small">direct solliciteren</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
        </div>
    </div>
</div>
<?php wp_reset_query(); ?>
