<script type="text/javascript">

var stage = findGetParameter('stage');
var newtext = '';
switch (stage) {
	case 's1_2_1':
		newtext = 'Wil jij jouw recruitmentkennis verder ontwikkelen?';
		break;
	case 's1_3_1':
		newtext = 'Als jij recruitment leuk vindt en ambitieus bent past ons traineeship perfect bij jou!';
		break;
	case 's1_3_2':
		newtext = 'Ok, je vond recruitment niet zo leuk. Wil je het nog eens proberen onder betere begeleiding?';
		break;
	case 's1_3_3':
		newtext = 'Recruiters stellen graag vragen aan kandidaten. Wil je recruiter worden?';
		break;
	case 's1_4_1':
		newtext = 'Wij zoeken collega\'s die meester in recruitment willen worden.';
		break;
	case 's1_4_2':
		newtext = 'Ontdek andere mooie vacatures bij HetRecruitingKantoor';
		break;
	case 's1_4_3':
		newtext = 'Ok, je vond recruitment niet zo leuk. Wil je het nog eens proberen onder betere begeleiding?';
		break;
	case 's1_4_5':
		newtext = 'Je hebt waarschijnlijk talent voor recruitment. Zullen we dat samen ontdekken?';
		break;
	case 's2_2_1':
		newtext = 'Je hebt ervaring met personeelsbemiddeling. Wil je er je verder in verdiepen?';
		break;
	case 's2_3_1':
		newtext = 'Je vond personeelsbemiddeling leuk. Ben je geinteresseerd in een recruitment traineeship?';
		break;
	case 's2_3_2':
		newtext = 'Je vond personeelsbemiddeling niet leuk. Wil je het nog eens proberen onder betere begeleiding?';
		break;
	case 's2_3_3':
		newtext = 'Jouw talent sluit misschien aan bij het vak personeelsbemiddeling. Sta je open om het te leren?';
		break;
	case 's2_4_1':
		newtext = 'Je bent ge&iuml;nteresseerd in een recruitment traineeship. Laten we kennis maken!';
		break;
	case 's2_4_3':
		newtext = 'Je vond personeelsbemiddeling niet leuk, maar wil het met goede begeleiding opnieuw proberen ?';
		break;
	case 's2_4_5':
		newtext = 'Je hebt misschien talent voor personeelsbemiddeling. Wij bieden je de beste begeleiding.';
		break;
	case 's3_2_1':
		newtext = 'Je hebt ervaring met werving & selectie. Wil je er je verder in verdiepen?';
		break;
	case 's3_3_1':
		newtext = 'Je vond werving & selectie leuk. Ben je ge&iuml;nteresseerd in een recruitment traineeship?';
		break;
	case 's3_3_2':
		newtext = 'Ok, je vond werving & selectie niet leuk. Wil je het nog eens proberen onder betere begeleiding?';
		break;
	case 's3_3_3':
		newtext = 'Jouw talent sluit misschien aan bij het vak werving & selectie. Wil je dat bij ons leren?';
		break;
	case 's3_4_1':
		newtext = 'Je bent ge&iuml;nteresseerd in een recruitment traineeship. Ontdek ons programma.';
		break;
	case 's3_4_2':
		newtext = 'Ontdek andere mooie vacatures bij HetRecruitingKantoor';
		break;
	case 's3_4_3':
		newtext = 'Ok, je vond werving & selectie niet leuk. Wil je het nog eens proberen onder betere begeleiding?';
		break;
	case 's3_4_5':
		newtext = 'Je hebt waarschijnlijk talent voor het vak recruiter. Wij hebben een mooi traineeship voor je!';
		break;
	default:
		newtext = '';
}

if(stage) {
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'send_to': 'DC-13179935/hetre0/'+stage+'+standard'
	});
}

if(newtext!='') {
	var titleobj = document.querySelector('div.content > div.title');
	if(titleobj) {
		titleobj.innerHTML = newtext;
	}
}

function findGetParameter(parameterName) {
	var result = null,
	tmp = [];
	var items = location.search.substr(1).split("&");
	for (var index = 0; index < items.length; index++) {
		tmp = items[index].split("=");
		if(tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	}
	return result;
}

</script>