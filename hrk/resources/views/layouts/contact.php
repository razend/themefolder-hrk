<?php 
	$headerimage = get_field('headerimage');
	$headerImageTitle = get_field('title_in_image_top');
	$customheaderimage = get_field('custom_image_header');
	$show_colored_space = get_field('show_colored_space');

if(get_field('headerimage') == 'custom_image'): ?>

<section id="fullwidth-image" class="d-flex justify-content-start align-items-center custom <?php echo $show_colored_space; ?>" style="background-image: url(<?php echo $customheaderimage; ?>);">

<?php else : ?>

<section id="fullwidth-image" class="<?php echo $headerimage; ?> d-flex justify-content-start align-items-center">

<?php endif; ?>

	<span><?php echo $headerImageTitle; ?></span>
	<div class="white-bar"></div>
</section>	
<?php if( get_field('orangebar') == 'yes' ): ?>
<section id="fullwidth-image" class="orangebar">
	<div class="white-bar"></div>
</section>
<?php endif; ?>
<section id="content" class="contact">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-10">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<p><?php the_content(); ?></p>
				<?php endwhile; else : ?>
					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php 
	$person1 = get_field('person1');
	$person2 = get_field('person2');
	$content = get_field('content');
	$btn_text = get_field('btn_text');
?>
<section id="blue" class="meetingbanner">
	<div class="container">
		<div class="row ">
			<div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo get_the_post_thumbnail_url($person1); ?>" alt="">
				</div>
				<!-- <div class="teammember-name"><?php // echo get_the_title($person1); ?></div> -->
			</div>
			<?php // if($person2): ?>
			<!-- <div class="col-12 col-md-8 d-flex"> -->
				<!-- <h1><?php // echo $content; ?></h1> -->
			<?php // else : ?>
			<div class="col-12 col-md-10 align-items-top ">
				<h1 style="text-align: left;"><?php echo $content; ?></h1>
				<a class="btn mettingbanner-button" data-appointlet-organization="hetrecruitingkantoor"><?php echo $btn_text; ?><script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
			<?php // endif; ?>

				
			</div>

			<?php // if($person2): ?>
			<!-- <div class="col-12 col-md-2 d-flex flex-column align-items-center justify-content-center">
				<div class="teammember-image rounded">
					<img src="<?php echo get_the_post_thumbnail_url($person2); ?>" alt="">
				</div>
				<div class="teammember-name"><?php echo get_the_title($person2); ?></div>
			</div>
			
			<div class="col-12 d-flex align-items-center justify-content-center">
				<a class="btn mettingbanner-button" data-appointlet-organization="hetrecruitingkantoor"><?php echo $btn_text; ?><script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script></a>
			</div> -->
			<?php // endif; ?>

		</div>
	</div>
</section>
<section id="content" class="contact">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-3">
				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<div class="contact-part" >
					<p class="strong no-p"><i class="fas fa-map-marker-alt icon-left"></i>Bezoekadres:</p>
						<span class="address" itemprop="streetAddress"><?php echo the_field('address','option'); ?></span><br>	
						<span class="zipcode" itemprop="postalCode"><?php echo the_field('zipcode','option'); ?></span> <span class="city" itemprop="addressLocality"><?php echo the_field('city','option'); ?></span>
					</div>
					<div class="contact-part">
						<?php if(get_field('phonenumber','option')): ?>
						 	<span itemprop="telephone"><i class="fas fa-phone icon-left"></i><?php echo the_field('phonenumber','option'); ?><br></span>
						 <?php endif; ?>
						 <?php if(get_field('mobilenumber','option')): ?>
							 <span itemprop="telephone"><i class="fas fa-phone icon-left"></i><?php echo the_field('mobilenumber','option'); ?></span>
						<?php endif; ?>
					</div>
					<div class="contact-part">
						<span itemprop="email"><i class="far fa-envelope icon-left"></i><a href="mailto:<?php echo the_field('emailaddress','option'); ?>"><?php echo the_field('emailaddress','option'); ?></a></span>
					</div>
					<div class="contact-part">
						<?php get_template_part('resources/components/socialmedia'); ?>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-9">
				<div class="d-flex">
					<div class="icon"><i class="fas fa-subway fa-6x"></i></div>
					<div class="content">
						<h4>Looproute vanaf tramhalte Transferium Westraven</h4>
						<ol class="routeSteps">
							<li>stap uit bij tramhalte P+R Westraven</li>
							<li>laat de tramhalte achter je</li>
							<li>steek over bij de stoplichten</li>
							<li>loop voor het Bastion Hotel rechtsaf het fietspad op;</li>
							<li>sla aan het einde, voor het water linksaf</li>
							<li>aan je linkerhand vind je het witte verzamelgebouw van Element Offices.</li>
						</ol>
					</div>
				</div>
				<div class="d-flex mt-5">
					<div class="icon"><i class="fas fa-car fa-6x"></i></div>
					<div class="content">
						<h4>Met de auto</h4>
						<ol class="routeSteps">
							<li>stel je navigatie in op Winthontlaan 200 Utrecht of postcode 3526 KV</li>
							<li>neem op de A12 afslag 17 naar de Europalaan (richting P+R Westraven)</li>
							<li>ga vanuit de A2 afrit bij de stoplichten rechtdoor de Mauritiuslaan op</li>
							of
							<li>sla vanuit de A27 afrit linksaf en vervolgens weer linksaf de Mauritiuslaan op</li>
							<li>sla voor het water rechtsaf richting de Winthontlaan (let op: onoverzichtelijk verkeerspunt!)</li>
							<li>na een paar honderd meter vind je de ingang van het parkeerterrein (meteen na het opvallend ANWB bord)</li>
							<li>parkeer (meestal) gratis op het terrein van het witte verzamelgebouw van Element Offices</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<section id="map" class="p-0 position-relative">
	<div class="white-bar"></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 map p-0">

				<iframe class="d-none d-md-block" width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=5.100252628326416%2C52.052629025026825%2C5.115702152252198%2C52.05862580679238&amp;layer=mapnik&amp;marker=52.05562751652936%2C5.107977390289307" ></iframe>
				<button class="btn  d-md-none m-5" onclick="mapsSelector()">Open <?php echo the_field('company_name','option'); ?> in maps</button>
			</div>
		</div>
	</div>
</section>