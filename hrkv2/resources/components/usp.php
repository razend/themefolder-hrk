<section id="usp" class="<?php if(is_front_page()): ?> homepage <?php endif; ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="d-md-flex justify-content-around">
					<?php if( have_rows('usps','option') ): while( have_rows('usps','option') ) : the_row();
						$usp = get_sub_field('usp');
					?>
					<div class="usp">
						<i class="fas fa-check icon-left"></i><?php echo $usp; ?>
					</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>