<div class="col-12">
	<?php the_posts_pagination( array(
		'mid_size'  => 2,
		'prev_text' => __( 'vorige pagina', 'textdomain' ),
		'next_text' => __( 'volgende pagina', 'textdomain' ),
	) ); ?>
</div>