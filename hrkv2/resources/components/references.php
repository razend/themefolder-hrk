<?php 
	// 30-8-21: blade engine opmaak verwijderd.
	$ref_selection = get_field('ref_selection','5');
?>

<section id="references" class="<?php if(is_front_page()):?> third-color <?php endif; ?>">
	<div class="container">
		<div class="row">
			<?php if(is_front_page()): ?>
			<div class="col-12 text-right">
				<h2>enkele referentieprojecten</h2>
			</div>
			<?php endif; ?>

			<?php foreach( $ref_selection as $post ):
			setup_postdata($post);

			$ref_img_txt = get_field('reference_image_text');
			$ref_txt = get_field('reference_text');
			$ref_img = get_field('reference_image');
			$ref_url = get_field('reference_url');
			$ref_readmore = get_field('reference_button_text');

			?>

			<div class="col-12 col-lg-3 reference ">
				<div class="thumbnail d-flex align-items-end " style="background-image: url(<?php echo $ref_img; ?>);" onclick="location.href('<?php echo $ref_url; ?>')">
					<span class="image_content">
						<?php echo $ref_img_txt; ?>
					</span>
				</div>
				<a class="title" href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
				<a class="readmore" href="<?php echo $ref_url ?>" target="_blank"><?php echo $ref_readmore; ?> <i class="fas fa-hand-pointer"></i></a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>