<?php 
	$developer_options = get_sub_field('developer_options');
	$section_id = $developer_options['section_id'];
	if ($section_id): 
		echo "<div id='" . $section_id . "'></div>";
	endif;
?>