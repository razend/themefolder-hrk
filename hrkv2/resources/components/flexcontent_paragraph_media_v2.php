<?php 

	// Layout settings
	$layout = get_sub_field('layout_selection');


	get_template_part( '/resources/components/parts/general/section_id');
	
	get_template_part('resources/components/parts/general/section_opening'); 

	if($layout == 'layout1'):

		get_template_part('resources/components/parts/paragraph_img_vid/layout1');	
	
	elseif($layout == 'layout2'):

		get_template_part('resources/components/parts/paragraph_img_vid/layout2');	

	elseif($layout == 'layout3'):

		get_template_part('resources/components/parts/paragraph_img_vid/layout3');	

	elseif($layout == 'layout4'):

		get_template_part('resources/components/parts/paragraph_img_vid/layout4');	

	elseif($layout == 'layout5'):

		get_template_part('resources/components/parts/paragraph_img_vid/layout5');	

	endif;

get_template_part('resources/components/parts/general/section_close'); 

wp_reset_query(); ?>