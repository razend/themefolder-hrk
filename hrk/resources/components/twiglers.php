<?php

$url = 'https://www.recruitmentbanen.nl/twiglers';
$ch = curl_init();
$timeoutInSeconds = 5;

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, $timeoutInSeconds);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeoutInSeconds);

$result = curl_exec($ch);
curl_close($ch);

try {
    $jobs = new SimpleXMLElement($result);
} catch (Exception $e) {
    // Het resultaat lijkt geen XML te zijn
    die;
}

$jobTitles = [];
for ($i = 0; $i < 5; $i++) {
    if (!isset($jobs->JobPosition[$i])) {
        break;
    }

    $jobTitles[] = [
        'title' => (string) $jobs->JobPosition[$i]->JobDetails->Title,
        'url' => (string) $jobs->JobPosition[$i]->JobDetails->JobUrl,
    ];
}

$html = '';
$wrapper = '<li><a href="%s" target="_blank">%s</a></li>';

foreach ($jobTitles as $job) {
    $html .= sprintf($wrapper, $job['url'], $job['title']);
}
?>

<ul class="jobfeed">
    {{$html}}
</ul>
